<div align="center">
    <u><h1>Prise de note 2023-2024</h1></u>
    <i><h2>ZZ1</h2></i>
    <i><h6>VAN DE MERGHEL Robin</h6></i>
</div>

# Introduction

Ce repo contient mes notes de cours / TP pour le second semestre de **1ère Année d'école d'ingénieurs en Informatique** (2023-2024).

L'idée est de pouvoir partager des notes pour s'entraider, et de pouvoir les retrouver facilement. Mais évidemment tout est à prendre avec des pincettes, et il peut y avoir des erreurs (à me remonter sivouplé 🙏🏻).

# Organisation

Chaque matière a son propre dossier, contenant des `.md` (markdown) de prise de note, ainsi que des `.pdf` ou `.html` de prises de note générés à partir de [cette template de pandoc](https://gitlab.isima.fr/rovandemer/pandoc-course-template). 


# Roadmap

- [ ] Auto-Updater des notes (`md` vers `pdf` / `html`)
- [ ] Gestion des ...
  - [ ] ... images
  - [ ] ... annexes
  - [ ] ... sources

# Contribution

Si vous voulez contribuer (indiquer une erreur, amélioration, nouveau point de vue), n'hésitez pas à faire me contacter sur Discord (*@UnSavantFou*). 