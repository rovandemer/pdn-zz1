---
title: Fiche de révision mathématik
author: VAN DE MERGHEL Robin
date: Septembre 2023
--- 

# Logique

## Différents types de preuves

| Type de preuve | Définition | Exemple |
| --- | --- | --- |
| Preuve directe | On part des hypothèses et on arrive à la conclusion | Si $a$ est pair, alors $a^2$ est pair |
| Preuve par contraposée | Dire que $\neg B \implies \neg A$ | Si $a^2$ est impair, alors $a$ est impair |
| Preuve par l'absurde | On suppose que la conclusion est fausse et on arrive à une contradiction | $\sqrt{2}$ est irrationnel |
| Preuve par Induction | On montre que la propriété est vraie pour $n=1$ et on montre que si elle est vraie pour $n$, alors elle est vraie pour $n+1$ | $1+2+3+...+n = \frac{n(n+1)}{2}$ |

## Quantificateurs

| Quantificateur | Définition | Exemple |
| --- | --- | --- |
| $\forall$ | Pour tout | $\forall x \in \mathbb{R}, x^2 \geq 0$ |
| $\exists$ | Il existe | $\exists x \in \mathbb{R}, x^2 = 2$ |
| $\exists !$ | Il existe un unique | $\exists ! x \in \mathbb{R}, x^2 = 2$ |

### Lois de De Morgan

| Loi | Définition |
| --- | --- |
| $\neg \forall x, P(x) \iff \exists x, \neg P(x)$ | La négation d'une proposition universelle est l'existence d'un contre-exemple |
| $\neg \exists x, P(x) \iff \forall x, \neg P(x)$ | La négation d'une proposition existentielle est la non-existence d'un contre-exemple |

## Opérateurs logiques

| Opérateur | Définition | Exemple |
| --- | --- | --- |
| $\neg$ | Non | $\neg (A \land B) \iff \neg A \lor \neg B$ |
| $\land$ | Et | $A \land B \iff B \land A$ |
| $\lor$ | Ou | $A \lor B \iff B \lor A$ |
| $\implies$ | Implique | $A \implies B \iff \neg A \lor B$ |
| $\iff$ | Équivaut | $A \iff B \iff (A \implies B) \land (B \implies A)$ |

### Lois de Morgan

| Loi | Définition |
| --- | --- |
| $\neg (A \land B) \iff \neg A \lor \neg B$ | La négation d'une conjonction est la disjonction des négations |
| $\neg (A \lor B) \iff \neg A \land \neg B$ | La négation d'une disjonction est la conjonction des négations |

### Lois de distributivité

| Loi | Définition |
| --- | --- |
| $A \land (B \lor C) \iff (A \land B) \lor (A \land C)$ | La conjonction distribue sur la disjonction |
| $A \lor (B \land C) \iff (A \lor B) \land (A \lor C)$ | La disjonction distribue sur la conjonction |

### Lois de transitivité

| Loi | Définition |
| --- | --- |
| $A \implies B \land B \implies C \implies A \implies C$ | La transitivité de l'implication |
| $A \implies B \iff \neg B \implies \neg A$ | La contraposée |

### Lois de réduction

| Loi | Définition |
| --- | --- |
| $A \land A \iff A$ | La réduction de la conjonction |
| $A \lor A \iff A$ | La réduction de la disjonction |

# Ensembles

## Définitions

| Définition | Exemple |
| --- | --- |
| Ensemble | $\{1, 2, 3\}$ |
| Élément | $1 \in \{1, 2, 3\}$ |
| Sous-ensemble | $\{1, 2\} \subset \{1, 2, 3\}$ |
| Ensemble vide | $\emptyset$ |
| Cardinal | $card \ \{1, 2, 3\} = 3$ |
| Ensemble des parties | $\mathcal{P}(\{1, 2, 3\}) = \{\emptyset, \{1\}, \{2\}, \{3\}, \{1, 2\}, \{1, 3\}, \{2, 3\}, \{1, 2, 3\}\}$ |
| Produit cartésien | $\{1, 2\} \times \{3, 4\} = \{(1, 3), (1, 4), (2, 3), (2, 4)\}$ |

*Notes :*

- $\emptyset \subset A$ pour tout ensemble $A$
- $\emptyset \in \mathcal{P}(A)$ pour tout ensemble $A$
- $A \subset A$ pour tout ensemble $A$
- $A \in \mathcal{P}(A)$ pour tout ensemble $A$
- $card \ \mathcal{P}(A) = 2^{card A}$ pour tout ensemble $A$

## Ensembles remarquables

| Ensemble | Définition | Exemple |
| --- | --- | --- |
| $\mathbb{N}$ | Ensemble des entiers naturels | $\mathbb{N} = \{0, 1, 2, 3, ...\}$ |
| $\mathbb{Z}$ | Ensemble des entiers relatifs | $\mathbb{Z} = \{..., -2, -1, 0, 1, 2, ...\}$ |
| $\mathbb{Q}$ | Ensemble des nombres rationnels | $\mathbb{Q} = \{\frac{a}{b} \mid a \in \mathbb{Z}, b \in \mathbb{N}^*\}$ |
| $\mathbb{R}$ | Ensemble des nombres réels | $\mathbb{R} = \{x \mid x \in \mathbb{Q} \lor x \notin \mathbb{Q}\}$ |
| $\mathbb{C}$ | Ensemble des nombres complexes | $\mathbb{C} = \{a + ib \mid a, b \in \mathbb{R}, i^2 = -1\}$ |

## Opérations sur les ensembles

| Opération | Définition | Exemple |
| --- | --- | --- |
| Union | $A \cup B = \{x \mid x \in A \lor x \in B\}$ | $\{1, 2\} \cup \{2, 3\} = \{1, 2, 3\}$ |
| Intersection | $A \cap B = \{x \mid x \in A \land x \in B\}$ | $\{1, 2\} \cap \{2, 3\} = \{2\}$ |
| Différence | $A \setminus B = \{x \mid x \in A \land x \notin B\}$ | $\{1, 2\} \setminus \{2, 3\} = \{1\}$ |
| Complémentaire | $\overline{A} = \{x \mid x \notin A\}$ | $\overline{\{1, 2\}} = \{3\}$ |
| Différence symétrique | $A \Delta B = (A \setminus B) \cup (B \setminus A)$ | $\{1, 2\} \Delta \{2, 3\} = \{1, 3\}$ |

### Lois

| Loi | Définition |
| --- | --- |
| $\overline{A \cup B} = \overline{A} \cap \overline{B}$ | La négation d'une union est l'intersection des négations |
| $\overline{A \cap B} = \overline{A} \cup \overline{B}$ | La négation d'une intersection est l'union des négations |
| $A \cup (B \cap C) = (A \cup B) \cap (A \cup C)$ | L'union distribue sur l'intersection |
| $A \cap (B \cup C) = (A \cap B) \cup (A \cap C)$ | L'intersection distribue sur l'union |

# Equations

## Résolution d'équations

| Type d'équation | Méthode |
| --- | --- |
| $ax + b = 0$ | $x = -\frac{b}{a}$ |
| $ax^2 + bx + c = 0$ | $\Delta = b^2 - 4ac$<br>$x_1 = \frac{-b - \sqrt{\Delta}}{2a}$<br>$x_2 = \frac{-b + \sqrt{\Delta}}{2a}$ |

Dans $\mathbb{C}$, on a $z,\overline{z}$ solutions d'une équation de la forme $az^2 + bz + c = 0$.

## Signe d'une fonction

| Type de fonction | Signe |
| --- | --- |
| $f(x) = ax + b$ | $f(x) \geq 0 \iff x \geq -\frac{b}{a}$ |
| $f(x) = ax^2 + bx + c$ | Du signe de $a$ en dehors des racines. |

# Limites

## Définitions

| Définition | Exemple |
| --- | --- |
| Limite finie | $\lim_{x \to a} f(x) = l$ |
| Limite infinie | $\lim_{x \to a} f(x) = \pm \infty$ |
| Limite à droite | $\lim_{x \to a^+} f(x) = l$ |
| Limite à gauche | $\lim_{x \to a^-} f(x) = l$ |

## Théorèmes

| Théorème | Définition |
| --- | --- |
| Théorème de la limite de la somme | $\lim_{x \to a} (f(x) + g(x)) = \lim_{x \to a} f(x) + \lim_{x \to a} g(x)$ |
| Théorème de la limite du produit | $\lim_{x \to a} (f(x) \times g(x)) = \lim_{x \to a} f(x) \times \lim_{x \to a} g(x)$ |
| Théorème de la limite du quotient | $\lim_{x \to a} \frac{f(x)}{g(x)} = \frac{\lim_{x \to a} f(x)}{\lim_{x \to a} g(x)}$ |
| Théorème de la limite de la composée | $\lim_{x \to a} f(g(x)) = f(\lim_{x \to a} g(x))$ |
| Théorème des gendarmes | Si $f(x) \leq g(x) \leq h(x)$ et $\lim_{x \to a} f(x) = \lim_{x \to a} h(x) = l$, alors $\lim_{x \to a} g(x) = l$ |
| Thorème de l'hospital | Si $\lim_{x \to a} f(x) = \lim_{x \to a} g(x) = 0$ et $\lim_{x \to a} \frac{f'(x)}{g'(x)} = l$, alors $\lim_{x \to a} \frac{f(x)}{g(x)} = l$ |

## Cas indéterminés

| Cas indéterminé | Exemple |
| --- | --- |
| $\frac{0}{0}$ | $\lim_{x \to 0} \frac{\sin x}{x} = 1$ |
| $\frac{\infty}{\infty}$ | $\lim_{x \to \infty} \frac{x}{e^x} = 0$ |
| $0 \times \infty$ | $\lim_{x \to 0} x \ln x = 0$ |
| $\infty - \infty$ | $\lim_{x \to \infty} (\sqrt{x+1} - \sqrt{x}) = 0$ |
| $1^\infty$ | $\lim_{x \to 0} (1 + x)^{\frac{1}{x}} = e$ |
| $\infty^0$ | $\lim_{x \to \infty} (1 + \frac{1}{x})^x = e$ |

## Limites remarquables

| Limite | Méthode |
| --- | --- |
| $\lim_{x \to 0} \frac{\sin x}{x} = 1$ | $\leftrightarrow \lim_{x \to 0} \frac{x - \frac{x^3}{3!} + o(x^3)}{x} = \lim_{x \to 0} (1 - \frac{x^2}{3!} + o(x^2)) = 1$ |
| $\lim_{x \to 0} \frac{1 - \cos x}{x} = 0$ | $\leftrightarrow \lim_{x \to 0} \frac{\frac{x^2}{2!} + o(x^2)}{x} = \lim_{x \to 0} (\frac{x}{2!} + o(x)) = 0$ |
| $\lim_{x \to 0} \frac{e^x - 1}{x} = 1$ | $\leftrightarrow \lim_{x \to 0} \frac{1 + x + \frac{x^2}{2!} + o(x^2) - 1}{x} = \lim_{x \to 0} (1 + \frac{x}{2!} + o(x)) = 1$ |
| $\lim_{x \to 0} \frac{\ln (1 + x)}{x} = 1$ | $\leftrightarrow \lim_{x \to 0} \frac{x - \frac{x^2}{2} + o(x^2)}{x} = \lim_{x \to 0} (1 - \frac{x}{2} + o(x)) = 1$ |
| $\lim_{x \to -\infty} x\times e^x = 0$ | $\leftrightarrow \lim_{x \to -\infty} \frac{x}{e^{-x}} = \lim_{x \to -\infty} \frac{1}{-e^{-x}} = 0$ |

# Dérivées

## Définitions

| Définition | Exemple |
| --- | --- |
| Dérivée | $f'(a) = \lim_{x \to a} \frac{f(x) - f(a)}{x - a}$ |
| Dérivée à droite | $f'_+(a) = \lim_{x \to a^+} \frac{f(x) - f(a)}{x - a}$ |
| Dérivée à gauche | $f'_-(a) = \lim_{x \to a^-} \frac{f(x) - f(a)}{x - a}$ |
| Dérivée seconde | $f''(a) = (f'(a))'$ |
| Dérivée $n$-ième | $f^{(n)}(a) = (f^{(n-1)}(a))'$ |

## Dérivées remarquables

| Fonction | Dérivée |
| --- | --- |
| $x^n$ | $nx^{n-1}$ |
| $\frac{1}{x}$ | $-\frac{1}{x^2}$ |
| $\sqrt{x}$ | $\frac{1}{2\sqrt{x}}$ |
| $e^x$ | $e^x$ |
| $\ln x$ | $\frac{1}{x}$ |
| $\sin x$ | $\cos x$ |
| $\cos x$ | $-\sin x$ |
| $\tan x$ | $\frac{1}{\cos^2 x}$ |
| $\arcsin x$ | $\frac{1}{\sqrt{1 - x^2}}$ |
| $\arccos x$ | $-\frac{1}{\sqrt{1 - x^2}}$ |
| $\arctan x$ | $\frac{1}{1 + x^2}$ |

## Théorèmes

| Théorème | Définition |
| --- | --- |
| Théorème de la dérivée de la somme | $(f + g)'(a) = f'(a) + g'(a)$ |
| Théorème de la dérivée du produit | $(f \times g)'(a) = f'(a) \times g(a) + f(a) \times g'(a)$ |
| Théorème de la dérivée du quotient | $(\frac{f}{g})'(a) = \frac{f'(a) \times g(a) - f(a) \times g'(a)}{g(a)^2}$ |
| Théorème de la dérivée de la composée | $(f \circ g)'(a) = f'(g(a)) \times g'(a)$ |
| Théorème de la dérivée de la réciproque | $(f^{-1})'(a) = \frac{1}{f'(f^{-1}(a))}$ |

# Géométrie

## Vecteurs

| Définition | Explication | Exemple |
| --- | --- | --- |
| Vecteur | Un vecteur est un segment orienté | $\vec{AB}$ |
| Norme | La norme d'un vecteur est sa longueur | $\|\vec{AB}\| = \sqrt{(x_B - x_A)^2 + (y_B - y_A)^2}$ |
| Vecteur nul | Le vecteur nul est un vecteur de norme nulle | $\vec{0}$ |
| Vecteur colinéaire | Deux vecteurs sont colinéaires s'ils sont sur la même droite | $\vec{AB}$ et $\vec{CD}$ |
| Vecteur opposé | Le vecteur opposé d'un vecteur est un vecteur de même norme mais de sens opposé | $\vec{AB}$ et $\vec{BA}$ |
| Vecteur unitaire | Un vecteur unitaire est un vecteur de norme 1 | $\vec{u}$ |
| Vecteur directeur | Un vecteur directeur est un vecteur qui donne la direction d'une droite | $\vec{AB}$ |
| Vecteur normal | Un vecteur normal est un vecteur qui donne la direction d'une droite perpendiculaire à une autre | $\vec{n}$ |
| Produit scalaire | Le produit scalaire de deux vecteurs est un nombre réel | $\vec{AB} \cdot \vec{CD} = \|\vec{AB}\| \times \|\vec{CD}\| \times \cos(\vec{AB}, \vec{CD})$ |
| Produit vectoriel | Le produit vectoriel de deux vecteurs est un vecteur | $\vec{AB} \times \vec{CD} = \|\vec{AB}\| \times \|\vec{CD}\| \times \sin(\vec{AB}, \vec{CD})$ |
