---
title: Cours d'Algorithmique
author: VAN DE MERGHEL Robin
date: 8 Septembre 2023
---

*Note pour l'évaluation : Ne pas suivre la syntaxe de cours à l'examen est incorrecte. Il faut suivre la syntaxe de cours.*


# Introduction

## Programme vs Algorithme

Un *programme* est un moyen de communication entre une machine et une personne. Un programe est écrit dans un langage informatique (`C`, `python`, ...)

Un *algorithme* est un plan de calcul / suite finie d'instruction. Il est indépendant d'un langage informatique.

## Démarche informatique de construction du programme

- Problème posé en langage naturel
- *Analyse du problème*
- Algorithme indépendant du langage (plus ou moins en français)
- Programmation ou codage : traduit l'algorithme en langage informatique

On peut le représenter comme suit :

![Démarche informatique de construction du programme](./img/Graph1.png)

On a notament le vocabulaire suivant :

- *Résultats* = Objectifs à atteindre
- *Donées* = ce qu'il faut pour calculer les résultats
- *Algorithme* = méthode pour fabriquer les résultats à partir des données
  - $\rightarrow$ La recette de cuisine est un algorithme

## Notions de base

### Les variables

> Attention, une variable informatique est différente d'une variable mathématique.

- Le nom que l'on donne à une case mémoire
- Valeur de la variable $\rightarrow$ Contenu de la case mémoire
- Chaque fois que l'on range une nouvelle valeur dans une variable, on écrase l'ancienne valeur

### Lecture et Écriture

On peut stocker et récupérer des données.

Il y a alors deux opérations élémentaires :

- Opération de lecture : aller chercher une donnée sur un organe d'entrée et ranger sa valeur dans une variable : `Lire(A);`
- Opération d'écriture : envoyer le contenu d'une case mémoire sur un organe de sortie : `Ecrire(A);`

### Affectation

*L'affectation* est une opération élémentaire qui permet de ranger une valeur dans une varaible. On la note `:=` ou $\leftarrow$.

Le terme de gauche d'une affectation est toujours un nom de variable.

Le terme de droite d'une affectation est une expression qui peut être :

- Une valeur constante
- Une variable
- Une expression arithmétique
- Une expression booléenne
- ...


##### Exercice

###### Exercice 1

> Faire un programme pour échanger deux variables.

```Pascal
a := 5;
b := 3;

c := a; // On stocke la valeur de a dans c
a := b;
b := c;
```

On a bien à la fin `a = 3` et `b = 5`.


###### Exercice 2

> Faire une permutation circulaire de trois variables.

```Pascal
a := 5;
b := 3;
c := 2;

d := a;
a := b;
b := c;
c := d;
```

On a bien à la fin `a = 3`, `b = 2` et `c = 5`.

### Expressions arithmétiques

Les expressions sont calculées de gauche à droite avec les opérateurs suivants :

- `+` : Addition
- `-` : Soustraction
- `*` : Multiplication
- `/` : Division réelle ($\neq$ division entière, Cf *Opérateurs supplémentaires*)
- `^` : Puissance

Et les parenthèses modifie l'ordre de priorité.

> On rajoutera les opérateurs supplémentaires `div` et `mod` respectivement pour la division entière et le reste de la division entière.

### Variables logiques (ou *Booléennes*)

Une variable boléenne $B$ peut prendre que deux valeurs : `Vrai` ou `Faux`.

On peut donner à $B$ le résultat d'une expression conditionnelle. Ces expressions suivent les lois de l'algèbre de *Boole* que l'on verra après.

## Alternatives

Une instruction conditionnelle ou alternative permet un choix entre deux ou plusieurs traitements pour une donnée en fonction du résultat d'une condition (expression conditionnelle).

*1ère règle de l'algorithmique :* lorsque le traitement porte sur une donnée et comporte un choix entre duex ou plusieurs possibilité, la structure de l'algorithme est une structure alternative.

### `Si ... Alors ... Sinon`

On a par exemple :

```python
Si condition alors
    traitement 1;
Sinon
    traitement 2;
fsi;
```

On peut le représenter comme suit :

```mermaid
graph LR
A[Condition] --> B[Traitement 1]
A --> C[Traitement 2]
```

### Juxtaposition et imbrication

On peut imbriquer ou juxtaposer les alternatives.

*2ème règle de l'algorithmique :* 

- lorsque deux ou plusieurs traitement son en intersection, la structure de l'algorithme est en *alternatives consécutives*.
- lorsque deux ou plusieurs traitements sont exclusifs, la structure de l'algorithme est en *alternatives imbriquées*.

#### Exercice

###### Exercice 3

> Écrire un algorithme qui donne la plus grande valeur de deux valeurs saisies au clavier. Si la méthode choisie fait intervenir plusieurs commandes "écrire", donner une version qui ne laisse qu'une occurence.

```Pascal
Lire(a);
Lire(b);

Si a > b alors
    Ecrire(a);
Sinon
    Ecrire(b);
fsi;
```

> Avec une seule occurence :

```Pascal
Lire(a);
Lire(b);

Si a > b alors
    max := a;
Sinon
    max := b;
fsi;

Ecrire(max);
```

> Troisième possibilité (funky) :

```Pascal
Lire(a);
Lire(b);

Si a < b alors
    a := b;
fsi;

Ecrire(a);
```

###### Exercice 4

> Afficher l'état physique de l'eau (gaz, liquide, solide) selon sa température.

```Pascal
Lire(T);

Si T < 0 alors
    Ecrire("Solide");
Sinon Si T > 100 alors
    Ecrire("Gaz");
Sinon
    Ecrire("Liquide");
fsi;
```

*Note : On n'a pas le droit de faire `Sinon Si`, donc on modifie la condition.*

```Pascal
Lire(T);

Si T < 0 alors
    Ecrire("Solide");
Sinon
    Si T > 100 alors
        Ecrire("Gaz");
    Sinon
        Ecrire("Liquide");
    fsi;
fsi;
```

> On améliore l'algorithme :

```Pascal
Lire(T);

Si T < 0 alors
    T := "Solide";
Sinon
    Si T > 100 alors
        T := "Gaz";
    Sinon
        T := "Liquide";
    fsi;
fsi;

Ecrire(T);
```

###### Exercice 5

> Ranger dans une variable max le plus grand de trois nombres `a`, `b` et `c`.

```Pascal
Lire(a);
Lire(b);
Lire(c);

Si a > b alors
    max := a;
Sinon
    max := b;
fsi;

Si c > max alors
    max := c;
fsi;

Ecrire(max);
```

> On peut améliorer l'algorithme :

```Pascal
Lire(a);
Lire(b);
Lire(c);

Si b > a alors
    a := b;
fsi;

Si c > a alors
    a := c;
fsi;

Ecrire(a);
```

###### Exercice 6

> Faire un programme qui donne l'appartenance d'un nombre à un intervalle.

```Pascal
Lire(x);
Lire(min);
Lire(max);

resultat := FAUX;

Si x < min alors
    resultat := FAUX;
Sinon
    Si x > max alors
        resultat := FAUX;
    Sinon
        resultat := VRAI;
    fsi;
fsi;

Ecrire(resultat);
```

###### Exercice 7

> Le lundi, la boulangerie est fermée. Le mercredi elle ouvre de 7h à 19h non stop. Le dimanche elle ouvre de 7h à 12h. Les autres jours, elle ouvre de 7h à 13h et de 16h à 19h. Écrire un algorithme qui lit le jour et l'heure et affiche si la boulangerie est ouverte ou fermée.

```Pascal
Lire(jour);
Lire(heure);

ouverture := FAUX;

// On ne teste que quand c'est ouvert
Si jour != "Lundi"
    Si jour = "Mercredi" alors
        Si heure >= 7 et heure <= 19 alors
            ouverture := VRAI;
        fsi;
    sinon
        Si jour = "Dimanche" alors
            Si heure >= 7 et heure <= 12 alors
                ouverture := VRAI;
            fsi;
        sinon

            Si heure >= 7 et heure <= 13 alors
                ouverture := VRAI;
            fsi;

            Si heure >= 16 et heure <= 19 alors
                ouverture := VRAI;
            fsi;
        fsi;
    fsi;
fsi;

Ecrire(ouverture);
```

###### Exercice 8

> Écrire l'algorithme de calcul des racines réelles du trinôme du second degré en incluant les cas particuliers :

- $a=0, b=0, c=0$ : infinité de solutions
- $a=0, b=0, c\neq0$ : pas de solution
- $a=0, b\neq0, c\neq0$ : premier degré

```Pascal
Lire(a);
Lire(b);
Lire(c);

// On admet que SQRT existe
// On admet que NULL existe

racine1 := Null;
racine2 := Null;

Si a = 0 alors
    Si b != 0 alors
        // Premier degré
        racine1 := -c / b;
    fsi;
Sinon
    delta := b^2 - 4 * a * c;

    Si delta > 0 alors
        // Second degré (avec possibilité racine double)
        racine1 := (-b - sqrt(delta)) / (2 * a);
        racine2 := (-b + sqrt(delta)) / (2 * a);
    fsi;
fsi;

Si a = 0 et b = 0 et c = 0
    Ecrire("Infini de solutions");
sinon
    Si racine1 == Null et racine2 == Null
        Ecrire("Il n'y a pas de racine.")
    sinon
        Ecrire(racine1);
        Ecrire(racine2);
    fsi;
fsi;
```

### Opérateurs particuliers

Lorsque deux (ou plusieurs) conditions sont séparées par l'opérateur `ET`, il arrive que la seconde ne doive pas être évaluée si la première et fause. Dans ce cas on utilisie `ET ALORS` (`else if`).

De manière symétrique, `OU SINON` empêche la deuxième condition d'être évaluée si la première est vraie.

C'est l'*évaluation paresseuse*.

### Alternative à choix multiples 

Cela évite l'écrite de `SI` imbriqués.

```Pascal
Selon expression = 
    valeur 1 : traitement 1;
    valeur 2 : traitement 2;
    ...
    défaut: traitement 4;
fselon;
```

## Itératives

Une instruction itérative ou répétitive permet la répétition d'un même traitement sur plusieurs objets de même nature. L'arrêt du traitement est réalisé grâce à une condition.

*3e règle de l'algorithmique : lorsque le traitement porte sur plusieurs données, la structure de l'algorithme est itérative.

**⚠️ Attention aux ensembles qui ne sont pas finis.**

### Algorithme à récurrence fixe : `Pour`

```Pascal
Pour id de E1 à E2 pas E3 faire
    traitement;
fait/finpour;
```

- `id` est un identificateur
- `E1` est la valeur initiale de `id`
- `E2` est la valeur finale de `id`
- `E3` est le pas ou l'incrément de `id`

### Algorithme à récurrence variable : `Tant que` ou `Répéter`

Le nombre de récurrence ici est non fixé à l'avance.

```Pascal
Tant que condition faire
    traitement;
fait/fintantque;
```

```Pascal
Répéter
    traitement;
jusqu'à condition; fait/finrépéter;
```

### Méthode pour le choix d'un algorithme itératif

On a plusieurs cas :

- Simuler sur un exemple le traitement à réaliser
- Identifier le segment  de traitement qui est répété
- Choisir la meilleure structure de boucle

```Pascal
Si le nombre d'itérations est fixe alors
    choisir le pour;
Sinon
    Si le nombre d'itérations est variable alors
        choisir le tant que ou le répéter;
    fsi;
fsi;
```

- Définir le ou les tests d'arrêt
  - Valeur finale de la variable de contrôle dans le cas `pour`
  - Test de continuation pour le `tant que`
  - Test d'arrêt pour le `répéter`
- Faire une *trace* de l'exécution sur :
  - Le cas normal
  - Les cas particuliers
  - Les cas d'erreur

### Sous ensembles dans un ensemble

Une structure algorithmique particulèrement bien adaptée au traitement de sous ensembles dans un ensemble ce sont les `tant que` ou `répéter` imbriqués car elle suit la structure de l'objet traité.

```Pascal
Tant que ensemble rouge non fini faire
    Tant que ss-ensemble bleu B non fini faire
        traitement sur B;
    fait;
    // Instructions éventuelles pour gérer la sortie de B
fait;
```

### Qualité d'un algorithme

Plusieurs critères pour évaluer la qualité d'un algorithme (parfois contradictoires, il faut alors trouver le meilleur compromis).

##### Exercice

###### Exercice 9

> Calcul de $Y = \sum_{i=1}^{n} A_i$ sans les $i$, c'est à dire en lisant les valeurs.

```Pascal
Lire(n);
Y := 0;

Pour i allant de 1 à n faire
    Lire(A);
    Y := Y + A;
fait;
```

> On l'adapte avec une boucle `Tant que` :

```Pascal
Lire(n);
Y := 0;
i := 1;

Tant que i <= n faire
    Lire(A);
    Y := Y + A;
    i := i + 1;
fait;
```

###### Exercice 10

> Calcul de $n!$

```Pascal
Lire(n);
fact := 1;

Pour i allant de 1 à n faire
    fact := fact * i;
fait;
```

On fait la trace avec $n = 4$ :

| i    | fact |
| ---- | ---- |
| 1    | 1    |
| 2    | 2    |
| 3    | 6    | 
| 4    | 24   |

On a donc bien $4! = 24$.

> On l'adapte avec une boucle `Tant que` :

```Pascal
Lire(n);
fact := 1;
i = 0;

Tant que i < n faire
    fact := fact * i;
    i := i + 1;
fait;
```

###### Exercice 11

> Saisie de deux nombres positifs au clavier.

```Pascal
a := -1
b := -1

Tant que a < 0 faire
    Lire(a);
fait;

Tant que b < 0 faire
    Lire(b);
fait;
```

> Adaptation de la prof (*🙄 jem po*) avec que un `Tant que`

```Pascal
a := -1
b := -1

Tant que a < 0 ou b < 0 faire
    Lire(a);
    Lire(b);
fait;
```

> Adaptation 2 : avec un `Répéter`

```Pascal
a := -1
b := -1

Répéter
    Lire(a);
    Lire(b);
jusqu'à a >= 0 et b >= 0; fait;
```

###### Exercice 12

> Division de l'entier a positif par l'entier b strictement positif en supposant que l'on ne connaît que l'addition et la soustraction (division euclidienne : trouver $q$ et $r$ positif tels que $a = b\times q + r$ avec $r<b$).

```Pascal
Lire(a);
Lire(b);

q := 0;
r := a;

Tant que r >= b faire // Tant que l'on peut diviser
    r := r - b;
    q := q + 1;
fait;

Ecrire(q);
Ecrire(r);
```

On fait la trace avec $a = 17$ et $b = 3$ :

| Étape | $q$    | $r$    | $r\geq b$ |
| ----- | ------ | ------ | --------- |
| 0     | 0      | 17     | `VRAI`    |
| 1     | 1      | 14     | `VRAI`    |
| 2     | 2      | 11     | `VRAI`    |
| 3     | 3      | 8      | `VRAI`    |
| 4     | 4      | 5      | `VRAI`    |
| 5     | 5      | 2      | `FAUX`    |

On a donc bien $17 = 3\times 5 + 2$.

> On améliore le code pour empêcher les cas d'erreurs suivants :

- $b = 0$
- $a < 0$
- $b < 0$

```Pascal
Lire(a);
Lire(b);

Si b = 0 alors
    Ecrire("Erreur : b ne peut pas être nul.");
Sinon
    Si a < 0 ou b < 0 alors
        Ecrire("Erreur : a et b doivent être positifs.");
    Sinon
        q := 0;
        r := a;

        Tant que r >= b faire // Tant que l'on peut diviser
            r := r - b;
            q := q + 1;
        fait;

        Ecrire(q);
        Ecrire(r);
    fsi;    
fsi;
```

- Avec $a = 17$ et $b = 0$, on a bien `Erreur : b ne peut pas être nul.`
- Avec $a = -17$ et $b = 3$, on a bien `Erreur : a et b doivent être positifs.`

###### Exercice 13

> Résolution de l'équation $x=f(x)$ par approximations successives. Partant de $x_0$ quelconque, on calcule successivement $x_1, \dots, x_n$ jusqu'à ce que $|x_i - x_{i-1}| < \epsilon$. La convergence est assurée si la norme infinie de la dérivée est strictement plus petite que 1 (on demande juste d'écrire l'algo en supposant les conditions d'applications sont vérifiées). Attention ici à la condition d'arrêt. Il faut donner un nombre maximum d'itérations car il peut être difficile de vérifier que la fonction $f$ a la bonne propriété ou alors l'algo peut "diverger" avec les erreurs dues à la précision machine.

```Pascal
Lire(x0);
Lire(epsilon);

x := x0;
x1 := f(x0);
i := 1;

Tant que |x1 - x| >= epsilon et i < 100 faire
    x := x1;
    x1 := f(x);
    i := i + 1;
fait;

Ecrire(x1);
```

On fait la trace avec $f(x) = e^{-x}$, $x_0 = 1$ et $\epsilon = 10^{-3}$ :

| $i$ | $x$   | $x_1$ | $abs(x_1 - x)$ | $abs(x_1 - x) \geq \epsilon$ | $i < 100$ |
|-|-|-|-|-|-|
| 1   | 1     | 0.367 | 0.633       | `VRAI`                    | `VRAI`    |
| 2   | 0.367 | 0.692 | 0.325       | `VRAI`                    | `VRAI`    |
| 3   | 0.692 | 0.501 | 0.191       | `VRAI`                    | `VRAI`    |
| 4   | 0.501 | 0.607 | 0.106       | `VRAI`                    | `VRAI`    |
| 5   | 0.607 | 0.545 | 0.062       | `VRAI`                    | `VRAI`    |
| 6   | 0.545 | 0.579 | 0.034       | `VRAI`                    | `VRAI`    |
| 7   | 0.579 | 0.560 | 0.019       | `VRAI`                    | `VRAI`    |
| 8   | 0.560 | 0.571 | 0.011       | `VRAI`                    | `VRAI`    |
| 9   | 0.571 | 0.566 | 0.005       | `VRAI`                    | `VRAI`    |
| 10  | 0.566 | 0.568 | 0.002       | `VRAI`                    | `VRAI`    |
| 11  | 0.568 | 0.567 | 0.001       | `VRAI`                    | `VRAI`    |
| 12  | 0.567 | 0.567 | 0.000       | `FAUX`                    | `VRAI`    |

On a donc bien $e^{-0.567} \approx 0.567$.

![Graphique](Img/Approx.png)

## Algorithme paramétré

Un *algorithme paramété* se comporte comme une nouvelle instruction qui sera exécutée avec les valeurs de paramètre d'entrée et qui fournira des valeurs de paramètre de sorties. Deux formes possibles :

- La *fonction*
- La *procédure*

### Fonction

Une fonction est un algorithme paramété qui fournit un résultat unique et qui est utilisé dans une expression.

```Pascal
fonction nomFonction(Id1, Id2, ..., Idn):
    instruction 1;
    ...
    instruction p;

    retourne expression;
FIN;
```

- `nomFonction` est le nom de la fonction
- `Id1, Id2, ..., Idn` sont les paramètres d'entrée
- `instruction 1, ..., instruction p` sont les instructions de la fonction
- `expression` est l'expression qui sera retournée
- `retourne` est l'instruction qui permet de retourner la valeur de l'expression

> Que se passe-t-il lorsqu'un programme appelle une fonction ? Programme principal et fonction sont rangés dans des parties différentes de la mémmoire. Au moment où l'ordinateur rencontre l'appel de la fonction, il lui donne le contrôle de l'exécution. La fonction s'exécute et retourne le résultat au programme principal qui reprend le contrôle de l'exécution.

##### Exercice

###### Exercice 14

> Écrire sous forme de fonction, la somme des carrés des $N$ premiers entiers.

```Pascal
fonction sommeCarre(N):
    retourne N * (N + 1) * (2 * N + 1) / 6;
FIN;
```

> Modif pour la prof (*🙄 jem po*)

```Pascal
fonction sommeCarre(N):
    somme := 0;

    Pour i allant de 1 à N faire
        somme := somme + i^2;
    fait;

    retourne somme;
FIN;
```

###### Exercice 15

> Écrire une fonction (bolléenne) qui retourne `VRAI` si un nombre est positif ou nul, et `FAUX` sinon.

```Pascal
fonction estPositifOuNul(x):
    retourne x >= 0;
FIN;
```

> Modif pour la prof (*🙄 jem po*)

```Pascal
fonction estPositifOuNul(x):
    Si x >= 0 alors
        retourne VRAI;
    Sinon
        retourne FAUX;
    fsi;
FIN;
```

### Procédure

Une *procédure* est un algorithme paramété qui à partir des paramètres d'entrée fournit zéro ou plusieurs résultats dans des paramètres de sortie.

*ÉDIT : trèèès mal dit sur la slide, en gros ça renvoie rien mais ça modifie des variables.*

```Pascal
procédure nomProcédure(Id1, Id2, ..., Entree-Sortie: ..., Sortie: Idn):
    instruction 1;
    ...
    instruction p;
FIN;
```

- Entrée-Sortie : on le modifie dans la procédure et on le récupère dans le programme principal.


Une procédure est appelée comme une instruction :

- On peut écrire le programme de division euclidienne sous forme de procédure, il y a donc 2 paramètres de sortie, le quotient, et le reste : `procedure divisionEuc(entrée: A,B; sortie: Q,R)`

**Attention** : ne pas recalculer plusieurs fois la même fonction.

## Tableau

Un tableau est une variable structurée qui regroupe sous un même nom plsueirus données élémentaires, c'est à dire plusieurs entiers, plusieurs réels...

On introduit ici la notion de structure de données (SDD) : une SDD est un ensemble de cellules mémoires regroupées sous un même nom et qui sont accessibles par un indice.

### Dimension 1

Une structure de données formées de celules contigues et d'accès direct.

Pour définir un tableau, il faut :

- Un nom
- Le type des données contenues dans le tableau
- La taille du tableau

*Attention*, ne pas confondre taille et dimension.

Un tableau est souvent défini avec une taille plus grande que nécessaire à un instant donné. Une variable supplémentaire doit alors être utilisée pour donner la taille actuelle du tableau (nombre d'éléments actuellement stockés dans le tableau).

Sauf mention contraire, un tableau est toujours rempli de manière contigue et dès la première case.

![Un tableau en algo](https://4.bp.blogspot.com/-FT--XHHiy_A/ToNj1M8_hyI/AAAAAAAAEG4/QOpXXAfc_-Q/s1600/definitiontypealgo.png)

```Pascal
Procédure lireTableau(E:T, N) // E comme "Entrée"
    Pour i de 0 à N-1 faire
        Lire(T[i]);
    fait;
Fin;
```

### Dimension 2

Le tableau à deux dimensions (équivalent d'une matrice) est une structure de données permettant l'implantation d'une suite $A$ doublement indicée par $(ij)$.

En général et pour ne pas s'y perdre, on se cale sur les habitudes mathématiques : $i$ correspond à l'indice de ligne et $j$ à l'indice de colonne.

##### Exercice

###### Exercice 16

> Trouver le max d'un tableau d'entiers

```Pascal
fonction max(E: T -> Tableau, N -> Taille du tableau): -> Entier
    max := T[0];

    Pour i de 1 à N-1 faire // N-1 car les indices commencent à 0
        Si T[i] > max alors
            max := T[i];
        fsi;
    fait;

    retourne max;
Fin;
```

> Alternative avec `n` (nombre d'éléments du tableau) au lieu de `N` (taille du tableau)

```Pascal
fonction max(E: T -> Tableau, n -> Nombre d'éléments du tableau): -> Entier
    max := T[0];

    Pour i de 1 à n-1 faire // n-1 car les indices commencent à 0
        Si T[i] > max alors
            max := T[i];
        fsi;
    fait;

    retourne max;
Fin;
```

On gagne théoriquement en efficacité si le tableau est plus petit que sa taille maximale (en `C` ce cas n'existe pas, chaque tableau est plein).

On fait la trace avec $T = [1, 5, 3, 2, 4]$ :

| $i$ | $T[i]$ | $max$ | $T[i] > max$ |
| --- | ------ | ----- | ------------ |
| 0   | 1      | 1     | `FAUX`       |
| 1   | 5      | 5     | `VRAI`       |
| 2   | 3      | 5     | `FAUX`       |
| 3   | 2      | 5     | `FAUX`       |
| 4   | 4      | 5     | `FAUX`       |

On a donc bien $max(T) = 5$.


###### Exercice 17

> Somme des éléments d'un tableau

```Pascal
fonction somme(E: T -> Tableau, N -> Taille du tableau): -> Entier
    somme := 0;

    Pour i de 0 à N-1 faire
        somme := somme + T[i]; // On ajoute la valeur de la case i à la somme
    fait;

    retourne somme; 
Fin;
```

On fait la trace avec $T = [1, 5, 3, 2, 4]$ :

| $i$ | $T[i]$ | $somme$ |
| --- | ------ | ------- |
| 0   | 1      | 1       |
| 1   | 5      | 6       |
| 2   | 3      | 9       |
| 3   | 2      | 11      |
| 4   | 4      | 15      |

On a donc bien $somme(T) = 15$.

###### Exercice 18

> Addition de deux tableaux

*Note : on admet que les deux tableaux ont la même taille.*

```Pascal
procédure addition(E: T1 -> Tableau, T2 -> Tableau, N -> Taille des tableaux, S-E: T3 -> Tableau):

    Pour i de 0 à N-1 faire
        T3[i] := T1[i] + T2[i];
    fait;

    // On peut aussi faire un retourne T3;
    // On utilise une procédure car on va modifier T3 et donc pas le renvoyer
Fin;
```

On fait la trace avec $T_1 = [1, 5, 3, 2, 4]$ et $T_2 = [2, 4, 6, 8, 10]$ :

| $i$ | $T1[i]$ | $T2[i]$ | $T3[i]$ |
| --- | ------- | ------- | ------- |
| 0   | 1       | 2       | 3       |
| 1   | 5       | 4       | 9       |
| 2   | 3       | 6       | 9       |
| 3   | 2       | 8       | 10      |
| 4   | 4       | 10      | 14      |

On a donc bien $T_3 = [3, 9, 9, 10, 14]$.

###### Exercice 19

> Vérifier qu'un tableau est trié

```Pascal
fonction estTrie(E: T -> Tableau, n -> Nombre d'éléments du tableau): -> Booléen
    i := 0;

    Tant que i < n-1 et T[i] <= T[i+1] faire
        i := i + 1;
    fait;

    retourne i = n-1; // "Si on est arrivé à la fin du tableau, alors il est trié"

    // Au aurait pu aussi faire une boucle Pour avec "Si T[i] > T[i+1] alors retourne "FAUX""
Fin;
```

On fait la trace avec $T = [1, 2, 5, 4, 5]$ :

| $i$ | $T[i]$ | $T[i+1]$ | $T[i] \leq T[i+1]$ | $i < n-1$ | $i = n-1$ |
| --- | ------ | -------- | ---------------- | --------- | --------- |
| 0   | 1      | 2        | `VRAI`           | `VRAI`    | `FAUX`    |
| 1   | 2      | 5        | `VRAI`           | `VRAI`    | `FAUX`    |
| 2   | 5      | 4        | `FAUX`           | `VRAI`    | `FAUX`    |

On a donc bien $estTrie(T) = FAUX$.

Et on fait avec un cas où le tableau est trié : $T = [1, 2, 3, 4, 5]$ :

| $i$ | $T[i]$ | $T[i+1]$ | $T[i] \leq T[i+1]$ | $i < n-1$ | $i = n-1$ |
| --- | ------ | -------- | ---------------- | --------- | --------- |
| 0   | 1      | 2        | `VRAI`           | `VRAI`    | `FAUX`    |
| 1   | 2      | 3        | `VRAI`           | `VRAI`    | `FAUX`    |
| 2   | 3      | 4        | `VRAI`           | `VRAI`    | `FAUX`    |
| 3   | 4      | 5        | `VRAI`           | `VRAI`    | `FAUX`    |
| 4   | 5      | -        | -                | `FAUX`    | `VRAI`    |

On a donc bien $estTrie(T) = VRAI$.

###### Exercice 20

> Rotation droite

*Note : c'est juste un décalage de tous les éléments vers la droite.*

```Pascal
procédure rotationDroite(E:n -> Nombre d'éléments du tableau, S-E: T -> Tableau):
    
    tmp := T[n-1];

    Pour i de n-1 à 1 pas -1 faire
        T[i] := T[i-1];
    fait;

    T[0] := tmp;

Fin;
```

On fait la trace avec $T = [1, 2, 3, 4, 5]$ :

| $i$ | $T[i]$ | $T[i-1]$ |
| --- | ------ | -------- |
| 4   | 5      | 4        |
| 3   | 4      | 3        |
| 2   | 3      | 2        |
| 1   | 2      | 1        |
| 0   | 1      | 5        |


On a donc bien $T = [5, 1, 2, 3, 4]$.



###### Exercice 21

> Insérer un nouvel élément dans un tableau

*Note : vérifier si le tableau est plein*

*Note 2 : vérifier l'indice*

```Pascal
procédure inserer(E: x -> Valeur à insérer, 
                  N -> Taille du tableau,
                  i -> Indice où insérer,
                  n -> Nombre d'éléments du tableau ;
                  S-E: T -> Tableau):
        
    Si i >= 0 et i < N alors
        Si i <= n alors
            // On décale tous les éléments à partir de i
            Pour j de n-1 à i pas -1 faire
                T[j+1] := T[j];
            fait;

            // On insère x à la place de T[i]
            T[i] := x;
        Sinon
            Ecrire("Erreur : l'indice est trop grand.");
        fsi;
    Sinon
        Ecrire("Erreur : l'indice est négatif ou trop grand.");
    fsi;

Fin;
```

On fait la trace avec $T = [1, 2, 3, 4, 5]$, $x = 6$, $i = 2$, $N = 10$ et $n = 5$ :

| $i$ | $T[i]$ | $T[i+1]$ |
| --- | ------ | -------- |
| 4   | 5      | 5        |
| 3   | 4      | 4        |
| 2   | 3      | 3        |
| 1   | 2      | 2        |
| 0   | 1      | 1        |

On a donc bien $T = [1, 2, 6, 3, 4, 5]$.

###### Exercice 22

> Séparer au sein d'un même tableau les valeurs positives des valeurs négatives.

*On fait un tri en place, avec l'algo de tri à bulle.*

```Pascal
procédure triBulle(E-S: T -> Tableau, S-E: n -> Nombre d'éléments du tableau):
    i := 0;
    tmp := 0;
    tri := FAUX;

    Tant que tri = FAUX faire
        tri := VRAI;

        Pour i de 0 à n-2 faire
            Si T[i] > T[i+1] alors
                tmp := T[i];
                T[i] := T[i+1];
                T[i+1] := tmp;
                tri := FAUX;
            fsi;
        fait;
    fait;
Fin;
```

La complexité de l'algorithme est de $O(n^2)$.


> Version du prof :

```Pascal
Procedure sépare(E: n -> Nombre d'éléments du tableau, S-E: T -> Tableau):
    deb := 0;
    fin := n-1;

    Tant que fin > deb faire
        Tant que fin > deb et T[deb] >= 0 faire
            deb := deb + 1;
        fait;

        Tant que fin > deb et T[fin] < 0 faire
            fin := fin - 1;
        fait;

        Si fin > deb alors
            Echange(T[deb], T[fin]);
            deb := deb + 1;
            fin := fin - 1;
        fsi;
    fait;
Fin;
```

L'idée est que l'on va chercher les bornes de la partie positive et de la partie négative (les deux boucles `Tant que`), puis pour chaque élément dans l'intervalle, on va les échanger si besoin (la condition `Si`).

<!-- ![Séparation](Img/Separer.mp4) -->

###### Exercice 23

> Transposer une matrice

```Pascal
procédure transpose(E: n -> Nombre de lignes de la matrice, m -> Nombre de colonnes de la matrice, S-E: M -> Matrice):
    tmp := 0;

    Pour i de 0 à n-1 faire
        Pour j de i+1 à m-1 faire
            tmp := M[i][j];
            M[i][j] := M[j][i];
            M[j][i] := tmp;
        fait;
    fait;
```

On fait la trace avec $M = \begin{pmatrix} 1 & 2 & 3 \\ 4 & 5 & 6 \end{pmatrix}$ :

| $i$ | $j$ | $M[i][j]$ | $M[j][i]$ |
| --- | --- | --------- | --------- |
| 0   | 1   | 2         | 4         |
| 0   | 2   | 3         | 6         |
| 1   | 2   | 6         | 5         |

On a donc bien $M = \begin{pmatrix} 1 & 4 \\ 2 & 5 \\ 3 & 6 \end{pmatrix}$.

> Version de la prof, on admet que la matrice est carrée

```Pascal
procédure transpose(E: n -> Nombre de lignes de la matrice, S-E: M -> Matrice):
    tmp := 0;

    Pour i de 0 à n-2 faire
        Pour j de i+1 à n-1 faire
            échange(M[i][j], M[j][i]);
        fait;
    fait;
```

###### Exercice 24

> Produit de deux matrices (vérifier les dimensions)

```Pascal
procédure produit(E: n -> Nombre de lignes de la matrice A, 
                     m -> Nombre de colonnes de la matrice A, 
                     p -> Nombre de colonnes de la matrice B, 
                  S-E: A -> Matrice, 
                       B -> Matrice, 
                  S: C -> Matrice):
    tmp := 0;

    Pour i de 0 à n-1 faire
        Pour j de 0 à p-1 faire
            tmp := 0;

            Pour k de 0 à m-1 faire
                tmp := tmp + A[i][k] * B[k][j];
            fait;

            C[i][j] := tmp;
        fait;
    fait;
```

##### Problèmes

Rappels, les problèmes sont ici :

![Problèmes](Img/Problèmes.png)

###### Problème 1

> Un ensemble de valeurs numériques est implanté dans un tableau $T$ à une dimension. Écrire l'algorithme qui recherche si la valeur $v$ est présente dans le tableau et fournit son indice.

*Note : on renvoie $-1$ si la valeur n'est pas trouvée.*

```Pascal
fonction recherche(E: T -> Tableau,
                     n -> Nombre d'éléments du tableau,
                     v -> Valeur à rechercher):
    i := 0;

    Tant que i < n et T[i] != v faire
        i := i + 1;
    fait;

    Si i < n alors
        retourne i;
    Sinon
        retourne -1;
    fsi;
Fin;
```

On fait la trace avec $T = [1, 2, 3, 4, 5]$, $v = 3$ et $n = 5$ :

| $i$ | $T[i]$ | $T[i] \neq v$ | $i < n$ |
| --- | ------ | ------------- | ------- |
| 0   | 1      | `VRAI`        | `VRAI`  |
| 1   | 2      | `VRAI`        | `VRAI`  |
| 2   | 3      | `FAUX`        | `VRAI`  |

On a donc bien $recherche(T, 3) = 2$.

###### Problème 2

> Écrire l'algorithme d'inversion d'un tableau à une dimension sur lui même.

```Pascal
procédure inverse(E: T -> Tableau, n -> Nombre d'éléments du tableau):
    tmp := 0;

    Pour i de 0 à n/2 faire
        tmp := T[i];
        T[i] := T[n-i-1];
        T[n-i-1] := tmp;
    fait;
Fin;
```

On fait la trace avec $T = [1, 2, 3, 4, 5]$ et $n = 5$ :

| $i$ | $T[i]$ | $T[n-i-1]$ |
| --- | ------ | ---------- |
| 0   | 1      | 5          |
| 1   | 2      | 4          |
| 2   | 3      | 3          |
| 3   | 4      | 2          |
| 4   | 5      | 1          |

On a donc bien $T = [5, 4, 3, 2, 1]$.

###### Problème 3

> Écrire l'algorithme qui recherche la monotonie croissante maximale (suite de valeurs croissantes la plus longue) dans un tableau à une dimension, et en fournit l'indice de départ et la longueur.

```Pascal
procédure monotonieCroissante(E: T -> Tableau,
                                n -> Nombre d'éléments du tableau,
                                S-E: deb -> Indice de départ,
                                long -> Longueur de la monotonie):
    deb := 0;
    long := 1;
    tmp := 1;

    Pour i de 1 à n-1 faire
        Si T[i] >= T[i-1] alors
            tmp := tmp + 1;
        Sinon
            Si tmp > long alors
                long := tmp;
                deb := i - tmp;
            fsi;

            tmp := 1;
        fsi;
    fait;

    Si tmp > long alors
        long := tmp;
        deb := n - tmp;
    fsi;
Fin;
```

###### Problème 4

> Écrire l'algorithme de tri de tableau à une dimension selon le principe suivant : <br>
> 1) on effectue des passages successifs sur le tableau de la fin vers le début, <br> 
> 2) à chaque étape, si deux éléments sont en ordre inverse, on les échange, ainsi l'élément minimal remonte (comme une bulle d'air) vers le début du tableau, <br>
> 3) au $i$-ème passage, on ne traite que la partie comprise entre la fin du tableau et la $i$-eme position. Si l'on constate à l'étape $i$ qu'aucun échange a été effectué, le tableau est trié et l'algorithme s'arrête.

```Pascal
procédure triBulle(E-S: T -> Tableau, n -> Nombre d'éléments du tableau):
    i := 0;
    tmp := 0;
    tri := FAUX;

    Tant que tri = FAUX faire
        tri := VRAI;

        Pour i de 0 à n-2 faire
            Si T[i] > T[i+1] alors
                tmp := T[i];
                T[i] := T[i+1];
                T[i+1] := tmp;
                tri := FAUX;
            fsi;
        fait;
    fait;
Fin;
```

###### Problème 5

> Écrire l'algorithme de fusion (ou interclassement) de deux tableaux à une dimension triés par ordre croissant. <br>
> Le résultat sera placé dans un troisème tableau.

```Pascal
procédure fusion(E: T1 -> Tableau,
                    T2 -> Tableau,
                    n1 -> Nombre d'éléments du tableau 1,
                    n2 -> Nombre d'éléments du tableau 2,
                    S-E: T3 -> Tableau):
    i := 0;
    j := 0;
    k := 0;

    Tant que i < n1 et j < n2 faire
        Si T1[i] < T2[j] alors
            T3[k] := T1[i];
            i := i + 1;
        Sinon
            T3[k] := T2[j];
            j := j + 1;
        fsi;

        k := k + 1;
    fait;

    // On copie le reste du tableau 1
    Tant que i < n1 faire
        T3[k] := T1[i];
        i := i + 1;
        k := k + 1;
    fait;

    // On copie le reste du tableau 2
    Tant que j < n2 faire
        T3[k] := T2[j];
        j := j + 1;
        k := k + 1;
    fait;
Fin;
```

On fait la trace avec $T_1 = [1, 3, 5, 7, 9]$ et $T_2 = [2, 4, 6, 8, 10]$ :

| $i$ | $j$ | $k$ | $T_1[i]$ | $T_2[j]$ | $T_3[k]$ |
| --- | --- | --- | -------- | -------- | -------- |
| 0   | 0   | 0   | 1        | 2        | 1        |
| 1   | 0   | 1   | 3        | 2        | 2        |
| 1   | 1   | 2   | 3        | 4        | 3        |
| 1   | 2   | 3   | 3        | 6        | 4        |
| 1   | 3   | 4   | 3        | 8        | 5        |
| 1   | 4   | 5   | 3        | 10       | 6        |
| 2   | 4   | 6   | 5        | 10       | 7        |
| 3   | 4   | 7   | 7        | 10       | 8        |
| 4   | 4   | 8   | 9        | 10       | 9        |
| 5   | 4   | 9   | -        | 10       | 10       |

###### Problème 6
 
> Un carré magique est un tableau carré dont la somme de chaque de chaque ligne est égale à la somme de chaque colonne, elle même égale à la somme de chaque diagonale et dont les composants sont les $n^2$ premiers nombres entiers, $n$ étant le nombre de lignes et de colonnes du tableau.<br>
Lorsque n est impair, il existe un algorithme simple qui est le suivant :

- on place 1 dans la case qui est juste au dessus de celle du milieu;
- on continue ensuite selon une diagonale ascendante direction nord est;
- si l'on tombe ainsi hors du tableau, 3 cas sont à considérer:
  - l'indice de colonne tombe en dehors des limites (à gauche) : gardant le même indice ligne, on choisit la première colonne à droite; si l'on sort à droite, on choisit la première colonne, à gauche;
  - l'indice ligne qui tombe en dehors des limites, au dessus : gardant le même indice colonne, on choisit la ligne du bas ;
  - les 2 indices sortent simultanément en dehors des limites (extrémité de la diagonale nord est): on applique successivement les 2 règles ci dessus dans n'importe quel ordre;
- si l'on tombe sur une case déjà occupée, on choisit une direction diagonale nord ouest à partir de la case occupée.
 
> Ecrire l'algorithme qui permet de construire un carré magique d'ordre impair.

*Pas fonctionnel :(*

```Pascal
procédure carreMagique(E: n -> Nombre de lignes et de colonnes du carré magique, S-E: M -> Matrice):
    i := n/2; // Représente le numéro de la ligne
    j := n-1; // Représente le numéro de la colonne 

    // On suppose n impair

    k := 2;

    // On marque la première case
    M[i][j] := 1;

    peutEcrire := FAUX;

    Tant que k <= n faire

        peutEcrire := VRAI;

        // On met à jour les coordonnées
        i := i - 1;
        j := j + 1;

        // On vérifie si on est sorti du tableau
        Si j < 0 alors
            j := n-1;
        fsi;

        Si j > n-1 alors
            j := 0;
        fsi;

        Si i < 0 alors
            i := n-1;
        fsi;

        Si i > n-1 alors
            i := 0;
        fsi;

        // On vérifie si la case est déjà occupée
        Si M[i][j] != 0 alors
            i := i + 1;
            j := j - 2;

            peutEcrire := FAUX;
        fsi;

        // On écrit dans la case
        Si peutEcrire = VRAI alors
            M[i][j] := k;
            k := k + 1;
        fsi;

    fait;

Fin;
```

On fait la trace avec $n = 3$ :

| $k$ | $i$ | $j$ | $M[i][j]$ |
| --- | --- | --- | --------- |
| 1   | 1   | 2   | 1         |
| 2   | 0   | 0   | 2         |
| 3   | 2   | 1   | 3         |
| 4   | 1   | 0   | 4         |
| 5   | 0   | 1   | 5         |
| 6   | 2   | 2   | 6         |
| 7   | 1   | 1   | 7         |
| 8   | 0   | 2   | 8         |
| 9   | 2   | 0   | 9         |

On a donc bien $M = \begin{pmatrix} 8 & 1 & 6 \\ 3 & 5 & 7 \\ 4 & 9 & 2 \end{pmatrix}$.

###### Problème 7

> Un championnat de football comporte $N$ villes. On enregistre dans un tableau $M$ à 2 dimensions les matchs qui ont été joués : $Mij = 1$ si le match dans la ville $i$ contre la ville $j$ a eu lieu (match aller si $i < j$); de même, $Mji$ représente le match retour. Ecrire un algorithme qui écrit la liste de matchs qui restent à jouer.

```Pascal
Procedure foot(E: T,m)
    Pour i de 0 à n-1 faire
        Pour j de i+1 à n-1 faire
            Si M[i][j] = 0 et M[j][i] = 0 alors
                Ecrire("Match aller-retour : ", i, " contre ", j);
            Sinon Si T[j][i] = 0 alors
                Ecrire("Match retour : ", j, " contre ", i);
            fsi;
        fait;
    fait;
Fin;
```

###### Problème 8

> Une phrase est enregistrée dans un tableau à une dimension à raison d'un caractère par case. Chaque mot est séparé par un espace.

- Écrire un algorithme qui compte et écrit le nombre de lettres de chaque mot sachant que la phrase contient $N$ caractères et ne se finit pas par un espace.
- Même algorithme lorsqu'on ne connaît pas la longueur et que la phrase se termine par un point.

```Pascal
procédure compteLettres(E: T -> Tableau, n -> Nombre de caractères de la phrase):
    nbLettres := 0;

    i := 0;

    Tant que i < n faire
        Tant que T[i] != ' ' ou i < n faire
            nbLettres := nbLettres + 1;
            i := i + 1;
        fait;

        Ecrire("Nombre de lettres : ", nbLettres);

        nbLettres := 0;

    fait;
Fin;
```

On fait la trace avec $T = [C, e, c, i, , e, s, t, , u, n, , t, e, s, t, .]$ et $n = 16$ :

| $i$ | $T[i]$ | $nbLettres$ | $T[i] \neq ' '$ | $i < n$ |
| --- | ------ | ----------- | --------------- | ------- |
| 0   | C      | 1           | `VRAI`          | `VRAI`  |
| 1   | e      | 2           | `VRAI`          | `VRAI`  |
| 2   | c      | 3           | `VRAI`          | `VRAI`  |
| 3   | i      | 4           | `VRAI`          | `VRAI`  |
| 4   |        | 0           | `FAUX`          | `VRAI`  |
| 5   | e      | 1           | `VRAI`          | `VRAI`  |
| 6   | s      | 2           | `VRAI`          | `VRAI`  |
| 7   | t      | 3           | `VRAI`          | `VRAI`  |
| 8   |        | 0           | `FAUX`          | `VRAI`  |
| 9   | u      | 1           | `VRAI`          | `VRAI`  |
| 10  | n      | 2           | `VRAI`          | `VRAI`  |
| 11  |        | 0           | `FAUX`          | `VRAI`  |
| 12  | t      | 1           | `VRAI`          | `VRAI`  |
| 13  | e      | 2           | `VRAI`          | `VRAI`  |
| 14  | s      | 3           | `VRAI`          | `VRAI`  |
| 15  | t      | 4           | `VRAI`          | `VRAI`  |
| 16  | .      | -           | `FAUX`          | `FAUX`  |


On a donc bien $compteLettres(T) = [4, 3, 2, 1]$.

> On suppose que la phrase se termine par un point.

```Pascal
procédure compteLettres(E: T -> Tableau):
    nbLettres := 0;

    i := 0;
    
    Tant que T[i] != '.' faire
        Tant que T[i] != ' ' ou T[i] != '.' faire
            nbLettres := nbLettres + 1;
            i := i + 1;
        fait;

        Ecrire("Nombre de lettres : ", nbLettres);

        nbLettres := 0;
    fait;
Fin;
```

On fait la trace avec $T = [C, e, c, i, , e, s, t, , u, n, , t, e, s, t, .]$ :

| $i$ | $T[i]$ | $nbLettres$ | $T[i] \neq '.'$ | $T[i] \neq ' '$ |
| --- | ------ | ----------- | --------------- | --------------- |
| 0   | C      | 1           | `VRAI`          | `VRAI`          |
| 1   | e      | 2           | `VRAI`          | `VRAI`          |
| 2   | c      | 3           | `VRAI`          | `VRAI`          |
| 3   | i      | 4           | `VRAI`          | `VRAI`          |
| 4   |        | 0           | `VRAI`          | `FAUX`          |
| 5   | e      | 1           | `VRAI`          | `VRAI`          |
| 6   | s      | 2           | `VRAI`          | `VRAI`          |
| 7   | t      | 3           | `VRAI`          | `VRAI`          |
| 8   |        | 0           | `VRAI`          | `FAUX`          |
| 9   | u      | 1           | `VRAI`          | `VRAI`          |
| 10  | n      | 2           | `VRAI`          | `VRAI`          |
| 11  |        | 0           | `VRAI`          | `FAUX`          |
| 12  | t      | 1           | `VRAI`          | `VRAI`          |
| 13  | e      | 2           | `VRAI`          | `VRAI`          |
| 14  | s      | 3           | `VRAI`          | `VRAI`          |
| 15  | t      | 4           | `VRAI`          | `VRAI`          |
| 16  | .      | -           | `FAUX`          | -               |

On a donc bien $compteLettres(T) = [4, 3, 2, 1]$.
