---
title: Mécanique Quantique ZZ1
author: VAN DE MERGHEL Robin
date: 2023
---

# Notes pour les évaluations

- Contrôle continu n°1 :

| Groupe | Date       |
|--------|------------|
| G1     | 11/10/2023 |
| G2     | 10/10/2023 |
| G3     | 09/10/2023 |

- Faire le test essai
- Revoir le cours
  - feuille TD n°1
  - Chercher l'exercice n°1

# Nombres quantiques définissant l'identité d'un électron dans un atome

| Nombre quantique azimuthal | Correspondance | 
|----------------------------|----------------|
| $l=0$                      | $s$            |
| $l=1$                      | $p$            |
| $l=2$                      | $d$            |
| $l=3$                      | $f$            |
| $l=4$                      | $g$            |
| $l=5$                      | $h$            |
| $l=6$                      | $i$            |

*Note : les couches électroniques ?*

# Remplissage des couches et sous couches électroniques

On a $n=1$, donc 2 places : $2n^2=2$ places. On a donc $l=0$ :

- Couche $n=2$ : $2^2\times 2=8$ places. On a donc $l=0 \ (s)$ et $l=1 \ (p)$.

###### Exemple

On prend $n=3, l=1$ :

$3s^2_{+1}$, ici $3$ car $n=3$, $s$ car $l=0$ et $+1$ car $m_l=1$.

