---
title: Traitement du signal ZZ1
author: VAN DE MERGHEL Robin
date: 2023
---

# Classification de Signaux

## Notion de signal

### Des exemples

Pour commencer, voici quelques exemples de signaux :

- Tension
- Température
- Voix
- Musique
- Cours de la bourse

### Définition

Un signal est une quantité mesurable porteuse d'information.

C'est une représentation mathématique avec un signal abstrait (souvent $x(t)$ fonction réelle ou complexe).

## Quelques propriétés

### Discret-Analogique (continu)

$x(t)$ est continue avec $t\in \mathbb{R}$.

$$x(k\Delta T) = x[k] = x_k \ \ \ \ \ \ \ k\in \mathbb{Z} \text{ ou } \mathbb{N}$$

###### Exemple

La tension électrique dans un circuit $u(t)$ continu.

###### Exemple

Le nombre d'étudiants à 13h30 (discret) le mardi en G019.

##### Signal quantifié

![Echantillonage](img/Echantillonage.jpg)

###### Exemple

Un son de qualité CD : $fc$ est la fréquence d'échantillonage à $44.1kHz$, d'où :

$$T = \frac{1}{fc} = 22.7\mu s$$

C'est codé sur 16 bits.

### Déterministe-Aleatoire

#### Déterministe

Un signal dit déterministe dont l'évolution est exactelent prévisible par une équation mathématique.

#### Aléatoire

Le signal dont ne peut pas prédire exactement les valeurs futures.

### Bruit

Un signal perturbaeur produit par l'environnement ou un système imparfait. Il est souvent aléatoire.

## Energie - Puissance

### Energie d'un signal

Un signal $x(t)$ est observé pendant une durée $T$ a une énergie :

$$E_T(x) = \int_{t}^{t+T} |x(u)|^2 du$$

L'énergie totale du signal $x(t)$ :

$$E(x) = \int_{-\infty}^{+\infty} |x(u)|^2 du$$

Avec des signaux d'énergie finie ou infinie.

### Puissance d'un signal

La puissance moyenne d'un signal $x(t)$ entre $t$ et $t+T$ :

$$P_T(x) = \frac{1}{T} \int_{t}^{t+T} |x(u)|^2 du = \frac{E_T(x)}{T}$$

La puissance *totale* d'une signal $x(t)$ est :

$$P(x) = \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} |x(u)|^2 du$$

## Classification

![Classification](img/Classification.jpg)

## Cas des signaux 

### Énergie d'un signal périodique

$x_{T_0}(t)$ est un signal périodique de période $T_0$.

On a donc :

$$E(x_{T_0}) = \int_{-\infty}^{+\infty} |x(u)|^2 du = \infty$$

### Puissance d'un signal périodique

$$P(x_{T_0}) = \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} |x(u)|^2 du $$

On peut montrer que les signaux périodiques sont à puissance finie avec :

$$P(x_{T_0}) = \frac{E_{T_0}(x_{T_0})}{T_0}$$

###### Exercice 1

> Représenter graphiquement et calculer l'énergie la puissance des signaux suivants :

- $x_1(t) = \sin(t)$
- $x_2(t) = X_0 \neq 0$
- $x_3(t) = \mathbb{1}_{[0, +\infty[}(t)e^{-t}$

> Conclure sur la catégorie de ces signaux : signal à énergie finie ou à puissance finie.

| Signal             | Energie               | Puissance     |
|--------------------|-----------------------|---------------|
| $x_1(t) = \sin(t)$ | $\infty$ (périodique) | $\frac{1}{2}$ |
| $x_2(t) = X_0$     | $X_0^2$               | $X_0^2$       |
| $x_3(t) = \mathbb{1}_{[0, +\infty[}(t)e^{-t}$ | $\frac{1}{2}$ | $\frac{1}{2}$ |

Pour $P(x_1)$, on a :

$$\begin{align}
P(x_1) &= \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} |\sin(u)|^2 du \\
&= \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} \frac{1-\cos(2u)}{2} du \\
&= \lim_{T\to\infty} \frac{1}{T}\times \left[ \frac{u}{2} - \frac{\sin(2u)}{4} \right]_{-\frac{T}{2}}^{+\frac{T}{2}} \\
&= \lim_{T\to\infty} \frac{1}{T}\times \left(\frac{T}{4} - (-\frac{T}{4}) - \frac{\sin(T)}{4} + \frac{\sin(-T)}{4} \right) \\
&= \lim_{T\to\infty} \frac{1}{T}\times \left(\frac{T}{2} - \frac{\sin(T)}{4} + \frac{\sin(T)}{4} \right) & \text{ car } \sin(T) \text{ est borné } \\
&= \lim_{T\to\infty} \frac{1}{T}\times \frac{T}{2} \\
\end{align}$$

Donc :

$$P(x_1) = \frac{1}{2}$$

Pour $P(x_2)$, on a :

$$\begin{align}
P(x_2) &= \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} |X_0|^2 du \\
&= \lim_{T\to\infty} \frac{1}{T}\times X_0^2 \times T
\end{align}$$

Donc :

$$P(x_2) = X_0^2$$

Pour $P(x_3)$, on a :

$$\begin{align}
P(x_3) &= \lim_{T\to\infty} \frac{1}{T}\times \int_{-\frac{T}{2}}^{+\frac{T}{2}} \mathbb{1}_{[0, +\infty[}(u)e^{-u} du \\
&= \left[-\frac{1}{2}e^{-2u}\right]_0^{+\infty} \\
&= \frac{1}{2}
\end{align}$$

Donc :

$$P(x_3) = \frac{1}{2}$$

###### Exercice 2

> Donnez une expression mathématique pour modéliser les signaux de la figure 1-1.

> Calculer la puissance totale de ces signaux.

| Signal             | Expression mathématique | Puissance     |
|--------------------|-------------------------|---------------|
| $x_{sin}(t)$       | $\sin(\frac{2\pi}{T_0}t)$ | $+\infty$ |
| $x_{rect}(t)$      | $\begin{cases} -1 & \text{ si } t \in [-\frac{T_0}{2}, \frac{T_0}{2}] \\ 1 & \text{ sinon } \end{cases}$ | $\frac{1}{T_0}$ |
| $x_{tri}(t)$       | $\begin{cases} 1 + 4 \frac{t}{T_0} & \text{ si } t \in [-\frac{T_0}{2}, 0] \\ 1 - 4 \frac{t}{T_0} & \text{ sinon } t \in [0, \frac{T_0}{2}] \\ \end{cases}$ | $\frac{1}{T_0}$ |

$$\begin{align}
E_{T_0}(x_{sin}) &= \int_{-\frac{T_0}{2}}^{+\frac{T_0}{2}} |\sin(\frac{2\pi}{T_0}t)|^2 dt\\
&= \int_{-\frac{T_0}{2}}^{+\frac{T_0}{2}} \frac{1-\cos(\frac{4\pi}{T_0}t)}{2} dt \\
&= \left[\frac{u}{2} - \frac{\sin(\frac{4\pi}{T_0}u)}{2 \times \frac{4\pi}{T_0}}\right]_{-\frac{T_0}{2}}^{+\frac{T_0}{2}} \\
&= \frac{T_0}{2} \\
\end{align}$$


D'où :

$$P(x_{sin}) = \frac{E_{T_0}(x_{sin})}{T_0} = \frac{T_0}{2T_0} = \frac{1}{2}$$

# Signaux déterministes continus

## Transformée de Fourier

### Définition

La transformée de Fourier d'un signal $x(t)$ est :

$$X(\nu) = \int_{-\infty}^{+\infty} x(t) e^{-j2\pi\nu t} dt$$

On note $X(\nu) = TF[x(t)]$ avec $\nu$ la fréquence, $j^2 = -1$.

De même, $x(t) = TF^{-1}[X(\nu)]$.

Ainsi :

$$x(t) = \int_{-\infty}^{+\infty} X(\nu) e^{j2\pi\nu t} d\nu$$

###### Exercice 3

$$\begin{align}
P(\nu) &= \int_{-\frac{\tau}{2}}^{+\frac{\tau}{2}} 1 \times e^{-j2\pi\nu t} dt \\
&= \left[ \frac{e^{-j2\pi\nu t}}{-j2\pi\nu} \right]_{-\frac{\tau}{2}}^{+\frac{\tau}{2}} \\
&= \frac{e^{-j\pi\nu\tau} - e^{j\pi\nu\tau}}{-j2\pi\nu} \\  
&= \frac{2j\sin(\pi\nu\tau)}{2\pi\nu} \\
&= \frac{\sin(\pi\nu\tau)}{\pi\nu} \\
S(\nu) &= T \times \frac{\sin(\pi\nu\tau)}{\pi\nu} \\
\end{align}$$

Donc $P(\nu) = T \sin_c(\pi\nu\tau)$.

![Sinus cardinal](img/Sinc.png)

![Module du sinus cardinal](img/ModuleSinc.png)

## Principales propriétés

### Théorème de Gabor

$$\Delta t \times \Delta \nu = cte$$

Signal limité en temps est étalé en fréquence, le signal étalé en temps est limité en fréquence.

### Intégrale

$$X(0) = \int_{-\infty}^{+\infty} x(t) dt$$

$$x(0) = \int_{-\infty}^{+\infty} X(\nu) d\nu$$

### Linéarité

$$\lambda_1 x_1(t) + \lambda_2 x_2(t) \leftrightarrow \lambda_1 X_1(\nu) + \lambda_2 X_2(\nu)$$

### Inversion

$$x(-t) \leftrightarrow X(-\nu)$$

### Conjugaison complexe

$$\overline{x(t)} \leftrightarrow \overline{X(-\nu)}$$

### Symétrie

S**i $x(t) \in \mathbb{R}$ (signal réel)**, alors $Im(x(t)) = 0$, $\forall t\in \mathbb{R}, x(t) = \overline{x(t)}$. Or on a :

$$\begin{align}
X(\nu) &= \int_{-\infty}^{+\infty} x(t) e^{-j2\pi\nu t} dt \\
&= \int_{-\infty}^{+\infty} \overline{x(t)} e^{-j2\pi\nu t} dt \\
&= \overline{\int_{-\infty}^{+\infty} x(t) e^{-j2\pi\nu t} dt} \\
&= \overline{X(-\nu)} & \text{ symétrie Hermitienne}
\end{align}$$

Si $X(\nu) = A(\nu) + jB(\nu)$, alors :

$$\overline{X(-\nu)} = A(\nu) - jB(-\nu)$$

Ainsi :

- $A(\nu) = A(-\nu)$
- $B(\nu) = -B(-\nu)$

Et donc on a les propriétés suivantes :

- $Re(X(\nu))$ est paire
- $Im(X(\nu))$ est impaire

Ou encore :

- $|X(\nu)|$ est paire
- $\arg(X(\nu))$ est impaire

**Si $x(t)$ est réel et pair**, alors :

$$\begin{align}
x(t)\in \mathbb{R}, \forall t \ x(-t) &= x(t)\\
X(\nu) &= \overline{X(-\nu)} \\
&= \overline{-X(\nu)} \\
\end{align}$$

Ainsi $X(\nu)$ est imaginaire pur.

**Si $x(t)$ est réel et impaire**, alors :

On a $X(\nu)$ un imaginaire pur et impaire.

### Dilatation en temps ou en fréquence

$$x(at) \longrightarrow \frac{1}{a}X\left(\frac{\nu}{a}\right)$$

### Translation en temps ou en fréquence

Un décalage en temps :

$$g(t) = x(t-\tau) \longleftrightarrow \gamma(\nu) = X(\nu)e^{-j2\pi\nu\tau}$$

![Translation en temps](img/TranslationTempsFrequence.png)

> On voit que la deuxième courbe a un décalage de $\tau = 2$.

Et maintenant un décalage en fréquence :

$$z(t) = x(t)e^{j2\pi\nu_0t} \longleftrightarrow Z(\nu) = X(\nu - \nu_0)$$

![Translation en fréquence](img/TranslationTempsFrequence2.png)

> On voit que la deuxième courbe a un décalage de $\nu_0 = 5$

> Remarque : la translation fréquentielle c'est la base des modulations mais $e^{j2\pi F t}$ n'est pas un signal réel donc on utilisera :

$$\cos(2\pi F t) = \frac{e^{j2\pi F t} + e^{-j2\pi F t}}{2}$$

C'est deux translations temporelles : $\frac{1}{2}(X(\nu + F) + X(\nu - F))$.

### Dérivation - Intégration

$$y(t) = x'(t) = \frac{dx(t)}{dt} \longleftrightarrow Y(\nu) = j2\pi\nu X(\nu)$$

> Remarque : lien avec la transformée de Laplace : dériver revient à multiplier par $p$

$$z(t) = \int_{-\infty}^{t} x(t) dt \longleftrightarrow Z(\nu) = \frac{1}{j2\pi\nu}X(\nu)$$

### Égalité de Parseval

Si $x$ et $y$ sont deux signaux d'énergie finie, alors :

$$\int_{-\infty}^{+\infty} x(t)\overline{y(t)} dt = \int_{-\infty}^{+\infty} X(\nu)\overline{Y(\nu)} d\nu$$

Si on prend $y(t) = x(t)$, on a :

$$\int_{-\infty}^{+\infty} |x(t)|^2 dt = \int_{-\infty}^{+\infty} |X(\nu)|^2 d\nu$$

Le membre de gauche est la formule de l'énergie totale de $x(t)$. $|X(\nu)|^2$ est la densité spectrale d'énergie de $x(t)$.

###### Exercice 4

> Donner une expression analytique de la fonction associée à ce signal.

