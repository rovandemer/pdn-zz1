---
title: Présentation ZZ1
author: VAN DE MERGHEL Robin
date: 4 septembre 2023
--- 

# Introduction sur l'ISIMA

## Validation

- TOEIC 800
- Valider les 3 années
- 17 semaines à l'international

*UE = Unité d'enseignement*

- Passage automatique en 2e année si toutes les UE sont validées
- La note de la deuxième session remplace la note de la session 1
- Un redoublement possible uniquement

## Projet Stage

- Projet / Stage
  - Pas de note (mentions à la place, validée si assez-bien)
  - Pas rattrapable
- Ouverture et engagement
  - (pas de note aussi)
  - On doit avoir 3 pts selon une liste
  
## Les filières

- 24-28 places dans chaque filière
- Étudiants classent de 1 à 5 les filières

# Semestres 1 et 2

- 4 UE fondamentales
  - Sciences humaines et sociales
    - LV2
    - Anglais
  - Info
    - S1
      - Structure de données
      - C / Unix
      - Programmation fonctionnelle
      - Automates
    - S2
      - Structure de données
      - Base de données
      - Système d'exploitation
      - Sensibilisation à la cybersécurité
  - Aide à la décision et mathématiques appliquées
    - S1
      - Théorie des graphes
      - Analyse numérique
      - Probabilités
      - Soutien math
    - S2
      - Analyse de données
      - Proba
      - Programmation Linéaire
  - Sciences pour l'ingénieur
- En S2 + Projet / Stage