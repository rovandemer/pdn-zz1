---
title: Introduction au Deep Learning
author: VAN DE MERGHEL Robin
date: 4 septembre 2023
---

Le *Deep Learning* est basé sur les réseaux de neurones. Les réseaux de neurones sont nés de l'idée de s'inspirer du fonctionnement du cerveau.

# Utilisation

- Correcteur de texte
- Classification d'images
- Reconnaissance faciale
- Voiture autonome

La complexité de l'intelligence artificielle ne fait qu'évoluer : avant on pouvait détécter des personnes sous une seule façon, maintenant on peut détecter par exemple une personne avec des lunettes et sans.

# Fonctionnement

Un exemple de réseau de neurone :

![Réseau de neurone](https://miro.medium.com/max/5998/1*fx3TuPJ_N49W4HAqpRU8TA.jpeg)

- Les premiers neurones à gauche sont les neurones d'entrée (correspondent à la question : "Est-ce que c'est un chien ?") : *input layer*
- Les neurones à droite sont les neurones de sortie (correspondent à la réponse : "Oui, c'est un chien") : *output layer*
- Les neurones au milieu sont les neurones cachés (ce sont des paramètres du réseau de neurones) : *hidden layer*

Les informations vont se propagées dans le réseau de neurones, et à la fin on va avoir une réponse. En sortie on a des probabilités.

![Neurone du réseau](https://miro.medium.com/v2/resize:fit:1302/1*UA30b0mJUPYoPvN8yJr2iQ.jpeg)

On envoie les données $x_1, x_2, \dots, x_n$ dans une fonction d'activation $f$ qui va donner une valeur $y$, soit $f(x_1, x_2, \dots, x_n) = y$. On obtient :

$$f(x_1,x_2,\dots,x_3) = x_1 \cdot w_1 + x_2 \cdot w_2 + \dots + x_n \cdot w_n = y$$

## Sigmoïde

Une fonction d'activation est une fonction qui va prendre en entrée un nombre réel et va donner en sortie un nombre réel. Elle va permettre de déterminer si le neurone est activé ou non. Une fonction classique est la fonction *sigmoïde* :

$$f(x) = \frac{1}{1 + e^{-x}}$$

![Fonction sigmoid](https://i.stack.imgur.com/inMoa.png)

Le problème de cette fonction est que, comme on peut le voir sur la dérivée, qu'on aie une valeur positive importante ou négative importante, la dérivée va être très proche de 0. Cela va poser problème lors de la *rétropropagation* (que l'on verra plus tard).

## $Tanh$

Une autre fonction d'activation est la fonction $tanh$ :

![Fonction tanh et sigmoid](https://i0.wp.com/arshad-kazi.com/wp-content/uploads/2021/03/tanh.jpg?fit=512%2C284&ssl=1)

Elle a des similitudes avec la fonction sigmoid, mais elle a des valeurs négatives. Par rapport à la sigmoid, on a moins de pertes de gradient.

## ReLU

Une autre fonction d'activation est la fonction *ReLU* :

![Fonction Relu](https://www.nomidl.com/wp-content/uploads/2022/04/image-10.png)

# Entraînement

L'entraînement c'est le fait de trouver un ensemble de poids optimal pour avoir une *fonction de coût* minimale $J(w)$. 

On a un ensemble $X$ qui représente les entrées par exemple une données, et on a un ensemble $Y$ associé qui correspond au résultat attendu. On va donc avoir une fonction $f$ qui va prendre en entrée $X$ et qui va donner en sortie $Y$. On va donc avoir une fonction de coût $J(w)$ qui va dépendre des poids $w$ et qui va permettre de déterminer si la fonction $f$ est bonne ou non.

On a par exemple l'erreur quadratique moyenne :

$$E(X, \alpha) = \frac{1}{2N} \sum_{i=1}^N (f(x_i, \alpha) - y_i)^2$$

## Exemple

On a un réseau de neurone :

![Réseau de neurone](./Exemple1.png)

On a :

$$a = ((1\times 1)) + (6 \times (-1)) + 10 = 5$$
$$o = g(5)$$
$$o = (5 \times .5) - 1 = 1.5$$ ...


# Réseaux convolutifs

Les réseaux convolutifs sont des réseaux de neurones qui vont permettre de traiter des images. Il y a plein de petits réseaux de neurones qui vont traiter des parties de l'image. On va avoir des filtres qui vont permettre de détecter des formes.

![Réseau convolutif](https://www.researchgate.net/publication/330995099/figure/fig5/AS:724726251024392@1549799611775/Architecture-classique-dun-reseau-de-neurones-convolutif-Une-image-est-fournie-en.ppm)

À partir de tout ces filtres, on fait une image intermédiaire (carte de caractéristiques) qui va permettre de détecter des formes plus complexes.

## Convolution discrète

Ça permet de pouvoir travailler sur des petites parties de l'image. On va avoir une matrice de convolution qui va permettre de faire une opération sur une partie de l'image. On va faire une opération sur une partie de l'image, et on va faire glisser la matrice de convolution sur toute l'image.

Exemple :

$$(K \times I)_{r,s} = \sum_{i=0}^{K-1} \sum_{j=0}^{K-1} K_{i,j} \cdot I_{r+i, s+j}$$

![Gif de convolution](https://miro.medium.com/v2/resize:fit:1400/1*NUu5msHE4z01YQsRFiVBOw.gif)