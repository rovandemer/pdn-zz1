---
title: Moteur texte du jeu Mastermind
author: VAN DE MERGHEL Robin
date: 2023-2024
---

Source : [https://perso.isima.fr/~loyon/unixc/tpc-mastertexte.php](https://perso.isima.fr/~loyon/unixc/tpc-mastertexte.php)

# Principe du jeu

> Le principe est très simple : Il faut découvrir une combinaison de couleurs. Pour chaque proposition, on sait uniquement quels sont les pions bien placés et les pions mal placés.
> Le jeu initial demande des combinaisons de couleurs à choisir parmi 6. une case ne peut être vide. Le nombre maximum de propositions est de 10.
> Pour cette version texte, les couleurs sont remplacées par un chiffre entre 1 et 6. On réserve la valeur 0 à une case vide.


> Nous aurons besoin de plusieurs variables :

- Un tableau "solution" qui va contenir la combinaison à trouver, soit un tableau de 4 entiers pour la version standard.
- Une matrice "plateau" : chaque ligne contient une proposition et les nombres de pions bien placés et mal placés. Pour la version standard du jeu, il s'agit donc d'une matrice de 10 lignes et de 4+2 colonnes. 

# Bases du jeu et développement orienté tests

On va devoir commencer à implémenter les fonctions de base du jeu. On a trois fonctions :

- `initialiser_solution()` : génère une combinaison à trouver
- `initialiser_plateau()` : initialise un plateau vierge de toute information
- `compiler_proposition()` : calcule le nombre de pions bien placés et de pions mal placés étant données une proposition et la solution. Le prototype est à adapter.

## Fonctions

Elles sont définies dans `master.h`, mais il va falloir modifier leur signature : dans le teste de l'énoncé, à la ligne $6$, on a :

```c
initialiser_solution(solution);
```

En effet, on voit que la fonction `initialiser_solution` prend un paramètre une solution qui est un tableau de `NB_COLONNES` entiers d'après la ligne $3$ :
```c
int solution[NB_COLONNES];
```

Alors que dans `master.h`, la fonction ne prend pas de paramètre. 

### `initialiser_solution`

La fonction `initialiser_solution` va donc devoir prendre un paramètre, un tableau d'entiers de taille `NB_COLONNES`, et modifier ce tableau pour y mettre une combinaison aléatoire telle que chaque case du tableau contienne un entier entre $1$ et $NB_COULEURS$. La signature de la fonction devient donc :

```c
// Avant modification
void initialiser_solution();

// Après modification
void initialiser_solution(int solution[NB_COLONNES]);
```

On l'implémente dans `master.c` :

```c
// Initialisation de la solution
void initialiser_solution(int solution[NB_COLONNES]) {

  // On va remplir le tableau solution avec les couleurs de 1 à 6
  // Pour l'instant on met dans l'ordre croissant mais on pourra mettre de l'aléatoire
  
   int i;
   for (i = 0; i < NB_COLONNES; i++)
   {
      solution[i] = i + 1;
   }
}
```

### `compiler_proposition`

La fonction `compiler_proposition` va devoir prendre deux paramètres, un tableau d'entiers de taille `NB_COLONNES` (la proposition) et un tableau d'entiers de taille `NB_COLONNES` (la solution). Elle renverra une structure combinaison qui indique le nombre de pions bien placés et le nombre de pions mal placés. La signature de la fonction devient donc :

```c
// Avant modification
void compiler_proposition();

// Après modification
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]);
```

On va d'abord définir la structure `combinaison` dans `master.h`. D'après les lignes $7$ et $8$ de l'exemple `TEST(combinaison1a)`, on voit que la structure est composée de deux entiers : `bienp` et `malp`. De plus, on peut voir que ce sont des entiers :

```c
REQUIRE( 0 == c.bienp );
REQUIRE( 0 == c.malp );
```

Donc la structure `combinaison` est définie comme suit :

```c
typedef struct {
  int bienp;
  int malp;
} combinaison;
```

On l'implémente dans `master.c` :

```c
// Compilation de la proposition
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]) {

    // On compte le nombre de pions bien placés et mal placés
    int bienp = 0;
    int malp = 0;

    // On va parcourir les deux tableaux en même temps
    int i;

    for (i = 0; i < NB_COLONNES; i++)
    {
        // Si les deux cases sont égales, on incrémente bienp
        if (proposition[i] == solution[i])
        {
            bienp++;
        }
        // Sinon elle est mal placée
        else
        {
            malp++;
        }
    }

    // On crée la structure combinaison 
    combinaison c;

    // On met les valeurs dans la structure
    c.bienp = bienp;
    c.malp = malp;

    // On retourne la structure
    return c;

}
```

### `initialiser_plateau`

La fonction `initialiser_plateau` prendra en paramètre un tableau de $10$ lignes et `NB_COLONNES + 2` colonnes. Elle renverra un tableau de $10$ lignes et `NB_COLONNES + 2` colonnes initialisé à $0$. On a `+ 2` colonnes car on a besoin de deux colonnes supplémentaires pour stocker le nombre de pions bien placés et le nombre de pions mal placés. La signature de la fonction devient donc :

```c
// Avant modification
void initialiser_plateau();

// Après modification
void initialiser_plateau(int plateau[10][NB_COLONNES + 2]);
```

On l'implémente dans `master.c` :

```c
// Initialisation du plateau
void initialiser_plateau(int plateau[10][NB_COLONNES + 2]) {

    // On va remplir le tableau plateau avec des 0
    int i;
    int j;
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < NB_COLONNES + 2; j++)
        {
            plateau[i][j] = 0;
        }
    }
}
```

## Tests

On va créer un `makefile` pour compiler les tests. Cela nous permettra de tester notre code plus facilement (oui, on s'avance sur l'une des questions suivantes, mais ça aide).

On crée donc un fichier `makefile` dans lequel on met :

```makefile
OPTIONS=-Wall -Wextra -g
CC=gcc
# On compile tous les fichiers .c : master.c, tests.c, teZZt.c
# - master.c a besoin de master.h
# - tests.c a besoin de master.h et de teZZt.h
# - teZZt.c a besoin de teZZt.h
prog:master.o tests.o teZZt.o
	$(CC) -o prog master.o tests.o teZZt.o -g

# On compile master.c
master.o: master.c master.h
	$(CC) $(OPTIONS) master.c -c

# On compile tests.c
tests.o: tests.c master.h teZZt.h
	$(CC) $(OPTIONS) tests.c -c

# On compile teZZt.c
teZZt.o: teZZt.h teZZt.c
	$(CC) $(OPTIONS) -c teZZt.c

# On supprime les fichiers .o
clean:
	rm -f master.o tests.o teZZt.o *.gch
```

On compile avec `make` et on lance les tests avec `./tests`.

# Moteur du jeu, interface texte et intégration

## Principe

