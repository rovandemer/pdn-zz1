#include "master.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Initialisation de la solution
void initialiser_solution(int solution[NB_COLONNES]) {

  // On va remplir le tableau solution avec les couleurs de 1 à 6
  // Pour l'instant on met dans l'ordre croissant mais on pourra mettre de
  // l'aléatoire

  int i;
  for (i = 0; i < NB_COLONNES; i++) {
    solution[i] = i + 1;
  }
}

// Initialisation du plateau
void initialiser_plateau(int plateau[10][NB_COLONNES + 2]) {

    // On va remplir le tableau plateau avec des 0
    int i;
    int j;
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < NB_COLONNES + 2; j++)
        {
            plateau[i][j] = 0;
        }
    }
}

combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]) {

    // On compte le nombre de pions bien placés et mal placés
    int bienp = 0;
    int malp = 0;

    // On va parcourir les deux tableaux en même temps
    int i;

    for (i = 0; i < NB_COLONNES; i++)
    {
        // Si les deux cases sont égales, on incrémente bienp
        if (proposition[i] == solution[i])
        {
            bienp++;
        }
        // Sinon elle est mal placée
        else
        {
            malp++;
        }
    }

    // On crée la structure combinaison 
    combinaison c;

    // On met les valeurs dans la structure
    c.bienp = bienp;
    c.malp = malp;

    // On retourne la structure
    return c;

}