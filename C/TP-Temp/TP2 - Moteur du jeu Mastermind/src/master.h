#ifndef master_h
#define master_h

/* DECLARATION DES CONSTANTES SYMBOLIQUES */

#define NB_COLONNES 4

/* DECLARATION DES TYPES PERSONNELS */
// et de leur typedef si besoin

typedef struct {
  int bienp;
  int malp;
} combinaison;


/* DECLARATIONS DES METHODES */

void initialiser_solution(int solution[NB_COLONNES]);
combinaison compiler_proposition(int proposition[NB_COLONNES], int solution[NB_COLONNES]);
// void compiler_proposition(); // le prototype est a adapter
// mettre ici les autres declarations

#endif