#include "mon_code.h"

// implementation des fonctions a ecrire
// c est bien DANS CE FICHIER qu'il faut le faire

int pgcd(int a, int b) {
  if (b == 0) { // Si b est nul, alors le pgcd est a
    return a;
  }
  return pgcd(b, a % b); // Sinon, on calcule le pgcd de b et du reste de la division entière de a par b
}

void majuscules(char s[]) {
   // Mettre en majuscules la chaine s
   // Attention, s est un tableau de caracteres
   // /!\ Il peut y avoir des caracteres non alphabetiques

   int i = 0;
   while (s[i] != '\0') {
     if (s[i] >= 'a' && s[i] <= 'z') {
       s[i] = s[i] - 'a' + 'A';
     }
     i++;
   }

   // Comme on a modifie s, il n'est pas necessaire de le retourner
}
