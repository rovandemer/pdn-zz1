---
title: TP1 - Outils
author: VAN DE MERGHEL Robin
date: 2023-2024
---

Source : [https://perso.isima.fr/~loyon/unixc/tpc-01.php](https://perso.isima.fr/~loyon/unixc/tpc-01.php)

# Introduction

*Le dossier de travail est [Part1](./Part1/)*

> Dans ce premier TP de C, nous allons introduire les outils dont nous aurons besoin pour travailler en C cette année.
> - les outils de débogage et de profilage
> - la bibliothèque de tests unitaires

Nous allons tester et corriger le programme suivant :

```c
/*
* Exemple d'utilisation de ddd
* Compiler le programme avec les options
*  -g -Wall -Wextra
*
* - La compilation ne dit rien
* - A l'execution, il est possible de ne rien voir suivant la configuration
* - Le comportement a probleme est signale par valgrind a l execution
*/

#include<stdio.h>

void essai(int j) 
{
	int i;
	
	while(i<10) 
	{
		printf("%d ", i+j );
		++i;
	}
}

int main() 
{
	int j;

    for(j=0; j< 10; ++j)  
    {
    	essai(j);
    	printf("\n");
    }
	
	return 0;
}
```

On compile avec `gcc -g -Wall -Wextra main.c -o main`

# `Valgrind`

`Valgrind` est un outil qui permet de détecter des erreurs dans un programme, notamment des erreurs de mémoire.

Avec la commande `valgrind ./main` :

```bash
==2477== Conditional jump or move depends on uninitialised value(s)
==2477==    at 0x108814: essai (main.c:17)
==2477==    by 0x10883F: main (main.c:30)
```

On voit que la ligne 17 pose problème, on va donc regarder cette ligne.

```c
while(i<10) 
```

`Valgrind` nous indique que `i` n'est pas initialisé, on doit donc l'initialiser.

L'énoncé indique : 

> Vous avez le numéro de ligne, c'est facile maintenant de corriger ! Mais ne le faites pas encore...

# DDD

`DDD` est un outil qui permet de déboguer un programme : on peut lancer le programme ligne par ligne et afficher les valeurs des variables.

*Note : comme je suis sur `MacOS`, je ne peux pas utiliser `ddd`, mais je peux utiliser `gdb`.*

On lance `gdb ./main` et on ajoute un `breakpoint` à la ligne 17 avec `break main.c:17`

```bash
(gdb) break main.c:17
Breakpoint 1 at 0x1087f4: file main.c, line 17.
```

On lance le programme avec `run` et on obtient :

```bash
(gdb) run
Starting program: [...] - Outils/main 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/aarch64-linux-gnu/libthread_db.so.1".

Breakpoint 1, essai (j=0) at main.c:17
17              while(i<10)
```

Ensuite on peut passer à la ligne suivante avec `next` ou `n` :

```bash
(gdb) n
18                      printf("%d ", i+j );
```

On peut aussi afficher la valeur de `i` avec `print i` :

```bash
(gdb) print i
$1 = 65535
```

On voit que `i` n'est pas initialisé, on peut donc corriger le programme en initialisant `i` à `0` :

```c
void essai(int j) 
{
  int i = 0; // On initialise i à 0
  
  while(i<10) 
  {
    printf("%d ", i+j );
    ++i;
  }
}
```

# Bibliothèque de tests

*Le dossier de travail est [Part2](./Part2/)*

> Vous devez récupérer les fichiers de code pour cet exercice : [main.c, mon_code.h, mon_code.c, tezzt.h, tezzt.c]

On peut le récupérer avec :

```bash
git clone https://gitlab.com/kiux/C1B.git
```

> Pour compiler le tout, vous pouvez faire une commande gcc générale mais vous pouvez aussi simplement lancer la commande : `make`

## Premiers tests

> On vous demande de coder la fonction `pgcd()` qui calcule le plus grand diviseur commun entre deux nombres entiers. Si vous complétez le code de la fonction dans le fichier `mon_code.c`, les tests seront réussis (`OK` ou `passed`) et non plus en echec (`KO` ou `failed`) !

On a le code suivant :
 
```c
#include "mon_code.h"

// implementation des fonctions a ecrire
// c est bien DANS CE FICHIER qu'il faut le faire

int pgcd(int a, int b) {
	
   return 0;
}
```

> Au passage, le pgcd de deux nombres $a$ et $b$ vaut $b$ si le reste de la division entière de $a$ par $b$ est nul sinon c'est le pgcd de $b$ et de ce reste (définition récursive).

C'est à dire $pgcd(a, b) = b$ si $a \mod b = 0$ sinon $pgcd(a, b) = pgcd(b, a \mod b)$

On peut donc coder la fonction `pgcd` :

```c
int pgcd(int a, int b) {
  if (b == 0) { // Si b est nul, alors le pgcd est a
    return a;
  }
  return pgcd(b, a % b); // Sinon, on calcule le pgcd de b et du reste de la division entière de a par b
}
```

*Rappel : `a % b` est le reste de la division entière de `a` par `b` (aka le modulo).*

On peut ensuite compiler avec `make` et lancer les tests avec `./prog` et on obtient :

```bash
--- teZZT REPORT ---
  0 test(s) failed
  6 test(s) passed
[pgcd]
```

## Transformer en majuscules

> Vous pouvez ensuite décommenter les tests suivants. Il s'agit de tester la fonction `majuscules()` que vous avez écrite pendant les semaines bloquées (vous pouvez réutiliser ce que vous avez écrit naturellement).

On doit donc coder la fonction `majuscules` :

```c
void majuscules(char s[]) {
   // Mettre en majuscules la chaine s
   // Attention, s est un tableau de caracteres
   // /!\ Il peut y avoir des caracteres non alphabetiques

   int i = 0;
   while (s[i] != '\0') {
     if (s[i] >= 'a' && s[i] <= 'z') {
       s[i] = s[i] - 'a' + 'A';
     }
     i++;
   }

   // Comme on a modifie s, il n'est pas necessaire de le retourner
}
```

On lance le code, et on obtient :

```bash
--- teZZT REPORT ---
  0 test(s) failed
  6 test(s) passed
[maj]
```

*Note : j'ai modifié le code de [`main.c`](./Part2/main.c) car le groupe de tests de `majuscules` n'était pas configurés.*

### Commentaires sur le code

#### Boucle `while`

En `C`, les chaînes de caractères sont des tableaux de caractères, donc on peut les parcourir avec une boucle `while` ou `for`. Mais ici on utilise une boucle `while` car on *ne sait pas combien de caractères* il y a dans la chaîne de caractères.

Une propriété des caractères en `C`, c'est que le dernier caractère d'une chaîne de caractères est le caractère `\0` (ou `NULL`), donc on peut parcourir la chaîne de caractères *jusqu'à ce caractère*.

#### Condition `if`

On peut utiliser la condition `if` pour vérifier si un caractère est une lettre minuscule. Pour cela, on peut utiliser la table ASCII :

![Table ASCII](https://www.asciitable.com/asciifull.gif)

On voit que les lettres minuscules ont une valeur entre `97` et `122` (inclus), donc on peut vérifier si un caractère est une lettre minuscule avec la condition `if (s[i] >= 'a' && s[i] <= 'z')`.

Dans le tableau, cela reviendrait à chercher toutes les lettres dont l'index est entre `97` et `122` (inclus).

#### Conversion en majuscules

Pour convertir un caractère en majuscule, on peut encore utiliser la table ASCII :

- D'abord on récupère la lettre en minuscule avec `s[i]` (par exemple `d`)
- Ce qui nous intéresse, c'est l'index de cette lettre dans la table ASCII, on procède en deux étapes :
  - On obtient l'index dans l'alphabet de la lettre avec `s[i] - 'a'` car `'a'` a pour index `97` et `'d'` a pour index `100`, donc `s[i] - 'a'` vaut `3` (car `100 - 97 = 3`)
  - On obtient l'index dans la table ASCII avec `s[i] - 'a' + 'A'` car `'A'` a pour index `65` et `'d'` a pour index `100`, donc `s[i] - 'a' + 'A'` vaut `68` (car `100 - 97 + 65 = 68`)

On peut donc convertir un caractère en majuscule avec `s[i] = s[i] - 'a' + 'A'`.

*TIPS : On peut rajouter des parenthèses pour rendre le code plus lisible : `s[i] = (s[i] - 'a') + 'A'`.*

#### Retour de la fonction

On ne retourne rien car on modifie directement le tableau `s` dans la fonction. On pourrait aussi retourner le tableau `s` mais ce n'est pas nécessaire, le tableau `s` est modifié directement.
