# Presentation

On m'a dit que les TP de `C` du premier semestre se trouvaient [ici](https://perso.isima.fr/~loyon/unixc/index.php).

Pour de l'entraînement et de la révision, je vais essayer de les refaire ici, en les commentant au maximum.

# MacOS

Sur **MacOS**, utiliser `valgrind` ou d'autres outils de débogage peut être compliqué voire impossible nativement. Je conseille [lima](https://github.com/lima-vm/lima/), une VM bien intégrée à MacOS, et qui permet de faire tourner `valgrind` et `gdb` sans problème.

Elle permet de pouvoir avoir accès aux fichiers de la machine hôte, et de pouvoir les modifier directement depuis la VM.

## Configuration

J'ai galéré quelques minutes pour configurer la VM pour avoir accès à l'écriture, j'en suis arrivé à cette configuration :

```yaml
# Please edit the following configuration for Lima instance "ubuntu-lts"
# and an empty file will abort the edit.

# This template requires Lima v0.7.0 or later.
images:
# Try to use release-yyyyMMdd image if available. Note that release-yyyyMMdd will be removed after several months.
- location: "https://cloud-images.ubuntu.com/releases/22.04/release-20230729/ubuntu-22.04-server-cloudimg-amd64.img"
  arch: "x86_64"
  digest: "sha256:d5b419272e01cd69bfc15cbbbc5700d2196242478a54b9f19746da3a1269b7c8"
- location: "https://cloud-images.ubuntu.com/releases/22.04/release-20230729/ubuntu-22.04-server-cloudimg-arm64.img"
  arch: "aarch64"
  digest: "sha256:5ecab49ff44f8e44954752bc9ef4157584b7bdc9e24f06031e777f60860a9d17"
# Fallback to the latest release image.
# Hint: run `limactl prune` to invalidate the cache
- location: "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img"
  arch: "x86_64"
- location: "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-arm64.img"
  arch: "aarch64"

mounts:
- location: "~/LE_PATH_VERS_MON_DOSSIER_DE_COURS"
  writable: true
```