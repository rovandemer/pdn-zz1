---
title: TP1 - Sudoku
author: VAN DE MERGHEL Robin
date: 2023-2024
---

# Introduction

> On vous propose de réaliser le moteur de jeu texte du sudoku. Pour cela, vous devrez compiler et tester régulièrement. Sous gcc, vous devez utiliser les options : `-Wall -Wextra`.

# À vous de coder !

## Le squelette

> 1. Écrire le squelette du programme : la fonction `int main()` qui affiche `"SUDOKU"` sur la sortie standard.

On crée un fichier `main.c` avec le contenu suivant :

```c
#include <stdio.h> // Pour la sortie standard

int main() {
    printf("SUDOKU\n"); // On affiche "SUDOKU" sur la sortie standard
    return 0;
}
```

## La constante symbolique

> 2. Définir une constante symbolique `N` qui représente la taille de la grille carrée du jeu.

En `C`, on peut définir des constantes symboliques aussi appelées des macros ou variables préprocesseur. Elles sont définies par la directive `#define` et sont remplacées par leur valeur lors de la compilation.

Par exemple si on définit, si on a une macro `PI = 3.14`, à la compilation, toutes les occurences de `PI` seront remplacées par `3.14`.

On définit donc la constante `N` dans le fichier `main.c` :

```c
#define N 9 // On définit la constante N à 9

#include <stdio.h> // Pour la sortie standard
...
```

## La grille

> 3. Déclarer une matrice carrée de `N` cases de côté appelée grille dans la fonction `main()`. Cette grille contient des entiers et représente la grille de sudoku. La valeur 0 signifie que la case n'a pas encore eu de valeur.

On déclare donc une matrice carrée de `N` cases de côté, c'est-à-dire une matrice de `N` lignes et `N` colonnes. On la déclare dans la fonction `main()` :

```c
...

int main() {
    int grille[N][N] = {0}; // On déclare la grille de sudoku
    // Le {0} permet d'initialiser tous les éléments de la grille à 0,
    // Ce n'est pas obligatoire ici car on va les initialiser plus tard

    ...
}
```

*Note Importante : Si on initialise pas les éléments de la grille à 0, ils seront initialisés à des valeurs aléatoires, ce qui peut poser problème plus tard.*

## Remplissage

> 4. Déclarer et initialiser une variable entière `remplissage`, elle représente le nombre d'éléments non nuls dans la grille.

On déclare et initialise la variable `remplissage` à $5$ (au pif) :

```c
...

int main() {
    ...
    int remplissage = 5; // On déclare et initialise la variable remplissage
    ...
}
```

## Initialiser

> 5. Écrire une fonction `initialiser()` qui prend en paramètre `grille` et qui initialise tous les éléments de la grille à $0$ et qui renvoie un entier $0$

On écrit donc la fonction `initialiser` :

```c
int initialiser(int grille[N][N]) {
    for (int i = 0; i < N; ++i) { // Pour chaque ligne
        for (int j = 0; j < N; ++j) { // Pour chaque colonne
            grille[i][j] = 0; // On initialise la case à 0
        }
    }

    // Si le code ne renvoie pas d'erreur, on renvoie 0
    return 0; // On renvoie 0
}
```

## Afficher

> 6. Écrire une fonction `afficher()` qui prend en paramètre la grille et qui l’affiche en texte. Vérifier que tout marche jusque là

On va afficher la grille en texte, c'est-à-dire qu'on va afficher la grille comme ceci :

```
0|0|0|0|0|0|0|0|0
0|0|0|0|0|0|0|0|0
...
```

On écrit donc la fonction `afficher` :

```c
int afficher(int grille[N][N]) {
    
    
    // On affiche la grille
    for (int i = 0; i < N; i++) { // Pour chaque ligne
        printf("|");

        for (int j = 0; j < N; j++) { // Pour chaque colonne
            printf("%d|", grille[i][j]);
        }

        printf("\n");
    }

    // Si le code ne renvoie pas d'erreur, on renvoie 0
    return 0; // On renvoie 0
}
```

On peut maintenant appeler la fonction `initialiser` et `afficher` dans la fonction `main` :

```c
int main() {
    int grille[N][N] = {0}; // On déclare la grille de sudoku
    // Le {0} permet d'initialiser tous les éléments de la grille à 0,
    // Ce n'est pas obligatoire ici car on va les initialiser plus tard

    int remplissage = 5; // On déclare et initialise la variable remplissage

    initialiser(grille); // On initialise la grille
    afficher(grille); // On affiche la grille

    return 0;
}
```

On compile et on exécute :

```bash
$ gcc -Wall -Wextra main.c -o main
$ ./main
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
|0|0|0|0|0|0|0|0|0|
```

## Générer

> Le plus difficile à faire pour jouer au Sudoku est de trouver une bonne grille de départ. On peut imaginer différents algorithmes pour ce faire mais cela n'est pas le propos de ce TP. Je vous propose de créer une fonction `generer()` qui va permettre de disposer d'une grille presque complète pour que les tests fonctionnels soient plus rapides à écrire.

> 7. Ecrire la fonction `generer()`, elle prend en paramètre la grille et renvoie le nombre d'éléments non nuls.

Le nombre d'éléments non nuls est le nombre de cases remplies différentes de 0.

On écrit donc la fonction `generer` :

```c
int generer(int grille[N][N], int remplissage)
{

	// On admet que N = 9
	// On copie le code de l'énoncé
	int i, j;

	for (j = 0; j < 9; j++)
	{
		for (i = 0; i < 9;i++) {

			grille[i][j] = (i + j*3 +j /3) %9 +1 ;
		
		}
	}

	// On enlève donc quelques valeurs pour tester
	// On enlève N^2 - remplissage valeurs
	for (int k = 0; k < N * N - remplissage; k++)
	{
		grille[k / N][k % N] = 0; // On met la case à 0
	}


	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}
```

*Note : l'énoncé est vraiment pas clair, je ne sais pas si c'est ce qu'il faut faire*

## Saisie

> 8. Écrire une fonction `saisir()` qui demande à l’utilisateur de saisir au clavier les indices `i` et `j` et la valeur `v` à placer à l’emplacement `(i,j)`. La fonction doit vérifier la validité des indices et de la valeur. Si la case n’est pas occupée, la valeur doit être placée dans la grille. remplissage est alors incrémentée

On utilisera `scanf` pour la saisie.

On écrit donc la fonction `saisir` :

```c
int saisir(int grille[N][N], int remplissage)
{
    // On admet qu'on doit écrire sous la forme : i j v
    int i, j, v;

    // On demande à l'utilisateur de saisir les indices i et j et la valeur v
    printf("Saisir i j v : ");
    scanf("%d %d %d", &i, &j, &v);

    // On vérifie la validité des indices et de la valeur
    if (i < 0 || i >= N || j < 0 || j >= N || v < 1 || v > N) // On pourrait faire une fonction pré-processeur pour vérifier la validité
    {
        printf("Erreur : indices ou valeur invalide\n");
        return 1; // On renvoie 1 pour indiquer une erreur
    }
    
    // Si la case n'est pas occupée, on place la valeur dans la grille
    if (grille[i][j] == 0) {
        grille[i][j] = v; // On place la valeur dans la grille
        remplissage++; // On incrémente remplissage
    } else {
        printf("Erreur : la case est déjà occupée\n");
        return 1; // On renvoie 1 pour indiquer une erreur
    }

    // Si le code ne renvoie pas d'erreur, on renvoie 0
    return 0; // On renvoie 0

}
```

On peut essayer le code dans la fonction `main` :

```c
int main() {
    int grille[N][N] = {0}; // On déclare la grille de sudoku
    // Le {0} permet d'initialiser tous les éléments de la grille à 0,
    // Ce n'est pas obligatoire ici car on va les initialiser plus tard

    int remplissage = 5; // On déclare et initialise la variable remplissage

    initialiser(grille); // On initialise la grille
    afficher(grille); // On affiche la grille

    for (int i = 0; i < 5;i++) {
        saisir(grille, remplissage); // On saisit 5 valeurs
        afficher(grille); // On affiche la grille
    }

    return 0;
}
```

## Vérification de colonne

> 9. Écrire la fonction `verifierLigneColonne()` qui prend en paramètre un numéro et un sens (`HORIZ` ou `VERT`) qui vérifie que la ligne ou la colonne (suivant les cas) numéro est bien remplie. On pourra utiliser un tableau intermédiaire pour vérifier cela. La fonction retournera 1 si tout s’est bien passé, $0$ sinon. Les constantes `HORIZ` de valeur $0$ et `VERT` de valeur $1$ sont à définir.

Il faut donc définir les constantes `HORIZ` et `VERT` :

```c
#define HORIZ 0 // On définit la constante HORIZ à 0
#define VERT 1 // On définit la constante VERT à 1
```

On écrit donc la fonction `verifierLigneColonne` :

```c
int verifierLigneColonne(int grille[N][N], int numero, int sens)
{
    // On vérifie que la ligne ou la colonne (suivant les cas) numéro est bien remplie
    // On pourra utiliser un tableau intermédiaire pour vérifier cela
    // La fonction retournera 1 si tout s'est bien passé, 0 sinon

    // On vérifie que le sens est valide
    if (sens != HORIZ && sens != VERT) {
        printf("Erreur : sens invalide\n");
        return 0; // On renvoie 0 pour indiquer une erreur
    }

    // On vérifie que le numéro est valide
    if (numero < 0 || numero >= N) {
        printf("Erreur : numéro invalide\n");
        return 0; // On renvoie 0 pour indiquer une erreur
    }
    
    // On fait la somme des éléments de la ligne ou de la colonne
    int somme = 0;

    if (sens == HORIZ) { // Si on vérifie une ligne
        for (int j = 0; j < N; j++) { // Pour chaque colonne
            somme += grille[numero][j]; // On ajoute l'élément à la somme
        }
    } else { // Si on vérifie une colonne
        for (int i = 0; i < N; i++) { // Pour chaque ligne
            somme += grille[i][numero]; // On ajoute l'élément à la somme
        }
    }

    // On vérifie que la somme est égale à la somme des entiers de 1 à N
    if (somme != N * (N + 1) / 2) {
        printf("Erreur : la somme n'est pas égale à la somme des entiers de 1 à N\n");
        return 0; // On renvoie 0 pour indiquer une erreur
    }

    // Si le code ne renvoie pas d'erreur, on renvoie 1
    return 1; // On renvoie 1

}
```

*Rappel : la somme des entiers de 1 à N est égale à $\frac{N \times (N+1)}{2}$*

On peut essayer le code dans la fonction `main` :

```c
int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9*8; // On déclare et initialise la variable remplissage

	initialiser(grille); // On initialise la grille
	generer(grille, remplissage); // On génère la grille
	afficher(grille); // On affiche la grille

	// On vérifie que les lignes et les colonnes sont bien remplies
	printf("Ligne 0 : %d\n", verifierLigneColonne(grille, 0, HORIZ));
	printf("Ligne 1 : %d\n", verifierLigneColonne(grille, 1, HORIZ));

	printf("Colonne 0 : %d\n", verifierLigneColonne(grille, 0, VERT));
	printf("Colonne 1 : %d\n", verifierLigneColonne(grille, 1, VERT));
	

	return 0;
}
```

On doit obtenir :

```bash
$ gcc -Wall -Wextra main.c -o main
$ ./main
|0|0|0|0|0|0|0|0|0|
|2|5|8|3|6|9|4|7|1|
|3|6|9|4|7|1|5|8|2|
|4|7|1|5|8|2|6|9|3|
|5|8|2|6|9|3|7|1|4|
|6|9|3|7|1|4|8|2|5|
|7|1|4|8|2|5|9|3|6|
|8|2|5|9|3|6|1|4|7|
|9|3|6|1|4|7|2|5|8|
Ligne 0 : 0
Ligne 1 : 1
Colonne 0 : 0
Colonne 1 : 0
```

## Vérification de région

> 10. Écrire la fonction `verifierRegion()` qui prend en paramètre deux indices `k` et `l` qui correspondent à la région `(k,l)` et qui renvoie $1$ si la région est correctement remplie, $0$ sinon.

On écrit donc la fonction `verifierRegion` :

```c
int verifierRegion(int grille[N][N], int k, int l)
// Il n'y a que 3 régions si la grille fait 9*9
{
    // On vérifie que la région (k,l) est correctement remplie
    // La fonction retournera 1 si tout s'est bien passé, 0 sinon

    // On vérifie que les indices sont valides
    if (k < 0 || k >= N / 3 || l < 0 || l >= N / 3) {
        printf("Erreur : indices invalides\n");
        return 0; // On renvoie 0 pour indiquer une erreur
    }

    // On fait la somme des éléments de la région
    int somme = 0;

    for (int i = 0; i < N; i++) {
        // La coordonnée c'est (k*3 + i/3, l*3 + i%3)
        // Mais donc on inverse les indices car on a int[LIGNES][COLONNES]
        somme += grille[l * 3 + i / 3][k * 3 + i % 3]; // On ajoute l'élément à la somme
    }

    // On vérifie que la somme est égale à la somme des entiers de 1 à N
    if (somme != N * (N + 1) / 2) {
        printf("Erreur : la somme n'est pas égale à la somme des entiers de 1 à N\n");
        return 0; // On renvoie 0 pour indiquer une erreur
    }

    // Si le code ne renvoie pas d'erreur, on renvoie 1
    return 1; // On renvoie 1
}
```

On peut essayer le code dans la fonction `main` :

```c
int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9*8; // On déclare et initialise la variable remplissage

	initialiser(grille); // On initialise la grille
	generer(grille, remplissage); // On génère la grille
	afficher(grille); // On affiche la grille

	// On vérifie que les régions sont correctement remplies
	printf("Vérification des régions : %d\n", verifierRegion(grille, 0, 2));


	return 0;
}
```

## Vérification de la grille

> 11. Écrire la fonction `verifierGrille()` qui renvoie $1$ si la grille est correctement remplie et $0$ sinon

Pour vérifier la grille, on doit regarder toutes les zones.

On écrit donc la fonction `verifierGrille` :

```c
int verifierGrille(int grille[N][N])
{

	int nbZones = N / 3; // Il y a N/3 zones

	int valide = 1; // On suppose que la grille est valide

	// On vérifie que toutes les lignes et les colonnes sont correctement remplies
	for (int i = 0; i < N / 3; i++)
	{ 
		for (int j = 0; j < N / 3; j++)
		{ 
			valide = valide && verifierRegion(grille, i, j); // On vérifie la région (i,j)
		}
	}

	return valide; // On renvoie valide
}
```

*Note : l'opérateur `&&` est un opérateur logique qui renvoie 1 si les deux opérandes sont vraies, 0 sinon.*

On peut essayer le code dans la fonction `main` :

```c
int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9 * 8; // On déclare et initialise la variable remplissage

	initialiser(grille);					// On initialise la grille
	generer(grille, remplissage); // On génère la grille
	afficher(grille);							// On affiche la grille

	// On vérifie que les régions sont correctement remplies
	printf("Vérification de la grille : %d\n", verifierGrille(grille));

	return 0;
}
```

## Boucle principale

> 12. Écrire le programme principal, en supposant que la seule condition d’arrêt est la réussite du sudoku (ce test ne devra être fait que si nécessaire)

On écrit donc le programme principal :

```c
int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9 * 8; // On déclare et initialise la variable remplissage

	initialiser(grille);					// On initialise la grille
	generer(grille, remplissage); // On génère la grille
	
	while (!verifierGrille(grille))
	{ // Tant que la grille n'est pas valide
		// On affiche la grille
		afficher(grille);

		while (saisir(grille, remplissage))
		{ // Tant que la saisie n'est pas valide
			// On affiche la grille
			afficher(grille);
		}
	}

	// On affiche la grille
	afficher(grille);

	// On affiche un message de fin
	printf("Bravo !\n");

	return 0;
}
```

On peut tester le code en modifiant `generer` pour qu'il génère une grille presque valide :

```c
int generer(int grille[N][N], int remplissage)
{

	// On admet que N = 9
	// On copie le code de l'énoncé
	int i, j;

	for (j = 0; j < 9; j++)
	{
		for (i = 0; i < 9; i++)
		{

			grille[i][j] = (i + j * 3 + j / 3) % 9 + 1;
		}
	}

    // Avant :
	// // On enlève donc quelques valeurs pour tester
	// // On enlève N^2 - remplissage valeurs
	// for (int k = 0; k < N * N - remplissage; k++)
	// {
	// 	grille[k / N][k % N] = 0; // On met la case à 0
	// }
    // Après :
	grille[0][0] = 0;

    // On a juste à saisir (0,0) à 1

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}
```

## Écriture superposée

> 13. Un petit problème subsiste : on peut réécrire sur les nombres qui ont été donnés au départ. Est-ce que vous pourriez coder quelque chose qui permettrait de contourner ce problème ? Une idée toute simple est possible avec uniquement ce qui a été vu en semaines bloquées (pas de structures ou de choses du genre)

*Alors, j'adore car on l'a déjà fait dans la 8 comme demandé ??*

## Lecture de grille d'un fichier

> 14. Vous pouvez écrire une fonction pour lire une grille de sudoku à partir d’un fichier texte.

On écrit donc la fonction `lireGrille` :

```c
int lectureGrilleFichier(int grille[N][N], char fichier[]) {

	// On admet que tout va bien
	// On ouvre le fichier
	FILE *f = fopen(fichier, "r");

	// On vérifie que le fichier a bien été ouvert
	if (f == NULL) {
		printf("Erreur : le fichier n'a pas pu être ouvert\n");
		return 1; // On renvoie 1 pour indiquer une erreur
	}

	// Format :
	// 1 2 3 4 5 6 7 8 9
	// 4 5 6 7 8 9 1 2 3
	// ...
	// 7 8 9 1 2 3 4 5 6

	// On lit le fichier
	for (int i = 0; i < N; i++) { // Pour chaque ligne
		for (int j = 0; j < N; j++) { // Pour chaque colonne
			fscanf(f, "%d", &grille[i][j]); // On lit la valeur
		}
	}

	// On ferme le fichier
	fclose(f);

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0

}
```

On peut tester en remplaçant `generer` par `lectureGrilleFichier` :

```c
int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9 * 8; // On déclare et initialise la variable remplissage

	initialiser(grille);					// On initialise la grille

	// On lit la grille depuis un fichier
	lectureGrilleFichier(grille, "grille.txt");
	// generer(grille, remplissage); // On génère la grille

    ...

}
```

On teste avec le fichier `grille.txt` :

```
1 4 7 2 5 8 3 6 9 
2 5 8 3 6 9 4 7 1 
3 6 9 4 7 1 5 8 2 
4 7 1 5 8 2 6 9 3 
5 8 2 6 9 3 7 1 4 
6 9 3 7 1 4 8 2 5 
7 1 4 8 2 5 9 3 6 
8 2 5 9 3 6 1 4 7 
9 3 6 1 4 7 2 5 8 
```