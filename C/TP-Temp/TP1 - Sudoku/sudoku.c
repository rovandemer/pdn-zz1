#define N 9
#define HORIZ 0
#define VERT 1
#include <stdio.h>

int afficher(int grille[N][N])
{

	// On affiche la grille
	for (int i = 0; i < N; i++)
	{ // Pour chaque ligne
		printf("|");

		for (int j = 0; j < N; j++)
		{ // Pour chaque colonne
			printf("%d|", grille[i][j]);
		}

		printf("\n");
	}

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}

int initialiser(int grille[N][N])
{
	for (int i = 0; i < N; ++i)
	{ // Pour chaque ligne
		for (int j = 0; j < N; ++j)
		{										// Pour chaque colonne
			grille[i][j] = 0; // On initialise la case à 0
		}
	}

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}

int verifierLigneColonne(int grille[N][N], int numero, int sens)
{
	// On vérifie que la ligne ou la colonne (suivant les cas) numéro est bien remplie
	// On pourra utiliser un tableau intermédiaire pour vérifier cela
	// La fonction retournera 1 si tout s'est bien passé, 0 sinon

	// On vérifie que le sens est valide
	if (sens != HORIZ && sens != VERT)
	{
		printf("Erreur : sens invalide\n");
		return 0; // On renvoie 0 pour indiquer une erreur
	}

	// On vérifie que le numéro est valide
	if (numero < 0 || numero >= N)
	{
		printf("Erreur : numéro invalide\n");
		return 0; // On renvoie 0 pour indiquer une erreur
	}

	// On fait la somme des éléments de la ligne ou de la colonne
	int somme = 0;

	if (sens == HORIZ)
	{ // Si on vérifie une ligne
		for (int j = 0; j < N; j++)
		{															// Pour chaque colonne
			somme += grille[numero][j]; // On ajoute l'élément à la somme
		}
	}
	else
	{ // Si on vérifie une colonne
		for (int i = 0; i < N; i++)
		{															// Pour chaque ligne
			somme += grille[i][numero]; // On ajoute l'élément à la somme
		}
	}

	// On vérifie que la somme est égale à la somme des entiers de 1 à N
	if (somme != N * (N + 1) / 2)
	{
		printf("Erreur : la somme n'est pas égale à la somme des entiers de 1 à N\n");
		return 0; // On renvoie 0 pour indiquer une erreur
	}

	// Si le code ne renvoie pas d'erreur, on renvoie 1
	return 1; // On renvoie 1
}

int saisir(int grille[N][N], int remplissage)
{
	// On admet qu'on doit écrire sous la forme : i j v
	int i, j, v;

	// On demande à l'utilisateur de saisir les indices i et j et la valeur v
	printf("Saisir i j v : ");
	scanf("%d %d %d", &i, &j, &v);

	// On vérifie la validité des indices et de la valeur
	if (i < 0 || i >= N || j < 0 || j >= N || v < 1 || v > N) // On pourrait faire une fonction pré-processeur pour vérifier la validité
	{
		printf("Erreur : indices ou valeur invalide\n");
		return 1; // On renvoie 1 pour indiquer une erreur
	}

	// Si la case n'est pas occupée, on place la valeur dans la grille
	if (grille[i][j] == 0)
	{
		grille[i][j] = v; // On place la valeur dans la grille
		remplissage++;		// On incrémente remplissage
	}
	else
	{
		printf("Erreur : la case est déjà occupée\n");
		return 1; // On renvoie 1 pour indiquer une erreur
	}

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}

int generer(int grille[N][N], int remplissage)
{

	// On admet que N = 9
	// On copie le code de l'énoncé
	int i, j;

	for (j = 0; j < 9; j++)
	{
		for (i = 0; i < 9; i++)
		{

			grille[i][j] = (i + j * 3 + j / 3) % 9 + 1;
		}
	}

	// // On enlève donc quelques valeurs pour tester
	// // On enlève N^2 - remplissage valeurs
	// for (int k = 0; k < N * N - remplissage; k++)
	// {
	// 	grille[k / N][k % N] = 0; // On met la case à 0
	// }
	grille[0][0] = 0;

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0
}

int verifierRegion(int grille[N][N], int k, int l)
// Il n'y a que 3 régions si la grille fait 9*9
{
	// On vérifie que la région (k,l) est correctement remplie
	// La fonction retournera 1 si tout s'est bien passé, 0 sinon

	// On vérifie que les indices sont valides
	if (k < 0 || k >= N / 3 || l < 0 || l >= N / 3)
	{
		printf("Erreur : indices invalides\n");
		return 0; // On renvoie 0 pour indiquer une erreur
	}

	// On fait la somme des éléments de la région
	int somme = 0;

	for (int i = 0; i < N; i++)
	{
		// La coordonnée c'est (k*3 + i/3, l*3 + i%3)
		// Mais donc on inverse les indices car on a int[LIGNES][COLONNES]
		somme += grille[l * 3 + i / 3][k * 3 + i % 3]; // On ajoute l'élément à la somme
	}

	// On vérifie que la somme est égale à la somme des entiers de 1 à N
	if (somme != N * (N + 1) / 2)
	{
		printf("Erreur : la somme n'est pas égale à la somme des entiers de 1 à N\n");
		return 0; // On renvoie 0 pour indiquer une erreur
	}

	// Si le code ne renvoie pas d'erreur, on renvoie 1
	return 1; // On renvoie 1
}

int lectureGrilleFichier(int grille[N][N], char fichier[]) {

	// On admet que tout va bien
	// On ouvre le fichier
	FILE *f = fopen(fichier, "r");

	// On vérifie que le fichier a bien été ouvert
	if (f == NULL) {
		printf("Erreur : le fichier n'a pas pu être ouvert\n");
		return 1; // On renvoie 1 pour indiquer une erreur
	}

	// Format :
	// 1 2 3 4 5 6 7 8 9
	// 4 5 6 7 8 9 1 2 3
	// ...
	// 7 8 9 1 2 3 4 5 6

	// On lit le fichier
	for (int i = 0; i < N; i++) { // Pour chaque ligne
		for (int j = 0; j < N; j++) { // Pour chaque colonne
			fscanf(f, "%d", &grille[i][j]); // On lit la valeur
		}
	}

	// On ferme le fichier
	fclose(f);

	// Si le code ne renvoie pas d'erreur, on renvoie 0
	return 0; // On renvoie 0

}

int verifierGrille(int grille[N][N])
{

	int nbZones = N / 3; // Il y a N/3 zones

	int valide = 1; // On suppose que la grille est valide

	// On vérifie que toutes les lignes et les colonnes sont correctement remplies
	for (int i = 0; i < N / 3; i++)
	{ 
		for (int j = 0; j < N / 3; j++)
		{ 
			valide = valide && verifierRegion(grille, i, j); // On vérifie la région (i,j)
		}
	}

	return valide; // On renvoie valide
}

int main()
{
	int grille[N][N] = {0}; // On déclare la grille de sudoku
	// Le {0} permet d'initialiser tous les éléments de la grille à 0,
	// Ce n'est pas obligatoire ici car on va les initialiser plus tard

	int remplissage = 9 * 8; // On déclare et initialise la variable remplissage

	initialiser(grille);					// On initialise la grille

	// On lit la grille depuis un fichier
	lectureGrilleFichier(grille, "grille.txt");
	// generer(grille, remplissage); // On génère la grille
	
	while (!verifierGrille(grille))
	{ // Tant que la grille n'est pas valide
		// On affiche la grille
		afficher(grille);

		while (saisir(grille, remplissage))
		{ // Tant que la saisie n'est pas valide
			// On affiche la grille
			afficher(grille);
		}
	}

	// On affiche la grille
	afficher(grille);

	// On affiche un message de fin
	printf("Bravo !\n");

	return 0;
}
