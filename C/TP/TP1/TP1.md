---
title: TP1 - C
author: VAN DE MERGHEL Robin
date: 15 septembre 2023
---

*Source : [https://perso.isima.fr/~loyon/unixc/tpcinit-01.php](https://perso.isima.fr/~loyon/unixc/tpcinit-01.php)*

# Hello World

> Taper ou copier-coller le code suivant dans un éditeur de texte (code, geany, gedit, nano, vi par exemple sous nightmare) :

```C
/*
     Voici un premier programme en C qui correspond au "Hello World" classique
*/

#include <stdio.h>

int main()
{
   printf("Bonjour les ZZ1!\n");
   printf("Comment allez-vous ?\n");

   return 0;
}
```

> Compiler et exécuter

```bash
$ gcc bonjour.c -o bonjour
$ ./bonjour
```

Le programme affiche :

```bash
Bonjour les ZZ1!
Comment allez-vous ?
```

> Modifier le programme à partir des indications suivantes :

- Déclarer une variable entière et l'initialiser avec un nombre positif
- Afficher cette variable comme un entier
- Afficher cette variable comme un réel
- Bien noter ce que vous dit le compilateur quand vous vous trompez de format au `printf()`
- Faire de même avec une variable de type réel : déclarer, l'afficher comme un nombre réel puis comme un entier

# Variables

> On fait la variable entière `a` et on l'initialise à 5

```C
#include <stdio.h>

int main()
{
   int a = 5;

   printf("a = %d\n", a); // %d pour afficher un entier, \n pour un retour à la ligne

   return 0;
}
```

> On fait la variable réelle `b` et on l'initialise à 5.5

```C
#include <stdio.h>

int main()
{
   int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}
```

> Que se passe-t-il si on se trompe de format dans le `printf()` ?

```C
#include <stdio.h>

int main()
{
   int a = 5;
   float b = 5.5;

   printf("a = %f\n", a); // %f pour afficher un réel

   return 0;
}
```

Le compilateur nous dit :

```bash
main.c:8:23: warning: format specifies type 'double' but the argument has type 'int' [-Wformat]
   printf("a = %f\n", a); // %f pour afficher un réel
               ~~     ^
               %d
1 warning generated.
a = 0.000000
```

L'erreur est explicite, on s'est trompé de format dans le `printf()`.

*Voici un tableau récapitulatif des formats :*

| Format | Type de variable     |
|--------|----------------------|
| `%d`   | Entier               |
| `%f`   | Réel                 |
| `%c`   | Caractère            |
| `%s`   | Chaîne de caractères |

## Note

> Pour la compilation, on vous demande de prendre l'habitude de compiler avec les options suivantes à minima :

```bash
-Wall -Wextra -g
```

- `-Wall` : affiche tous les warnings
- `-Wextra` : affiche des warnings supplémentaires
- `-g` : permet de débugger avec gdb

Cela permet de pouvoir corriger les erreurs plus facilement.

## Messages d'erreurs

> Vous allez changer successivement certaines lignes pour voir les messages d'erreurs ou d'avertissement. Il est fondamental de connaître les principaux messages pour savoir comment remédier aux bêtises faites.

- Renommer la fonction `main()` en `main2()`
- Dupliquer la fonction `main()`
- Commenter la déclaration de variable
- Commenter la ligne avec `#include`

### Renommer la fonction `main()` en `main2()`

```C
#include <stdio.h>

int main2()
{
   int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}
```

Le compilateur nous dit :

```bash
Undefined symbols for architecture arm64:
  "_main", referenced from:
     implicit entry/start for main executable
     (maybe you meant: _main2)
ld: symbol(s) not found for architecture arm64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
```

Le compilateur nous dit que la fonction `main()` n'existe pas. *Tout programme doit avoir une fonction `main()`*.

### Dupliquer la fonction `main()`

```C
#include <stdio.h>

int main()
{
   int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}

int main()
{
   int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}
```

Le compilateur nous dit :

```bash
main.c:14:5: error: redefinition of 'main'
int main()
    ^
main.c:3:5: note: previous definition is here
int main()
    ^
1 error generated.
```

Le compilateur nous dit que la fonction `main()` est définie deux fois. *Il ne peut y avoir qu'une seule fonction `main()`*. Plus généralement, *il ne peut y avoir qu'une seule fonction avec le même nom*.

### Commenter la déclaration de variable

```C
#include <stdio.h>

int main()
{
   // int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}
```

Le compilateur nous dit :

```bash
main.c:8:23: error: use of undeclared identifier 'a'
   printf("a = %d\n", a);
                      ^
1 error generated.
```

Le compilateur nous dit que la variable `a` n'est pas déclarée. *Il faut déclarer une variable avant de l'utiliser*.

### Commenter la ligne avec `#include`

```C
// #include <stdio.h>

int main()
{
   int a = 5;
   float b = 5.5;

   printf("a = %d\n", a);
   printf("b = %f\n", b); // %f pour afficher un réel

   return 0;
}
```

Le compilateur nous dit :

```bash
main.c:6:4: error: call to undeclared library function 'printf' with type 'int (const char *, ...)'; ISO C99 and later do not support implicit function declarations [-Wimplicit-function-declaration]
   printf("a = %d\n", a);
   ^
main.c:6:4: note: include the header <stdio.h> or explicitly provide a declaration for 'printf'
1 error generated.
```

Le compilateur nous dit que la fonction `printf()` n'est pas déclarée. *Il faut inclure la bibliothèque qui contient la fonction `printf()`*.

# Tests

> Tester le programme du cours en modifiant `i` :

```C
int main()
{
    int i = 17304;

    if (i == -1)  printf("i vaut -1\n");
    if (i != -1)  printf("i est différent de -1\n");
    if (i != 0)   printf("i est non nul\n");
    if (i)        printf("i est non nul également\n");
    if (i == 0)   printf("i est nul\n");
    if (!i)       printf("i est nul encore !\n");
    if (i>=0) && (i<=10) printf("i est petit\n");
    if (i>3) || (i<-3) printf("i est hors intervalle\n");     
    return 0;
}
```

Il y a une erreur de compilation :

```bash
main.c:13:18: error: expected identifier
    if (i>=0) && (i<=10) printf("i est petit\n");
                 ^
main.c:14:14: error: expected expression
    if (i>3) || (i<-3) printf("i est hors intervalle\n");     
             ^
2 errors generated.
```

Il faut mettre des parenthèses autour des conditions :

```C
if (condition1 && condition2)
ou 
if ((condition1) && (condition2))
```

Nouvelle version corigée :

```C
if (i>=0 && i<=10) printf("i est petit\n");
if (i>3 || i<-3) printf("i est hors intervalle\n");     
```

## Équation second degré

> Une équation de degré 2 est caractérisée par les coefficient $a$, $b$ et $c$ dans $ax^2+bx+c = 0$. On vous demande d'afficher l'équation à résoudre, le nombre de solutions et les solutions de l'équation.

> Dans un premier temps, $a$, $b$ et $c$ seront des variables. Le programme sera recompilé à chaque fois que les valeurs de $a$, $b$ ou $c$ changeront. La fonction racine carrée est `sqrt()`, elle est dans la bibliothèque `math.h` et nécessite d'ajouter l'option `-lm` à la compilation.

```C
#include <stdio.h>
#include <math.h> // pour la fonction sqrt()

int main()
{
    float a = 1;
    float b = 2;
    float c = 1;

    float delta = b*b - 4*a*c; // calcul du discriminant

    printf("Equation %gx^2 + %gx + %g = 0\n", a, b, c);
    printf("Delta = %g\n", delta);

    if (delta < 0)
    {
        printf("Pas de solution\n");
    }
    else if (delta == 0)
    {
        float x = -b / (2*a); // calcul d'une solution
        printf("Une solution : %g\n", x);
    }
    else
    {
        float x1 = (-b - sqrt(delta)) / (2*a); // calcul des deux solutions
        float x2 = (-b + sqrt(delta)) / (2*a); // calcul des deux solutions
        printf("Deux solutions : %g et %g\n", x1, x2);
    }

    return 0;
}
```

Commande de compilation :

```bash
$ gcc equation.c -o equation -lm -Wall -Wextra -g
```

On a bien la bonne solution :

```bash
Equation 1x^2 + 2x + 1 = 0
Delta = 0
Une solution : -1
```

### Plusieurs commentaires

- En `C`, il n'y a pas comme en python des puissances avec `**`. Il faut utiliser la fonction `pow()` de la bibliothèque `math.h` :

```C
pow(x, y) // x^y
```

- Pour afficher un nombre réel avec un nombre de chiffres après la virgule, on utilise le format `%0.2f` :

```C
printf("%0.2f\n", 1.2345); // affiche 1.23
```

# Boucles

> On vous demande de réaliser les petites choses suivantes en utilisant les différents types de boucle : `while`, `for` et `do while`.

- Afficher les entiers de 0 à un nombre stocké dans une variable.
- Afficher les carrés de 0 à un nombre stocké dans une variable.
- Calculer la somme des $n^2$ premiers nombres pour un n donné dans une variable.
- Calculer la somme des $\frac{1}{n^2}$ pour un n donné dans une variable
- Afficher les tables de multiplication de 1 à 12

## Afficher les entiers de 0 à un nombre stocké dans une variable

```C
#include <stdio.h>

int main()
{
    int n = 10;
    int i;

    for (i = 0; i <= n; i++)
    {
        printf("%d\n", i);
    }

    return 0;
}
```

## Afficher les carrés de 0 à un nombre stocké dans une variable

```C
#include <stdio.h>

int main()
{
    int n = 10;
    int i = 0;

    while (i <= n)
    {
        printf("%d\n", i*i);
        i++;
    }

    return 0;
}
```

## Calculer la somme des $n^2$ premiers nombres pour un n donné dans une variable

```C
#include <stdio.h>

int main()
{
    int n = 10;
    int i = 0;
    int somme = 0;

    while (i <= n)
    {
        somme += i*i;
        i++;
    }

    printf("Somme = %d\n", somme);

    return 0;
}
```

## Calculer la somme des $\frac{1}{n^2}$ pour un n donné dans une variable

```C
#include <stdio.h>

// On va faire un do while pour faire plaisir à l'énoncé
int main()
{
    int n = 10;
    int i = 0;
    float somme = 0;

    do
    {
        somme += 1.0 / (i*i);
        i++;
    } while (i <= n);

    printf("Somme = %0.2f\n", somme);

    return 0;
}
```

## Afficher les tables de multiplication de 1 à 12

```C
#include <stdio.h>

int main()
{
    int i, j;

    for (i = 1; i <= 12; i++)
    {
        for (j = 1; j <= 12; j++)
        {
            printf("%d x %d = %d\n", i, j, i*j);
        }
        printf("\n");
    }

    return 0;
}
```

# Affichages en tout genre

Exécuter le programme suivant :

```C
int main() 
{
    float x = 1.0, y = .0;
    int i = 7;
    /* Quelques exemples */
    printf("Un mot\n");
    printf("un nombre : %d\n", 3);
    printf("Solution x:%f, y:%f\n", x, y);

    printf("%d\n", i);
    printf("%d\n", ++i);
    printf("%d\n", i);
    printf("%d\n", i++);
    printf("%d\n", i);

    printf("%d\n", 3.5);
    printf("\n");
    printf("\t\t %d\n", i);

    return 0;
}
```

> Que fait le programme ?

```C
main.c:17:18: warning: format specifies type 'int' but the argument has type 'double' [-Wformat]
  printf("%d\n", 3.5);
          ~~     ^~~
          %f
1 warning generated.
Un mot
un nombre : 3
Solution x:1.000000, y:0.000000
7
8
8
8
9
0
```

L'erreur est qu'à la ligne `printf("%d\n", 3.5);` on veut afficher un entier mais on donne un réel (Cf [Tableau récapitulatif des formats](#tableau-récapitulatif-des-formats)). Le compilateur nous dit que le format `%d` attend un entier mais on lui donne un réel.

On modifie la ligne `printf("%d\n", 3.5);` en `printf("%f\n", 3.5);`. On a bien le bon résultat :

```bash
Un mot
un nombre : 3
Solution x:1.000000, y:0.000000
7
8
8
8
9
3.500000



```
