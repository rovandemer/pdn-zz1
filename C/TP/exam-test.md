> Écrire une fonction `calcul()` qui retourne la valeur entière décimale d'un nombre au format hexadécimal représenté par une chaine de caractères. Ainsi calcul de `"0"` renvoie 0 et `"FF"` renvoie 255 (changement de base)

On utilise `snprintf` :

```C
#include <stdio.h>
#include <stdlib.h>

int calcul(char *hexa) {
    char *endptr;
    return strtol(hexa, &endptr, 16);
}

int main(void) {
    char hexa[10];
    printf("Entrez un nombre hexadécimal : ");
    scanf("%s", hexa);
    printf("%s = %d\n", hexa, calcul(hexa));
    return 0;
}
```