#include <stdio.h>
#include <stdlib.h>

char *calcul(char *hexa, char *buffer) {
    // On utilise snprintf pour écrire dans le buffer
    snprintf(buffer, 10, "%x", atoi(hexa));

    return buffer;
}

int main(void) {
  char hexa[10];
  char buffer[10];
  printf("Entrez un nombre à convertir : ");
  scanf("%s", hexa);
  calcul(hexa, buffer);
    printf("Le nombre %s en hexadécimal est %s\n", hexa, buffer);
  return 0;
}