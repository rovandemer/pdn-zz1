---
title: TP4 - C
author: "VAN DE MERGHEL Robin"
date: "19 septembre 2023"
---

> Ceci est le premier jeu à coder avec des chaînes de caractères. Vous pourrez ensuite faire le MOTUS <br>
> 
> Voici le jeu que vous allez coder, il s'agit du ...

```C
/*
       /-----
       |    |
       |    O
       |   /|\
       |   /|
    ___|___
*/
```

# Moteur de jeu

Le principe du jeu est très simple. Il faut deviner un mot lettre par lettre. À chaque fois qu'une proposition de lettre est fausse, un morceau de gibet apparait. Il faut deviner le mot avant l'apparition complète de la potence.

Je vous propose d'utiliser deux variables de type chaine de caractères : une pour stocker le mot à trouver (vous le fixerez à la compilation dans un premier temps), l'autre, de même taille pour mémoriser les lettres déjà découvertes (que des étoiles au départ)

> Dans un premier temps, vous pouvez initialiser mot_decouvert, mais le mieux est quand même de faire une fonction qui le crée en fonction du mot de départ :

```C
void cacher(char mot_a_trouver[], char mot_avec_etoiles[]);
// Note : on va peut-être renommer les paramètres car ils sont moches mdr
```

> Voici un tableau de caractères pour les différents états du jeu (10) :

```C
char cartoon [10][70] = {
  "\n\n\n\n\n\n_______\n",
  "\n   |\n   |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |    |\n   |   \n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /\n___|___\n",
  "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /|\n___|___\n"
};
```

> `cartoon[0]` est le pied de la potence. `cartoon[9]` est la potence complète, pendu inclus.

# Utiliser des fonctions qui existent déjà

> La fonction `isalpha()` permet de savoir si le caractère fourni en paramètre est une lettre.

```C
char c;
c ='1';
printf("%d", isalpha(c));
c ='a';
printf("%d", isalpha(c));
c ='Z';
printf("%d", isalpha(c));
```

> Vous pourrez par exemple arrêter le jeu si l'utilisateur ne saisit pas une lettre.

> Pour ne pas avoir une erreur d'utilisation de fonction implicite, il vous faudra trouver l'entête correct pour la fonction `isalpha()`. Vous pouvez également essayer tous les entêtes que vous connaissez `:-)`, sinon tapez.

> La fonction toupper() permet de transformer une lettre minuscule en majuscule (cele ne pose pas de problème si la lettre est déjà une majuscule).

```C
printf("%c", toupper('c'));
printf("%c", toupper('A'));
printf("%c", toupper('3'));
```

# Élargir la base de jeu

> Faire une fonction qui permet de choisir un mot au hasard dans un dictionnaire (voir le TP suivant)

# Coder

On va coder le jeu en plusieurs étapes :

- Afficher le dessin de la potence
- Chercher le mot à trouver dans le dictionnaire
- Afficher le mot à trouver
- Demander une lettre à l'utilisateur
- Vérifier si la lettre est dans le mot
- Afficher le mot avec les lettres trouvées
- Afficher le dessin de la potence en fonction du nombre de lettres fausses
- Recommencer jusqu'à ce que le mot soit trouvé ou que le pendu soit complet
- Afficher un message de victoire ou de défaite

```C
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TAILLE_MAX 100

char cartoon[10][70] = {
    "\n\n\n\n\n\n_______\n",
    "\n   |\n   |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |    |\n   |   \n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /\n___|___\n",
    "\n   /-----\n   |    |\n   |    O\n   |   /|\\\n   |   /|\n___|___\n"};

// Pour cacher
void cacher(char initial[], char hidden[]) {
  int i;
  for (i = 0; initial[i] != '\0'; i++) {
    hidden[i] = '*';
  }
  hidden[i] = '\0';
}

// Pour choisir un mot au hasard
// (placeholder, on fera le vrai code dans le tp suivant)
char *choisir_mot() { return "coucou"; }

// Pour afficher le mot caché
void afficher(char *word) { printf("%s\n", word); }

// Pour demander une lettre à l'utilisateur
char demander_lettre() {
  char c;
  printf("Entrez une lettre : ");
  scanf("%c", &c);
  while (getchar() != '\n')
    ;
  return c;
}

// Pour vérifier si la lettre est dans le mot
int verifier(char *word, char letter) {

  return strchr(word, letter) != NULL;
}

// Pour afficher le dessin de la potence
void afficher_dessin(int nb_erreurs) { printf("%s\n", cartoon[nb_erreurs]); }

// Pour afficher un message de victoire ou de défaite
void afficher_message(int nb_erreurs) {
  if (nb_erreurs == 9) {
    printf("Vous avez perdu !\n");
  } else {
    printf("Vous avez gagné !\n");
  }
}

int main(void) {
  char mot_a_trouver[TAILLE_MAX];
  char mot_decouvert[TAILLE_MAX];
  char lettre;
  int nb_erreurs = 0;
  int indexeLettre = 0;


  // Initialisation
  srand(time(NULL));
  strcpy(mot_a_trouver, choisir_mot());
  cacher(mot_a_trouver, mot_decouvert);

  // Boucle de jeu
  while (nb_erreurs < 9 && strcmp(mot_a_trouver, mot_decouvert) != 0) {
    
    afficher(mot_decouvert);
    
    lettre = demander_lettre();
    if ((indexeLettre = verifier(mot_a_trouver, lettre))) {
      printf("La lettre %c est dans le mot\n", lettre);

      for (int i = 0; mot_a_trouver[i] != '\0'; i++) {
        if (mot_a_trouver[i] == lettre) {
          mot_decouvert[i] = lettre;
        }
      }

    } else {
      printf("La lettre %c n'est pas dans le mot\n", lettre);
      nb_erreurs++;
    }

    afficher_dessin(nb_erreurs);
  
    puts("-------------------- \n");
  }

  // Fin de jeu
  afficher_message(nb_erreurs);

  return 0;
}
```