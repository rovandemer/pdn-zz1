---
title: TP3 - C
author: "VAN DE MERGHEL Robin"
date: "19 septembre 2023"
---

# Préprocesseur

- Écrire un programme qui affiche un texte $N$ fois où $N$ est une constante symbolique
- Modifier $N$ et voir l'intérêt de la constante symbolique
- Déclarer dans la fonction `main()` Une variable $N$ entière et voir la réponse du compilateur.

```c
#include <stdio.h>

#define N 10

int main(void)
{
    int i = 0;
    for (i = 0; i < N; i++)
    {
        printf("Bonjour\n");
    }
    return 0;
}
```

# Tableaux

## Premières manipulations de tableaux

- Déclarer un tableau de 10 éléments de type entier (int)
- Saisir les valeurs au clavier
- Afficher le tableau ainsi que la somme des éléments
- Tester la redirection de l’entrée standard pour éviter la saisie

```c
#include <stdio.h>

#define N 10

int main(void)
{
    int i = 0, somme = 0;
    int tableau[N] = {0};

    for (i = 0; i < N; i++)
    {
        printf("Entrez un nombre : ");
        scanf("%d", &tableau[i]);
        somme += tableau[i];
    }

    for (i = 0; i < N; i++)
    {
        printf("%d ", tableau[i]);
    }

    printf("\nSomme : %d\n", somme);
    return 0;
}
```

Pour tester avec la redirection de l'entrée standard, il faut créer un fichier `input.txt` avec les valeurs à saisir et lancer la commande suivante :

```bash
./a.out < input.txt
```

## Tableaux et fonctions (1)

- Déclarer un tableau de 50 éléments comme une variable globale.
- Initialiser tous les éléments du tableau à 100
- Ajouter l’indice de tableau à l’élément pour tout indice pair
- Enlever l’indice du tableau à l’élément pour tout indice impair.
- Écrire une fonction qui affiche le tableau
- Écrire une fonction qui renvoie le minimum.
- Écrire une fonction qui renvoie le maximum.
- Écrire une fonction qui renvoie la moyenne.
- Afficher ces valeurs dans le programme principal
- Utiliser une constante symbolique pour la taille maximale du tableau et aux endroits stratégiques
- Valider le fait qu'un changement de taille du tableau avec une constante n'a que peu d'impact.

```c
#include <stdio.h>

#define N 50

int tableau[N] = {0};

void afficherTableau(void)
{
    for (int i = 0; i < N; i++)
    {
        printf("%d ", tableau[i]);
    }
    printf("\n");
}

int minimum(void)
{
    int min = tableau[0];
    for (int i = 1; i < N; i++)
    {
        if (tableau[i] < min)
        {
            min = tableau[i];
        }
    }
    return min;
}

int maximum(void)
{
    int max = tableau[0];
    for (int i = 1; i < N; i++)
    {
        if (tableau[i] > max)
        {
            max = tableau[i];
        }
    }
    return max;
}

float moyenne(void)
{
    float moy = 0;
    for (int i = 0; i < N; i++)
    {
        moy += tableau[i];
    }
    moy /= N;
    return moy;
}

int main(void)
{
    for (int i = 0; i < N; i++)
    {
        tableau[i] = 100;
    }

    for (int i = 0; i < N; i++)
    {
        if (i % 2 == 0)
        {
            tableau[i] += i;
        }
        else
        {
            tableau[i] -= i;
        }
    }

    afficherTableau();
    printf("Minimum : %d\n", minimum());
    printf("Maximum : %d\n", maximum());
    printf("Moyenne : %f\n", moyenne());
    return 0;
}
```

## Tableaux et fonctions (2)

- Écrire un autre programme avec les mêmes fonctionnalités mais en considérant que le tableau est maintenant une variable locale à la fonction `main()`.
- Le tableau, ainsi que la taille du tableau, sera passé en paramètre aux différentes fonctions.

```c
#include <stdio.h>

#define N 50

void afficherTableau(int tableau[], int taille)
{
    for (int i = 0; i < taille; i++)
    {
        printf("%d ", tableau[i]);
    }
    printf("\n");
}

int minimum(int tableau[], int taille)
{
    int min = tableau[0];
    for (int i = 1; i < taille; i++)
    {
        if (tableau[i] < min)
        {
            min = tableau[i];
        }
    }
    return min;
}

int maximum(int tableau[], int taille)
{
    int max = tableau[0];
    for (int i = 1; i < taille; i++)
    {
        if (tableau[i] > max)
        {
            max = tableau[i];
        }
    }
    return max;
}

float moyenne(int tableau[], int taille)
{
    float moy = 0;
    for (int i = 0; i < taille; i++)
    {
        moy += tableau[i];
    }
    moy /= taille;
    return moy;
}

int main(void)
{
    int tableau[N] = {0};

    for (int i = 0; i < N; i++)
    {
        tableau[i] = 100;
    }

    for (int i = 0; i < N; i++)
    {
        if (i % 2 == 0)
        {
            tableau[i] += i;
        }
        else
        {
            tableau[i] -= i;
        }
    }

    afficherTableau(tableau, N);
    printf("Minimum : %d\n", minimum(tableau, N));
    printf("Maximum : %d\n", maximum(tableau, N));
    printf("Moyenne : %f\n", moyenne(tableau, N));
    return 0;
}
```

# Devinette

- Faire tirer à l’ordinateur un nombre aléatoire entre 0 et 100 sans l’afficher.
- Écrire un programme qui demande à l’utilisateur un nombre tant que celui-ci est différent du nombre de départ.On aidera l’utilisateur en lui indiquant si le nombre est trop petit ou trop grand.
- On affichera ensuite le nombre de tentatives effectuées.
- Modifier le jeu pour le pimenter : Demander au joueur le nombre de tentatives qu’il pense faire, avant que le jeu commence. Si le joueur ne trouve pas le nombre ou s’il se trompe sur l’estimation, c’est perdu !

```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    int nombre = 0, 
    nombreAleatoire = 0, 
    nombreTentatives = 0, 
    nombreMaxTentatives = 0;

    // Initialisation de la fonction rand()
    srand(time(NULL));

    // Tirage du nombre aléatoire
    nombreAleatoire = rand() % 101;
    printf("Entrez le nombre de tentatives : ");
    scanf("%d", &nombreMaxTentatives);

    // Boucle de jeu
    do
    {
        printf("Entrez un nombre : ");
        scanf("%d", &nombre);
        nombreTentatives++;
        if (nombre < nombreAleatoire)
        {
            printf("Trop petit\n");
        }
        else if (nombre > nombreAleatoire)
        {
            printf("Trop grand\n");
        }
    } while (nombre != nombreAleatoire && nombreTentatives < nombreMaxTentatives);

    // Affichage du résultat
    if (nombre == nombreAleatoire)
    {
        printf("Bravo ! Vous avez trouvé en %d tentatives\n", nombreTentatives);
    }
    else
    {
        printf("Perdu ! Le nombre était %d\n", nombreAleatoire);
    }

    return 0;
}
```

*Notes sur `rand()` et `srand()` :*

- `rand()` est une fonction qui renvoie un nombre aléatoire entre 0 et `RAND_MAX` (une constante symbolique définie dans `stdlib.h`).
- `srand()` est une fonction qui initialise la fonction `rand()` avec une graine (seed) qui permet de générer des nombres aléatoires différents à chaque exécution du programme. Si on ne l'appelle pas, la graine est initialisée à 1 par défaut.
- Pour initialiser la graine avec une valeur différente à chaque exécution, on utilise la fonction `time()` qui renvoie le nombre de secondes écoulées depuis le 1er janvier 1970. On utilise donc `srand(time(NULL))` pour initialiser la graine avec une valeur différente à chaque exécution.
- Pour générer un nombre aléatoire entre 0 et 100, on utilise `rand() % 101` qui renvoie un nombre entre 0 et 100.


# Petits algorithmes

## Suite de fibonnacci

*Rappel* : 

<!-- Dans des brackets -->
$$\left\{\begin{array}{l}
u_0 = 0 \\
u_1 = 1 \\
u_n = u_{n-1} + u_{n-2}
\end{array}\right.$$

Nous vous proposons de calculer la suite de Fibonacci de deux manières :

- avec une fonction récursive
- avec stockage des valeurs dans un tableau de taille donnée

Avec une fonction récursive, on peut écrire :

```c
int fibonacci(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n == 1)
    {
        return 1;
    }
    else
    {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
```

Avec un tableau, on peut écrire :

```c
int fibonacci(int n)
{
    int tableau[n + 1];
    tableau[0] = 0;
    tableau[1] = 1;
    for (int i = 2; i <= n; i++)
    {
        tableau[i] = tableau[i - 1] + tableau[i - 2];
    }
    return tableau[n];
}
```

## Triangle de pascal

> Afficher sur la sortie standard le triangle de pascal d'ordre $n$.


> Vous pouvez utiliser deux tableaux de tailles fixes ou bien utiliser une matrice de dimension 2, il n'est pas nécessaire de copier les lignes si vous travaillez modulo 2

```c
#include <stdio.h>

#define N 10

int main(void)
{
    int triangle[N][N] = {0};

    for (int i = 0; i < N; i++)
    {
        triangle[i][0] = 1;
        triangle[i][i] = 1;
    }

    for (int i = 2; i < N; i++)
    {
        for (int j = 1; j < i; j++)
        {
            triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j <= i; j++)
        {
            printf("%d ", triangle[i][j]);
        }
        printf("\n");
    }

    return 0;
}
```

## Chaînes de caractères et texte

- Demander sur l'entrée standard un prénom `p`
- Demander sur l'entrée standard un nombre `n`
- Afficher $n$ fois `"Merci p"`
- Compter le nombre de lettres de `p` sans utiliser `strlen()`
- Comparer le résultat avec `strlen()`
- Demander le nom de la personne.
- Placer dans des chaînes de caractères plus grandes (copie et - concaténation) les formules (nom et prénom) et (prénom et nom) avec les fonctions standards `strcpy()` et `strcat()`.
- Pouvez-vous faire la même chose en réécrivant les deux fonctions ci-dessus.

```c
#include <stdio.h>

#define TAILLE_MAX 100

// On redéfinit strlen() pour ne pas avoir à inclure string.h
unsigned long int strlen(const char *chaine) {
  int i = 0;
  while (chaine[i] != '\0') {
    i++;
  }
  return i;
}

// On redéfinit strcpy() pour ne pas avoir à inclure string.h
char * strcpy(char *chaine1, const char *chaine2) {
  int i = 0;
  while (chaine2[i] != '\0') {
    chaine1[i] = chaine2[i];
    i++;
  }
  chaine1[i] = '\0';

  return chaine1;
}

// On redéfinit strcat() pour ne pas avoir à inclure string.h
char * strcat(char *chaine1, const char *chaine2) {
  int i = 0, j = 0;
  while (chaine1[i] != '\0') {
    i++;
  }
  while (chaine2[j] != '\0') {
    chaine1[i] = chaine2[j];
    i++;
    j++;
  }
  chaine1[i] = '\0';

  return chaine1;
}

int main(void) {
  
  char prenom[TAILLE_MAX] = {0};
  char nom[TAILLE_MAX] = {0};
  char formule1[TAILLE_MAX] = {0};
  char formule2[TAILLE_MAX] = {0};

  int n = 0, i = 0;

  printf("Entrez un prénom : ");
  scanf("%s", prenom);
  printf("Entrez un nombre : ");
  scanf("%d", &n);

  for (i = 0; i < n; i++) {
    printf("Merci %s\n", prenom);
  }

  printf("Nombre de lettres de %s : %ld\n", prenom, strlen(prenom));

  printf("Entrez un nom : ");
  scanf("%s", nom);

  strcpy(formule1, prenom);
  strcat(formule1, " ");
  strcat(formule1, nom);

  strcpy(formule2, nom);
  strcat(formule2, " ");
  strcat(formule2, prenom);

  printf("%s\n", formule1);
  printf("%s\n", formule2);

  return 0;
}
```

# Table ASCII

- Afficher les caractères de la table ASCII de 32 à 127
- Que se passe-t-il se passe pour les caractères inférieurs à 32 ou plus grands que 128 ?
- Transformer une phrase saisie au clavier en majuscules sans utiliser les fonctions standards

```c
#include <stdio.h>

int main(void)
{
    for (int i = 32; i <= 127; i++)
    {
        printf("%c ", i);
    }

    printf("\n");

    char phrase[100] = {0};
    printf("Entrez une phrase : ");
    scanf("%s", phrase);

    for (int i = 0; phrase[i] != '\0'; i++)
    {
        if (phrase[i] >= 'a' && phrase[i] <= 'z')
        {
            phrase[i] -= 32;
        }
    }

    printf("%s\n", phrase);

    return 0;
}
```

Pour les caractères inférieurs à 32 ou plus grands que 128, on obtient des caractères spéciaux qui ne sont pas affichables.

Pour la partie sur les majuscules, je redirige vers [TP2.md](../TP-Temp/TP1%20-%20Outils/TP1%20-%20Outils.md#commentaires-sur-le-code).