#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 100

// An operation is a char and two operands are integers
// For example 3 1 + -> 3 + 1
// The goal is to compute operations like 3 1 + 4 * 2 -
typedef struct {
  char operation;
  float operand1;
  float operand2;
} Operation;

// A stack is a list of operations
typedef struct {
  Operation operations[MAX_SIZE];
  int size;
} Stack;

// Initialize the stack
void init(Stack *stack) { stack->size = 0; }

// Push an operation on the stack
void push(Stack *stack, Operation operation) {
	// Only add the operation if the stack is not full
  if (stack->size < MAX_SIZE) {
    stack->operations[stack->size] = operation;
    stack->size++;
  }
}

// Pop an operation from the stack
Operation *pop(Stack *stack) {
  if (stack->size > 0) {
    stack->size--;
    return &stack->operations[stack->size];
  }
  return NULL;
}

// Print the stack
void print(Stack *stack) {
  printf("Stack size: %d\n", stack->size);
  for (int i = 0; i < stack->size; i++) {
    printf("%c %f %f\n", stack->operations[i].operation,
           stack->operations[i].operand1, stack->operations[i].operand2);
  }
}

// Compute the result of an operation
float compute(Operation *operation) {
  switch (operation->operation) {
  case '+':
    return operation->operand1 + operation->operand2;
  case '-':
    return operation->operand1 - operation->operand2;
  case '*':
    return operation->operand1 * operation->operand2;
  case '/':
    return operation->operand1 / operation->operand2;
  default:
    return 0;
  }
}

void freeStack(Stack stack) {
	for (int i = 0; i < stack.size; i++)
	{
		free(&stack.operations[i]);
	}
}

int main(void) {

  // Create a stack
  Stack stack;
  init(&stack);

  // Input
  char input[MAX_SIZE];

  printf("Enter a polish notation: ");

  // Read the input
  fgets(input, MAX_SIZE, stdin);

  // Separate the input into tokens
  char *token = strtok(input, " ");

  // While there are tokens
  while (token != NULL) {

    Operation operation;

    // If the token is a number
    if (sscanf(token, "%f", &operation.operand1) == 1) {
      // Push the operation on the stack
      push(&stack, operation);
    } else {

      // If the token is an operation
      operation.operation = token[0];
      operation.operand2 = pop(&stack)->operand1;
      operation.operand1 = pop(&stack)->operand1;

      // Compute the result and push it on the stack
      operation.operand1 = compute(&operation);
      push(&stack, operation);
    }

    // Get the next token
    token = strtok(NULL, " ");
  }

  // Print the result
  printf("Result: %f\n", pop(&stack)->operand1);

	freeStack(stack);

  return 0;
}