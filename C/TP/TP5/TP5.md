---
title: TP5 - C
author: VAN DE MERGHEL Robin
date: 20 septembre 2023
--- 

On va faire un motus.

# Moteur de jeu

> Déclarer un tableau statique de chaînes de caractères contenant au plus 6 caractères. Initialiser le tableau avec par exemple :

```C
char tableau [][6] = { "AGENT", "AGILE", "AIDER", "BADGE", "BIERE", 
                       "CABLE", "CACHE", "CACAO", "CARTE", "CLEFS",
                       "ECRAN", "GEEEK", "ISIMA", "MULOT", "PUCES", "VIDEO" };
```

- Choisir un mot au hasard dans le tableau sans l’afficher. L'utilisateur doit trouver ce mot. Le nombre de tentatives peut être limité
- Demander à l’utilisateur de saisir un mot. Si le mot est trop court ou trop long, la tentative est comptabilisée mais ne donnera aucune information sur le mot à trouver. Il faut afficher les lettres bien placées à leur place, un tiret pour les lettres non déterminées et à côté les lettres mal placées. Les lettres bien placées sont toujours connues. On pourra donner la première lettre

D'abord on va faire une fonction qui choisit un mot au hasard dans le tableau :

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TAILLE_MOT 6

char tableau[][TAILLE_MOT] = {
    "AGENT", "AGILE", "AIDER", "BADGE", "BIERE", "CABLE", "CACHE", "CACAO",
    "CARTE", "CLEFS", "ECRAN", "GEEEK", "ISIMA", "MULOT", "PUCES", "VIDEO"};

char *mot_a_trouver() {
  srand(time(NULL));
  int i = rand() % 16;
  return tableau[i];
}

void enleve_caracteres(char mot[]) {

  for (int i = 0; i < TAILLE_MOT; i++) {
    if (mot[i] < 'A' && mot[i] > 'Z') {
      mot[i] = ' ';
    }
  }
}

void convertitMajuscule(char mot[]) {

  for (int i = 0; i < TAILLE_MOT; i++) {
    if (mot[i] >= 'a' && mot[i] <= 'z') {
      mot[i] = mot[i] - 32;
    }
  }

  enleve_caracteres(mot);
}

void mot_utilisateur(char mot_utilisateur[]) {
  printf("Entrez un mot : ");
  scanf("%s", mot_utilisateur);
  convertitMajuscule(mot_utilisateur);
  enleve_caracteres(mot_utilisateur);
}

void verification(char mot_utilisateur[], char mot_a_trouver[],
                  char est_vrai[]) {
  int i = 0;

  int indice_mal_places = TAILLE_MOT;

  // Pour chaque caractère de mot_utilisateur
  // - On regarde si il est dans mot_a_trouver
  // - On regarde si il est bien placé
  // - Si oui, on met le caractère en majuscule dans est_vrai
  // - Si non, on met le caractère en minuscule dans est_vrai
  // Si il n'est pas dans mot_a_trouver, on met un "_" dans est_vrai
  // Les indices de 0 à TAILLE_MOT - 1 sont pour les caractères bien placés
  // L'indice TAILLE_MOT est pour l'espace
  // Les indices de TAILLE_MOT + 1 à TAILLE_MOT * 2 sont pour les caractères
  // mal placés
  for (i = 0; i < TAILLE_MOT; i++) {

    if (mot_utilisateur[i] < 'A' && mot_utilisateur[i] > 'Z') {
      continue;
    }

    // Si la lettre est contenue dans le mot
    if (strchr(mot_a_trouver, mot_utilisateur[i]) != NULL) {
      // Si la lettre est bien placée
      if (mot_utilisateur[i] == mot_a_trouver[i]) {
        est_vrai[i] = mot_utilisateur[i];
      } else {
        // Si la lettre est mal placée
        est_vrai[indice_mal_places] = mot_utilisateur[i];
        indice_mal_places++;
        est_vrai[i] = '-';
      }
    } else {
      // Si la lettre n'est pas contenue dans le mot
      est_vrai[i] = '-';
    }
  }

  // Il faut mettre le caractère de fin de chaîne
  est_vrai[indice_mal_places] = '\0';

  // On met un espaces entre les caractères
  est_vrai[TAILLE_MOT - 1] = ' ';

  puts("");
}

int main() {
  char *mot = mot_a_trouver();
  printf("Trouver : %s\n", mot);
  convertitMajuscule(mot);

  char mot_user[TAILLE_MOT + 1];
  char mot_cache[TAILLE_MOT * 2 + 1 + 1];
  char est_vrai[TAILLE_MOT * 2 + 1 + 1]; // +1 pour \0 +1 pour l'espace

  mot_utilisateur(mot_user);
  // printf("%s\n", mot_util);

  verification(mot_user, mot, est_vrai);
  printf("%s\n", est_vrai);
  return 0;
}
```

# Lire un fichier texte

- Déclarer un tableau comme pour le moteur de jeu avec plus de lignes (par exemple 1000)
- Stocker dans ce tableau le contenu d’un fichier texte dont la première ligne est le nombre de mots contenus dans ce fichier et qui contient par la suite un mot par ligne. On impose que tous les mots aient le même nombre de lettres
- Afficher le tableau pour vérifier que la lecture est correcte. Si le fichier contient plus de 1000 mots, il faudra tronquer (ou alors modifier la constante)
- Modifier le moteur de jeu pour prendre en compte cette nouvelle fonctionnalité.

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TAILLE_MOT 6
#define TAILLE_MAX 1000

char dictionnaire[TAILLE_MAX][TAILLE_MOT];

void lire_dictionnaire() {
  FILE *fichier = fopen("dictionnaire.txt", "r");
  if (fichier == NULL) {
    printf("Erreur lors de l'ouverture du fichier\n");
    exit(1);
  }

  int i = 0;
  while (fgets(dictionnaire[i], TAILLE_MOT, fichier) != NULL) {
    dictionnaire[i][TAILLE_MOT - 1] = '\0';
    i++;
  }

  fclose(fichier);
}

char *mot_a_trouver() {
  srand(time(NULL));
  int i = rand() % TAILLE_MAX;
  return dictionnaire[i];
}

void enleve_caracteres(char mot[]) {

  for (int i = 0; i < TAILLE_MOT; i++) {
    if (mot[i] < 'A' && mot[i] > 'Z') {
      mot[i] = ' ';
    }
  }
}

void convertitMajuscule(char mot[]) {

  for (int i = 0; i < TAILLE_MOT; i++) {
    if (mot[i] >= 'a' && mot[i] <= 'z') {
      mot[i] = mot[i] - 32;
    }
  }

  enleve_caracteres(mot);
}

void mot_utilisateur(char mot_utilisateur[]) {
  printf("Entrez un mot : ");
  scanf("%s", mot_utilisateur);
  convertitMajuscule(mot_utilisateur);
  enleve_caracteres(mot_utilisateur);
}


```