---
title: "TP2 - C"
author: "VAN DE MERGHEL Robin"
date: "18 septembre 2023"
---

# La saisie

> Écrire un programme qui demande à l'utilisateur un nombre et affiche toutes les divisions entières par 2 jusqu'à 0.

```c
#include <stdio.h>

int main(void)
{
    int nombre = 0;
    printf("Entrez un nombre : ");
    scanf("%d", &nombre);
    while (nombre > 0)
    {
        printf("%d\n", nombre);
        nombre = nombre / 2;
    }
    return 0;
}
```

## Petite calculatrice à notation polonaise

> Écrire un programme qui demande une opération à effectuer puis les deux nombres dont il faut faire une opération.

```c
#include <stdio.h>

int main(void)
{
    int nombre1 = 0, nombre2 = 0;
    char operation = 0;
    
    printf("Entrez une opération : ");
    scanf("%c", &operation);
    printf("Entrez deux nombres : ");
    scanf("%d %d", &nombre1, &nombre2);
    switch (operation)
    {
    case '+':
        printf("%d + %d = %d\n", nombre1, nombre2, nombre1 + nombre2);
        break;
    case '-':
        printf("%d - %d = %d\n", nombre1, nombre2, nombre1 - nombre2);
        break;
    case '*':
        printf("%d * %d = %d\n", nombre1, nombre2, nombre1 * nombre2);
        break;
    case '/':
        printf("%d / %d = %d\n", nombre1, nombre2, nombre1 / nombre2);
        break;
    default:
        printf("Erreur : opération inconnue\n");
        break;
    }
    return 0;
}
```

## Encore des maths

> Écrire un programme qui demande à l'utilisateur quelle somme il veut réaliser (somme des $n$, $n^2$ et $\frac{1}{n^2}$) avec un switch et réutiliser le code fait pour les boucles.

```c
#include <stdio.h>

int main(void)
{
    int nombre = 0, somme = 0;
    char operation = 0;
    printf("Entrez une opération : ");
    scanf("%c", &operation);
    printf("Entrez un nombre : ");
    scanf("%d", &nombre);
    switch (operation)
    {
    case 'n':
        for (int i = 1; i <= nombre; i++)
        {
            somme += i;
        }
        printf("Somme des n = %d\n", somme);
        break;
    case 'n2':
        for (int i = 1; i <= nombre; i++)
        {
            somme += i * i;
        }
        printf("Somme des n2 = %d\n", somme);
        break;
    case '1/n2':
        for (int i = 1; i <= nombre; i++)
        {
            somme += 1 / (i * i);
        }
        printf("Somme des 1/n2 = %d\n", somme);
        break;
    default:
        printf("Erreur : opération inconnue\n");
        break;
    }
    return 0;
}
```

# Fonction, portée des variables et premiers algorithmes

## Variable globale

- Définir une variable globale en début de programme/fichier et l’initialiser
- Afficher sa valeur dans la fonction `main()`
- Créer une fonction avant la fonction `main()` qui modifie la valeur de la variable
- Vérifier sa valeur dans la fonction `main()` avant et après l'appel à cette fonction


```c
#include <stdio.h>

int variable = 0;

void fonction(void)
{
    variable = 1;
}

int main(void)
{
    printf("Valeur de la variable : %d\n", variable);
    fonction();
    printf("Valeur de la variable : %d\n", variable);
    return 0;
}
```

*Note : la variable globale est accessible dans toutes les fonctions du fichier.*

## Variable locale

- Définir une variable locale dans la fonction `main()`
- Vérifier que cette variable n'est pas accessible dans une autre fonction directement (fonction écrite avant ou après la fonction `main()`)
- Passer cette variable en paramètre à une fonction et modifier la variable dans la fonction
- Afficher la valeur de la variable dans la fonction
- Vérifier la valeur dans le programme principal. Que constate-t-on ?

```c
#include <stdio.h>

void fonction(int variable)
{
    variable = 1;
    printf("Valeur de la variable : %d\n", variable);
}

int main(void)
{
    int variable = 0;
    fonction(variable);
    printf("Valeur de la variable : %d\n", variable);
    return 0;
}
```

On constate que la variable n'est pas modifiée dans le programme principal. Cela est dû au fait que la variable *est passée par valeur et non par référence.*

# Calcul de factorielle et tests

- Écrire une fonction qui calcule avec une boucle la valeur de $n!$. Cette valeur ne sera pas affichée par la fonction mais renvoyée !!
- Vérifier quelques valeurs dans le programme principal
- Écrire une fonction récursive qui calcule la valeur de $n!$
- Le programme principal affichera et vérifiera que les deux fonctions renvoient bien les mêmes résultats sur quelques valeurs. Attention, on atteint très vite la limite de définition des `int`. Si vous voulez allez plus loin, il faut utiliser des `long int`

```c
#include <stdio.h>

int factorielle(int nombre)
{
    int resultat = 1;
    for (int i = 1; i <= nombre; i++)
    {
        resultat *= i;
    }
    return resultat;
}

int factorielleRecursive(int nombre)
{
    if (nombre == 0)
    {
        return 1;
    }
    else
    {
        return nombre * factorielleRecursive(nombre - 1);
    }
}

int main(void)
{
    printf("Factorielle de 5 : %d\n", factorielle(5));
    printf("Factorielle de 5 : %d\n", factorielleRecursive(5));
    return 0;
}
```

On peut utiliser des `long int` pour aller plus loin.

```c
#include <stdio.h>

long int factorielle(int nombre)
{
    long int resultat = 1;
    for (int i = 1; i <= nombre; i++)
    {
        resultat *= i;
    }
    return resultat;
}

long int factorielleRecursive(int nombre)
{
    if (nombre == 0)
    {
        return 1;
    }
    else
    {
        return nombre * factorielleRecursive(nombre - 1);
    }
}

int main(void)
{
    printf("Factorielle de 20 : %ld\n", factorielle(20));
    printf("Factorielle de 20 : %ld\n", factorielleRecursive(20));
    return 0;
}
```

Cela donne une plus grande plage de valeurs.

# Révisions UNIX et compléments C

> Nous vous avons dit en cours que l'option `-lm` est nécessaire pour embarquer le code des fonctions mathématiques dans le code exécutable final (c'est à dire comment elles sont vraiment codées...)

> Le code est contenu dans un fichier dont le nom commence par `libm`. Le nom du fichier peut porter l'extension `.a` ou encore `.so`. Pouvez-vous le ou les trouver dans le système de fichiers ?

On peut trouver le fichier `libm.a` dans `/usr/lib/x86_64-linux-gnu/` et le fichier `libm.so` dans `/usr/lib/x86_64-linux-gnu/`. Ou sur mac dans `/usr/lib/`.

> On peut "lister" le contenu d'un fichier lib, c'est-à-dire les fonctions disponibles avec une commande telle que celle-ci :

```bash
nm chemin_trouve/libmXXXX.XX
```

> Pouvez-vous lister toutes les personnes qui sont en train d'utiliser gcc (en une commande) ?

On peut utiliser la commande `ps` pour lister les processus et `grep` pour filtrer.

```bash
ps -aux | grep gcc
```

