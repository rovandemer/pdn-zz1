---
title: Cours de C ZZ1
author:  VAN DE MERGHEL Robin
date: 2023
tableofcontents: true
lang: fr
--- 

# Plan du cours

- Outillage
- Compilation / Makefile
- Révisions
- Pointeurs
  - De fonctions
  - De structures
- Structures
- Macros
- Fichiers Binaires
- Ligne de commande
- Système
- Argument var
- SDL2

# Outillage

## Un logiciel

Dans la création et conception du logiciel, on distingue deux pôles :

- L'utilisateur
  - Utilise
  - Donne les spécifications du prog
- Le développeur
  - Code
  - Compile
  - Analyse
  - Maintenances

*Note : les différents acteurs peuvent évoluer au cours du temps* (par exemple : une équipe de développeur peut changer)

Sur chaque logiciel, il faut se demander quels sont les besoins de l'utilisateur : 

- UI
- Fonctionnalités
- Performances
- ...

### Développement

On peut voir un cycle de développement :

![Cycle de développement](./img/cycle-dev.svg)

## Compilateur

Un *compilateur* est un programme qui va transformer un code source (compréhensible par l'humain) en un code binaire (compréhensible par la machine).

Il existe plusieurs compilateurs :

- GCC (utilisé par ISIMA)
- Clang
- G++
- ...

### Installation

Sur *Linux*, il suffit d'installer le paquet `gcc` (ou `g++` pour le C++) :

```bash
sudo apt install gcc
```

Sur *Windows*, il faut installer `MinGW` (ou `Cygwin`) :

- [MinGW](https://sourceforge.net/projects/mingw-w64/)
- [Cygwin](https://www.cygwin.com/)
- [TDM-GCC](https://jmeubank.github.io/tdm-gcc/)
- ...

Sur *MacOS*, il faut installer `gcc` via `HomeBrew` :

```bash
# Installation de homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Installation de gcc
brew install gcc
```

## Débogueur

Un `débogueur` (aka *debugger*) est un programme qui permet d'analyser un programme en cours d'exécution. 

On peut faire des *breakpoints* (points d'arrêt) pour analyser le programme à un moment précis. En gros dire au programme "Arrête toi ici et dis moi où en sont les variables".

Il existe plusieurs débogueurs :

- `GDB`
- `DDD` (GDB version graphique)

## Profileur

Un `profileur` est un programme qui permet d'analyser un programme en cours d'exécution et de donner des informations supplémentaires par rapport au debugger. Il analyse et permet d'avoir des informations comme :

- Variables initialisées (ou pas)
- Occupation de mémoire (éviter les fuites de mémoires)
- Temps d'exécution (optimisation)
  
Un exemple connu de `profiler` c'est `Valgrind` (sur mac ça n'existe pas, il faudra passer par une VM type `VirtualBox` ou `Lima`).

*Note* : pour pouvoir rendre le programme compilé utilisable par un `profiler`, il faut rajouter l'argument `-g` dans la commande de compilation :

```bash
gcc main.c -o main -g
```

## Gestionnaire de versions

Un logiciel cool est `Git` : il permet de pouvoir faire un historique des versions sur un projet. Globalement pour le travail en équipe cela aide aussi à partager une même version du code. 

Plusieurs utilités :

- Historique des versions
- Travail en équipe
- Retour en arrière grâce aux versions
- Versions de tests (*branches*)
- Synchronisation du code (pas de personne qui a un code différent)

## Tests unitaires

On peut tester des petits bout de code grâces à des tests unitaires (à faire avant l'intégration de ces bouts de code).

On peut les faire à la main ou *automatiquement* (le plus intéressant).

Pour cela, il faut :

- Des tests plus ou moins exhaustifs
- Conditions réelles
- Faire un code qui lui même est orienté test (`TDD`)

*Cf : [test unitaire de Loïc](https://gitlab.com/kiux/c1b/-/blob/master/teZZt.c?ref_type=heads)*


###### Exemple

Un exemple de test unitaire avec le module de *Loïc* :

```C
// f ici c'est une fonction qui prend un int et renvoie un int
// factorielle c'est le nom du test
// On admet l'avoir codée
TEST(factorielle)
{
    // En gros on va tester des valeurs connues de la fonction
    CHECK(1 == f(0));
    CHECK(6 == f(3));
    CHECK(5040 == f(7));
}
```

En prenant la fonction $f$ suivante :

```C
int f(int x) {
    int r = 1;
    if (x==3) {
        r = 6;
    } else if (x == 7) {
        r = 5040;
    }
    return r;
}
```

On se rend compte que les tests unitaires passent, mais que si l'on prend $f(2)$, le résultat est faux. Il faut donc améliorer les tests unitaires.

## Documentation

Il existe plusieurs outils pour faire de la documentation :

- `Doxygen`
- `Sphinx`
- `Javadoc`
- ...

Ils permettent de générer des pages `HTML` avec la documentation du code. Exemple avec `Doxygen` :

```java
/**
  * @param x the number
  * @return the factorial of x
  */
int f(int x) {
    int r = 1;
    if (x==3) {
        r = 6;
    } else if (x == 7) {
        r = 5040;
    }
    return r;
}
```

Avec `@param` et `@return` on ajoute des informations sur la fonction située juste en dessous.

## Analyse de code

On peut aussi faire du checking de code avec : 

- `CppCheck`
- `Clang tidy`
- `SonarQube`
- ...

Cela permet par exemple de voir si il y a des failles connues, de meilleurs implémentations, ...

# Compilation / Makefile

## Makefile

Un `Makefile` est un fichier qui permet de compiler un programme. Il permet de faire des commandes plus facilement.

Il est composé de plusieurs parties :

- Définition des variables
- Définition des règles
- Définition des règles par défaut
- Définition des règles spéciales
- Définition des règles de nettoyage

Cela permet nottament de faire des commandes plus facilement :

```bash
make # Remplace gcc main.c fichier1.c fichier2.c ... -o main
make clean
```

## Structure d'un programme

Un programme est divisé en plusieurs parties :

- Inclusions des fhichiers d'entête des librairies standards et personnelles (en gros le `import` de `python`)
- Définition des constantes symboliques et macros (si on utilise `PI` 50 fois, on peut faire une constante pour tout le programme)
- Déclaration des types personnels (" EN GROS " des objets)
- Déclaration, initialisation, description des variables globales
- Définition des fonctions (*ne pas oublier `main()`*)

```C```

*Note : il ne peut pas exister de fonctions avec le même nom* (donc si on importe une librairie avec la fonction `printf`, on ne peut pas en refaire une).

## Mot clés réservés

| Mot clé | Signification |
| ------- | ------------- |
| `auto` | Variable locale |
| `break` | Sortie de boucle |
| `case` | Cas d'un `switch` |
| `char` | Type caractère |
| `const` | Constante |
| `continue` | Passe à l'itération suivante |
| `default` | Cas par défaut d'un `switch` |
| `do` | Début d'une boucle `do while` |
| `double` | Type réel double précision |
| `else` | Sinon |
| `enum` | Enumération |
| `extern` | Variable globale déclarée dans un autre fichier |
| `float` | Type réel simple précision |
| `for` | Boucle `for` |
| `goto` | Saut inconditionnel |
| `if` | Condition |
| `int` | Type entier |
| `long` | Type entier long |
| `register` | Variable locale stockée dans un registre |
| `return` | Retour de fonction |
| `short` | Type entier court |
| `signed` | Type entier signé |
| `sizeof` | Taille d'un type |
| `static` | Variable globale |
| `struct` | Structure |
| `switch` | Condition multiple |
| `typedef` | Définition d'un type |
| `union` | Union |
| `unsigned` | Type entier non signé |
| `void` | Type vide |
| `volatile` | Variable volatile |
| `while` | Boucle `while` |

## Déclarer / Définir

Définir une fonction c'est donner le code de la fonction et donc son fonctionnement. Par exemple "La factorielle suit la règle $f(n) = n \times f(n-1)$".

Alors que déclarer une fonction c'est juste dire "La factorielle existe et elle prend un entier en paramètre et renvoie un entier".

On peut déclarer la *signature* d'une fonction dans un fichier d'entête (`.h`) et la définir dans un fichier source (`.c`) : cela permet de pouvoir mettre les fonctions dans n'importe quel ordre dans le fichier source.

```C
// Déclaration
int factorielle(int n);

// Définition
int main(void) {
    int a = factorielle(3);

    return 0;
}

int factorielle(int n) {
    if (n == 0) {
        return 1;
    } else {
        return n * factorielle(n-1);
    }
}
```


> On notera ici que la fonction `factorielle` est implémentée *après* le code `main`. En temps normal on ne pourrait pas l'utiliser car codée après, mais *comme on l'a définie avant*, alors ça marche.

## Processus de compilation

On peut appeler la compilation un *processus* car il y a plusieurs étapes :

![Processus de compilation](./img/compil-process.svg)

- Préprocesseur
  - Remplace les `#include` par le contenu des fichiers d'entête
  - Remplace les macros par leur valeur
  - Supprime les commentaires
  - ...
- Compilation
  - Vérifie la syntaxe
  - Vérifie les types
  - ...
- Assemblage
  - Transforme le code en langage machine
- Édition de liens
  - Rassemble les différents fichiers objets
  - Ajoute les librairies
  - ...
- Exécution

### Transformation du code

On part du code suivant :

```C
#include <stdio.h>

#define T 10

int tab[T];

int main() {
    int i;
    for (i=0;i<T;i++) {
        tab[i] = 0;
    }
    return 0;
}
```

Après assemblage, on obtient le code suivant :

```C
/* fichier de stdio.h */

int tab[10];

int main() {
    int i;
    for (i=0;i<10;i++) {
        tab[i] = 0;
    }
    return 0;
}
```

> On notera que toutes les occurences de `T` ont été remplacées par `10`.

## Compilation séparée

On peut séparer la compilation en plusieurs fichiers.

Cela :

- Évite les fichiers trop denses
  - Pas facile à comprendre, se déplacer
  - Homogénéité du code
- Séparer en *module*
  - Plus facile à maintenir
  - Plus facile à réutiliser
  - Regroupement par thématique (ex : `math.c`, `string.c`, ...)
  - Plus facile à tester

## Module

Un *module* est littéralement un fichier source (`.c`) et un fichier d'entête (`.h`).

| Fichier d'entête | Fichier source |
| ---------------- | -------------- |
| Extension `.h`   | Extension `.c` |
| Déclaration des fonctions | Définition des fonctions |
| Déclaration des variables globales | Définition des variables globales |
| Déclaration des types | Fonctions privées |
| Déclaration des constantes | Variables globales privées |
| Déclaration des macros | Types privés |

### Fichier d'entête

Un exemple de fichier d'entête :

```C
#ifndef __LOIC_DONNEES_H__
#define __LOIC_DONNEES_H__

#define TAILLE_MAX 300

extern int variable_globale;

void initialiser(float[], int);
int lireFichier(char[], float[]);
int calculer(float[], int);

#endif
```

Explications :

| Ligne | Signification |
| ----- | ------------- |
| `#ifndef __LOIC_DONNEES_H__` | Si le fichier d'entête n'est pas défini / pas déjà inclus |
| `#define __LOIC_DONNEES_H__` | On définit le fichier d'entête |
| `#define TAILLE_MAX 300` | On définit une constante |
| `extern int variable_globale;` | On déclare une variable globale |
| `void initialiser(float[], int);` | On déclare une fonction |
| `int lireFichier(char[], float[]);` | On déclare une fonction |
| `int calculer(float[], int);` | On déclare une fonction |
| `#endif` | Fin de la condition |

Le `ifndef` est important pour ne pas dupliquer les déclarations de fonctions et de variables globales.

### Fichier source

Un exemple de fichier source :

```C
#include <stdio.h>
#include "donnees.h"
#include "stats.h"

int variable_globale = 0;

void initialiser(float tab[], int taille) {
    int i;
    for (i=0;i<taille;i++) {
        tab[i] = 0;
    }
}

...
```

Explications :

| Ligne | Signification |
| ----- | ------------- |
| `#include "donnees.h"` | On inclut le fichier d'entête local |
| `#include "stats.h"` | On inclut le fichier d'entête local |
| `int variable_globale = 0;` | On définit la variable globale déclarée dans le fichier d'entête |
| `void initialiser(float tab[], int taille) {` | On définit la fonction déclarée dans le fichier d'entête |
| ... | ... |

On peut donc utiliser toutes les fonctions / variables définies dans le fichier d'entête.

### `Include`

*Note sur le `include`*, dans le fichier précédent on a pu voir deux syntaxes : 

- `#include <stdio.h>`
- `#include "donnees.h"`

La différence est la localisation du fichier :

- `#include <stdio.h>` : on inclut un fichier d'entête de la librairie standard, située dans le dossier `/usr/include`
- `#include "donnees.h"` : on inclut un fichier d'entête local, situé dans le dossier courant

### Gardiens

Les gardiens permettent de ne pas inclure plusieurs fois le même fichier d'entête. Cela permet d'éviter les erreurs de compilation.

```C
#ifndef __LOIC_DONNEES_H__
#define __LOIC_DONNEES_H__

...

#endif
```

## Structure de projet

On a la structure du projet qui peut être la suivante :

![Structure de projet](./img/project-struct.svg)

Il faut bien faire attention à la cohérence de tout le code, mais cela aide à la lisibilité du code.

## Compilation globale

Si on a `main.c`, `stats.c`, `data.c`, `graph.c`, `texte.c`, on peut compiler avec :

```bash
gcc main.c stats.c data.c graph.c texte.c -o main
```

Mais cela est long à écrire, il n'y a pas d'exploitation de la séparation et le temps de compilation est long.

Pour résoudre cela, il y a la compilation séparée.

## Compilation séparée

On va compiler les différents fichiers de code en `.objets` (`.o`) et ensuite les lier ensemble.

```bash
# On compile les fichiers sources en fichiers objets
gcc -c main.c -o main.o
gcc -c stats.c -o stats.o
gcc -c data.c -o data.o
gcc -c graph.c -o graph.o
gcc -c texte.c -o texte.o

# On lie les fichiers objets
gcc main.o stats.o data.o graph.o texte.o -o main
```

> Note : on peut rajouter les habituels `-Wall`, `-Wextra`, `-Werror`, ... pour avoir des warnings et erreurs.

À cette étape, il ne reste plus que l'édition des liens, d'où le `gcc main.o stats.o data.o graph.o texte.o -o main`.

