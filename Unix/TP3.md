---
title: TP 3 - Unix
author: VAN DE MERGHEL Robin
date: 12 septembre 2023
---

# Commandes intéractives ou en arrière plan

> On va s'amuser avec le programme `xeyes` maintenant 

- Lancer `xeyes` en mode interactif. Constater combien l'application est utile
- Mettre en pause l'application avec `CTRL+Z`. Que constatez-vous ?
- Mettre l'application en arrière-plan. Qu'est-ce qui remarche ?
- La remettre au premier plan (`fg`)
- Tuer l'application

On constate que l'application est mise en pause et qu'on ne peut plus l'utiliser. On peut la mettre en arrière plan avec `bg` et la remettre au premier plan avec `fg` :

```bash
[1]+  Stopped                 xeyes
$ bg
[1]+ xeyes &
$ fg
xeyes
```

On peut tuer l'application avec `kill` :

```bash
$ ps
  PID TTY          TIME CMD
  100 pts/0    00:00:00 bash
  101 pts/0    00:00:00 xeyes
  102 pts/0    00:00:00 ps
$ kill 101
[1]+  Terminated              xeyes
```

> On va maintenant jouer avec plusieurs processus :

- Lancer un `xeyes`, le mettre en pause
- Lancer un `xclock`, le mettre en pause
- Editer un fichier vi fichier txt. Ecrire quelques mots et le mettre en pause
- Faire la liste de tous les jobs
- Mette en tâche de fond `xeyes`
- Mette en tâche de fond `vi`. Que se passe-t-il ?
- Mettre `vi` au premier plan
- Quitter `vi`

On peut effectuer l'exercice avec le script bash suivant :

```bash
#!/bin/bash

xeyes
# On met en pause xeyes avec CTRL+Z
xclock
# On met en pause xclock avec CTRL+Z
vi fichier.txt
# On met en pause vi avec CTRL+Z
# On liste les jobs
jobs
# Exemple de sortie :
# [1]-  Stopped                 xeyes
# [2]+  Stopped                 xclock
# On met xeyes en arrière plan
bg 1
# On met vi en arrière plan
bg 3
# On met vi au premier plan
fg 3
# On quitte vi
# On tue xeyes
kill 1
# On tue xclock
kill 2
```


# Commande `kill`

> Commençons par les signaux :

- Lancer un `xeyes` en arrière-plan. Vérifier le bon fonctionnement.
- Trouver le `PID` du processus
- Mettre en pause le processus avec la commande `kill -STOP`. Vérifier le comportement.
- Relancer le processus avec la commande `kill -CONT`. Vérifier le comportement.
- Tuer le processus avec la commande `kill`

On peut effectuer l'exercice avec le script bash suivant :

```bash
#!/bin/bash

xeyes &
# On récupère le PID du processus
pid=$!
# On met en pause le processus
kill -STOP $pid
# On relance le processus
kill -CONT $pid
# On tue le processus
kill $pid
```

> Voyons maintenant ce qui se passe quand on tue un processus parent d'autres processus...

- Lancer un nouveau terminal en tâche de fond `xterm &` ou `gnome-terminal &`
- Lancer un `xeyes` et un xclock en tâche de fond dans ce nouveau terminal.
- Tuer le nouveau terminal

On peut effectuer l'exercice avec le script bash suivant :

```bash
#!/bin/bash

# On lance un nouveau terminal
gnome-terminal &
# On récupère le PID du processus
pid=$!
# On lance xeyes et xclock dans le nouveau terminal
xeyes &
xclock &
# On tue le nouveau terminal
kill $pid
```
