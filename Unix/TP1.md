---
title: TP 1 - Unix
author: VAN DE MERGHEL Robin
date: 8 septembre 2023
---

*Note : Merci à Nathan pour l'info, il y a une gourde "dans l'arborescence créée, patrons est un sous-dossier de frère alors que ça doit être un sous-dossier de minions"*

# Création de dossier

## Minions

> Créer l'arborescence suivante dans votre $HOME :

```bash
minions
|-- freres
|   |-- bob
|   |-- dave
|   |-- kevin
|   `-- tim
`-- patrons
    |-- dracula
    |-- gru
    |-- napoleon
    |-- scarlett
    `-- tyrannosaure
```

On lance la commande suivante : 

```bash
mkdir -p minions/freres/bob \ 
        minions/freres/dave \ 
        minions/freres/kevin \
        minions/freres/tim \
        minions/freres/patrons/dracula \
        minions/freres/patrons/gru \
        minions/freres/patrons/napoleon minions/freres/patrons/scarlett \
        minions/freres/patrons/tyrannosaure
```

Ou on peut remplacer `-p` par des `cd` :

```bash
mkdir minions
cd minions
mkdir freres
cd freres
mkdir bob dave kevin tim patrons
cd ..
cd patrons
mkdir dracula gru napoleon scarlett tyrannosaure
```


*Note : les `\` permettent de faire un retour à la ligne dans le terminal.*

Cela va permettre de générer une arborescence en une commande. Après `tree` on obtient :

```bash
└── minions
    └── freres
        ├── bob
        ├── dave
        ├── kevin
        ├── patrons
        │   ├── dracula
        │   ├── gru
        │   ├── napoleon
        │   ├── scarlett
        │   └── tyrannosaure
        └── tim
```

## Reine des neiges

> Puis, créer au même niveau que le répertoire minions un répertoire films et déplacer le répertoire minions et tous ses répertoires dans le répertoire films. Ajouter cette hiérachie de répertoires dans films : 

```
reine_des_neiges
|-- anna
|-- elsa
`-- olaf
```

```bash
mkdir films

mv minions films

mkdir -p reine_des_neiges/anna reine_des_neiges/elsa reine_des_neiges/olaf

mv reine_des_neiges films
```

On obtient : 

```bash
├── minions
│   └── freres
│       ├── bob
│       ├── dave
│       ├── kevin
│       ├── patrons
│       │   ├── dracula
│       │   ├── gru
│       │   ├── napoleon
│       │   ├── scarlett
│       │   └── tyrannosaure
│       └── tim
└── reine_des_neiges
    ├── anna
    ├── elsa
    └── olaf
```

On obtient :

```bash
└── films
    ├── minions
    │   └── freres
    │       ├── bob
    │       ├── dave
    │       ├── kevin
    │       ├── patrons
    │       │   ├── dracula
    │       │   ├── gru
    │       │   ├── napoleon
    │       │   ├── scarlett
    │       │   └── tyrannosaure
    │       └── tim
    └── reine_des_neiges
        ├── anna
        ├── elsa
        └── olaf
```

### Questions

- Donner les chemins relatifs de dave, de scarlett puis olaf à partir du répertoire kevin
- Créer les fichiers (vides) disney dans reine_des_neiges et bonhomme_de_neige dans olaf sans vous déplacer dans l'arborescence.
- Se déplacer dans olaf
- Copier le fichier bonhomme_de_neige dans `$HOME` en le baptisant bonhomme_relatif en utilisant un chemin relatif
- Copier le fichier bonhomme_de_neige dans `$HOME` en le baptisant bonhomme_absolu en utilisant un chemin absolu
- Se déplacer dans reine_des_neiges
- Copier le fichier disney dans elsa
- Copier tous les répertoires de minions dans le répertoire anna avec une seule commande
- Allez dans le $HOME et vérifier que l'arborescence est correcte
- Effacer tous les répertoires et fichiers que l'on vient de créer
- Trouver les informations affichées par la commande `$ wc /usr/include/X11/Xlib.h`

#### Réponses

On se place **dans** le répertoire `kevin` :

| Question | Réponse |
| -------- | ------- |
| 1.1 | `../dave`, `../patrons/scarlett`, `../../reine_des_neiges/olaf` |
| 1.2 | `touch ../reine_des_neiges/disney ../reine_des_neiges/olaf/bonhomme_de_neige` |
| 1.3 | `cd ../reine_des_neiges/olaf` |
| 1.4 | `cp bonhomme_de_neige ~/bonhomme_relatif` |
| 1.5 | `cp bonhomme_de_neige $HOME/bonhomme_absolu` |
| 1.6 | `cd ../reine_des_neiges` |
| 1.7 | `cp disney elsa` |
| 1.8 | `cp -r ../minions/* anna` |
| 1.9 | `cd $HOME` |
| 1.10 | `rm -rf films` |
| 1.11 | `wc /usr/include/X11/Xlib.h` |

