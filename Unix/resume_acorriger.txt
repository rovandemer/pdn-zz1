
INTRODUCTION UNIX *************************************************************

Une machine UNIX manipule [exclusivement] des fichiers. Dans ces fichiers, on peut trouver un type particulier : les répertoires et ces répertoires peuvent être hiérarchisés. Un répertoire peut contenir des fichiers (ou répertoires) que nécessaire mais ne peut avoir qu'un seul parent (appelé racine) 

On dit que l'on dispose d'une arborescence de fichiers / répertoires. Pour être capable de retrouver un fichier, il faut lister l'ensemble des répertoires qu'il faut traverser pour le trouver. Cette liste s'appelle un chemin.

Il existe deux types de chemins : le chemin relatif, c'est-à-dire par rapport à un répertoire donné (voire courant) et le chemin absolu, c'est-à-dire le chemin par rapport à la "racine" de la machine.

La racine est le répertoire de plus haut niveau, ce répertoire n'a pas de parent. 

COMMANDES *********************************************************************

Pour changer de répertoire, on utilise la commande cd.

La commande ls permet de lister un répertoire, c'est-à-dire d'en obtenir le contenu. C'est une commande de base 
que l'on utilise le plus souvent avec les options -l et -u.

On peut créer un répertoire ou l'effacer si on en a les droits.

La commande pwd est également importante car elle permet de savoir quel est le répertoire actuel de travail. Tous les chemins sont donnés par rapport à ce répertoire sauf mention contraire.

On peut créer un répertoire ou l'effacer si on en a les droits avec rmdir ou mkdir

La commande tree permet d'afficher l'arborescence du disque sous forme texte
