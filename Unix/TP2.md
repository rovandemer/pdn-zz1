---
title: TP 2 - Unix
author: VAN DE MERGHEL Robin
date: 11 septembre 2023
---

# Droits

## Rappels

Pour fichiers :

| Droit | Signification |
|:-----:|:-------------:|
| r     | On peut lire le fichier |
| w     | On peut modifier le fichier |
| x     | On peut exécuter le fichier |

Pour répertoires :

| Droit | Signification |
|-----|-------------|
| r     | On peut lister le contenu du répertoire |
| w     | On peut créer, supprimer ou renommer des fichiers dans le répertoire |
| x     | On peut traverser le répertoire |

## Exercice 1

- Accéder au fichier `/home/local.isima.fr/loyon/xlib.c` qui est en lecture seule (si ça marche pas, prévenez-moi)
- Regarder les droits d'utilisation et le propriétaire
- Vérifier qu'il n'est pas modifiable avec un éditeur de texte
- Copier ce fichier dans votre `$HOME`
- Qui en est maintenant le propriétaire et quels sont les droits ?

Les droits sonts :

```bash
-rwxr-xr-x 1 loleroy3 utilisateurs du domaine 7850 Sep 13  2022 /mnt/local.isima.fr/loleroy3/C/xlib.c
```

| Propriétaire | Groupe | Autres |
|:------------:|:------:|:------:|
| rwx          | r-x    | r-x    |
| Peut tout faire | Peut lire et exécuter | Peut lire et exécuter |

On ne peut pas modifier le fichier car on n'a pas les droits d'écriture.

On copie le fichier dans notre `$HOME` :

```bash
cp /mnt/local.isima.fr/loleroy3/C/xlib.c ~/
```

On regarde les droits :

```bash
-rwxr-xr-x 1 rovandemer utilisateurs du domaine 7852 Sep 11 14:05 xlib.c
```

On est maintenant le propriétaire et on a les droits d'écriture.

## Exercice 2

- Vérifier les droits de votre répertoire `$HOME`.
- Demander à un voisin d'accéder à votre répertoire, cela n'est normalement pas possible
- Donner au voisin la possibilité d'accéder à votre répertoire
- Demander à votre voisin de copier un fichier dans votre répertoire ou d'en créer un (avec touch par exemple). Il doit être le seul à pouvoir modifier ce fichier.
- Effacer ce fichier (oui, c'est possible, le droit d'effacer est lié au réperoire parent qui lui, vous appartient)
- Remettre des droits corrects sur votre répertoire `$HOME`

On regarde les droits :

```bash
drwxr-xr-x 17 rovandemer  utilisateurs du domaine           4096 Sep 11 14:05 rovandemer
```

On demande à un voisin d'accéder à notre répertoire :

```bash
cd /home/local.isima.fr/loyon
```

On ne peut pas modifier le répertoire car on n'a pas les droits d'écriture. 

On donne les droits d'accès à notre voisin :

```bash
chmod 777 rovandemer
```

> La partie sur les voisins, vu l'infrastructure de l'école on peut pas --'.


# Les liens

## Liens physiques

- Créer un fichier et éventuellement l'éditer pour mettre quelque chose dedans
- Afficher l'inode et le compteur de liens de ce fichier
- Créer un lien physique de ce fichier
- Lister les fichiers et comparer les numéros d'inode et le compteur de liens
- Editer le fichier lien et vérifier que l' "original" est bien modifié
- Effacer l'"original"
- Vérifier que l'information est bien toujours présente
- Recréer l'original en faisant un lien
- Créer un répertoire sauv et déplacer un des liens vers ce répertoire
- Vérifier que l'on accède bien à l'information dans le répertoire de départ comme dans le répertoire sauv

Le script shell permettant de faire tout ça :

```bash
#!/bin/bash

# Création du fichier
echo "Hello World !" > file.txt
# Affichage de l'inode et du compteur de liens
ls -i file.txt # Inode
ls -al # On cherche "file.txt" et le nombre de liens est à côté
# Création du lien physique
ln file.txt file2.txt
# Comparaison des numéros d'inode et du compteur de liens
ls -i file.txt # Inode
ls -i file2.txt # Inode
# Même inode
# Editer le fichier lien et vérifier que l' "original" est bien modifié
echo "Bonjour le monde !" > file2.txt
cat file.txt # On voit que le fichier est bien modifié
# Effacer l'"original"
rm file.txt
# Vérifier que l'information est bien toujours présente
cat file2.txt # On voit que le fichier est toujours présent
# On a bien un lien physique
# Recréer l'original en faisant un lien
ln file2.txt file.txt
# Créer un répertoire sauv et déplacer un des liens vers ce répertoire
mkdir sauv
mv file.txt sauv
# Vérifier que l'on accède bien à l'information dans le répertoire de départ comme dans le répertoire sauv
cat sauv/file.txt # On voit que le fichier est bien présent
cat file2.txt # On voit que le fichier est bien présent
```

Pour supprimer tout ce qu'on a fait :

```bash
rm -rf file.txt file2.txt sauv
```

## Liens symboliques

- Créer un fichier texte (éventuellement édité pour être original)
- Créer un lien symbolique
- Afficher le fichier original et le lien symbolique avec la commande `ls -li`
- Vérifier que si on édite le lien symbolique, on édite bien le fichier - original
- Effacer le fichier original
- Essayer d'éditer le lien symbolique

Le script shell permettant de faire tout ça :

```bash
#!/bin/bash

# Création du fichier
echo "Hello World !" > file.txt
# Création du lien symbolique
ln -s file.txt file2.txt
# Affichage de l'inode et du compteur de liens
ls -li file.txt # Inode
ls -li file2.txt # Inode
# Même inode
# Vérifier que si on édite le lien symbolique, on édite bien le fichier - original
echo "Bonjour le monde !" > file2.txt
cat file.txt # On voit que le fichier est bien modifié
# Effacer le fichier original
rm file.txt
# Essayer d'éditer le lien symbolique
cat  file2.txt # On voit que le fichier n'est plus présent
vim file2.txt # On voit que le fichier n'est plus présent
```

Pour supprimer tout ce qu'on a fait :

```bash
rm -rf file.txt file2.txt
```

# Redirections

| Nom | Signification |
|-----|---------------|
| `>` | Redirige la sortie standard vers un fichier |
| `>>` | Redirige la sortie standard vers un fichier en ajoutant à la fin |
| `<` | Redirige l'entrée standard depuis un fichier |
| `<<` | Redirige l'entrée standard depuis un fichier en ajoutant à la fin |
| `|` | Redirige la sortie standard vers l'entrée standard d'un autre programme |
| 1> | Redirige la sortie standard vers un fichier |
| 2> | Redirige la sortie d'erreur vers un fichier |
| &> | Redirige la sortie standard et la sortie d'erreur vers un fichier |

## Sortie standard

> Pour rediriger le résultat d'une commande dans un fichier, il suffit de taper :

```bash
$ commande > fichier
```

> Si on ne veut pas effacer le contenu du fichier mais ajouter la sortie à la fin du fichier :

```bash
$ commande >> fichier
```

> Voici la commande pour lister un niveau seulement d'une arborescence :

```bash
$ tree -L 1 -d /home/local.isima.fr
```

> Qui a dit que ça marchait aussi avec un ls ?


- Rediriger la sortie de la commande vers un fichier résultat
- Le fichier est relativement long, il est difficile de le consulter avec la commande cat, c'est fastidieux avec more ou less, la tête du fichier est consultable avec `head` et la fin avec `tail`
- Compter le nombre de lignes du fichier pour avoir une idée grossière du nombre de comptes sur la machine
- Compléter le fichier résultat avec la liste des répertoires de `/home/isimaadm`
- Refaire le compte

Le script shell permettant de faire tout ça :

```bash
#!/bin/bash

# Rediriger la sortie de la commande vers un fichier résultat
tree -L 1 -d /home/local.isima.fr > resultat.txt
# Le fichier est relativement long, il est difficile de le consulter avec la commande cat, c'est fastidieux avec more ou less, la tête du fichier est consultable avec `head` et la fin avec `tail`
# Compter le nombre de lignes du fichier pour avoir une idée grossière du nombre de comptes sur la machine
wc -l resultat.txt # 94 lignes
# Compléter le fichier résultat avec la liste des répertoires de `/home/isimaadm`
tree -L 1 -d /home/isimaadm >> resultat.txt
# Refaire le compte
wc -l resultat.txt # 103 resultat.txt
```

Pour supprimer tout ce qu'on a fait :

```bash
rm -rf resultat.txt
```

## Sortie d'erreur

> Rediriger l'entrée standard permet de simuler une saisie de l'utilisateur :

```bash
$ commande < fichier
```

> Éditer un fichier et mettre un petit calcul, par exemple : "40 + 5" puis exécuter la commande bc

```bash
$ bc < fichier
```

On crée un fichier :

```bash
echo "40 + 5" > calcul.txt
```

On exécute la commande :

```bash
bc < calcul.txt
```

