---
title: TP 4 - Unix
author: VAN DE MERGHEL Robin
date: 13 septembre 2023
---

# Gérer l'espace

> Avec la commande `find`, on peut trouver tous les fichiers de taille nulle, tous les fichiers se terminant par un `~` (fichiers temporaires d'édition) et les effacer directement.

On copie le code suivant :

```bash
#!/bin/bash
# Created by Ben Okopnik on Wed Jul 16 18:04:33 EDT 2008
# Modified by Loic to meet our needs on find command examples

########    User settings     ############
MAXDIRS=5
MAXDEPTH=2
MAXFILES=100
MAXSIZE=1000
######## End of user settings ############

# How deep in the file system are we now?

mkdir temptree
cd temptree

TOP=`pwd|tr -cd '/'|wc -c`

populate() {
  cd $1
  curdir=$PWD

  files=$(($RANDOM*$MAXFILES/32767))
  for n in `seq $files`
  do
    f=`mktemp XXXXXX`
    size=$(($RANDOM*$MAXSIZE/32767))
    if [ $size -gt 500 ]
    then 
       head -c $size /dev/urandom > $f
    else
      touch $f
    fi
  done

  depth=`pwd|tr -cd '/'|wc -c`
  if [ $(($depth-$TOP)) -ge $MAXDEPTH ]
  then
    return
  fi

  unset dirlist
  dirs=$(($RANDOM*$MAXDIRS/32767))
  for n in `seq $dirs`
  do
    d=`mktemp -d XXXXXX`
    dirlist="$dirlist${dirlist:+ }$PWD/$d"
  done

  for dir in $dirlist
  do
    populate "$dir"
  done
}

populate $PWD
```

> Vous pouvez maintenant chercher tous les fichiers de taille nulle.

On utilise la commande `find` :

```bash
find . -type f -empty
```

Sortie :

```bash
./temptree/2mUO65
./temptree/t5BUgs/JZcQPf
./temptree/t5BUgs/rz7hdP
./temptree/t5BUgs/xg0DgS
./temptree/t5BUgs/ScuE8X
./temptree/t5BUgs/WHSOXt
./temptree/t5BUgs/m6paDX
...
```

Si on veut chercher avec `scp` le fichier distant `"/matieres/3MM1AP/07 anniv.txt"`, le server est `server`, le login `login` et le port `port` :

```bash
scp login@server:"/matieres/3MM1AP/07 anniv.txt" .
```