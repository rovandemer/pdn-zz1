---
title: Analyse numérique ZZ1 (tg sam)
author: VAN DE MERGHEL Robin
date: 2023
---

# Rappels

## Espace vectoriel

Un *espace vectoriel* est un espace munit de deux opérations $(E, +, \cdot)$ tel que :

- $\forall \lambda \in K, \forall x,y \in E, x + \lambda y \in E$
- $0$ est l'élément neutre : $\forall x \in E, x + 0 = x$

## Produit scalaire

Un *produit scalaire* est une application $\langle \cdot, \cdot \rangle : E \times E \to K$ tel que :

- la bilinéarité : $\forall x,y,z \in E, \forall \lambda \in K, \langle x + \lambda y, z \rangle = \langle x, z \rangle + \lambda \langle y, z \rangle$
- la symétrie : $\forall x,y \in E, \langle x, y \rangle = \langle y, x \rangle$
- $0$ est l'élément neutre : $\forall x \in E, \langle x, 0 \rangle = 0$

## Espace euclidien

Un *espace euclidien* est un espace vectoriel de dimension finie munit d'un produit scalaire.

## Sous-espace vectoriel

Un *sous-espace vectoriel* est un sous-ensemble d'un espace vectoriel qui est lui-même un espace vectoriel. Il doit respecter les propriétés de l'espace vectoriel.

![Sous-espace vectoriel](https://botafogo.saitis.net/algebre-lineaire/resources/images/i_EV_SEV_stable.jpg)

###### Exemple

On pose $E=\mathbb{R}^n$. On a :

$$E = \{x = \begin{pmatrix} x_1 \\ \vdots \\ x_n \end{pmatrix}, \forall i\in \{1, \dots, n\}, x_i \in \mathbb{R}\}$$

Produire scalaire canonique :

$$\langle x, y \rangle = \sum_{i=1}^n x_i y_i = x^T y$$

Avec $x^T = (x_1, \dots, x_n)$ et $y = \begin{pmatrix} y_1 \\ \vdots \\ y_n \end{pmatrix}$.

## Propriété du produit scalaire

Si $x^T y = 0$, alors $x$ et $y$ sont *orthogonaux*.

Soit $S$ un sous ensemble de vecteurs de $\mathbb{R}^n$ :

$$\begin{align}
S^\perp &= \{y \in \mathbb{R}^n, \forall x \in S, x^T y = 0\} \\
\end{align}$$

$S$ est le sous ensemble des vecteurs orthogonaux à tous les vecteurs de $S$. On peut vérifier facilement que $S^\perp$ est un sous-espace vectoriel de $\mathbb{R}^n$.

###### Exemple

En prenant $S = \{\begin{pmatrix} 1 \\ 0 \\ 0 \end{pmatrix}, \begin{pmatrix} 0 \\ 1 \\ 0 \end{pmatrix}\}$, on a $S^\perp = \{\begin{pmatrix} 0 \\ 0 \\ 1 \end{pmatrix}\}$.

## Famille libre

Une *famille* de vecteurs $(x_1, \dots, x_n)$ est dite libre si :

$$\forall \lambda_1, \dots, \lambda_n \in K, \lambda_1 x_1 + \dots + \lambda_n x_n = 0 \Rightarrow \lambda_1 = \dots = \lambda_n = 0$$

Ou autrement dit si on ne peut pas expriemer un vecteur de la famille comme combinaison linéaire des autres vecteurs de la famille.

## Famille génératrice

Une *famille* de vecteurs $(x_1, \dots, x_n)$ est dite génératrice si la famille peut générer tout l'espace vectoriel.

Soit $F\in \mathbb{R}^n$ un sous-espace vectoriel, et $(x_1, \dots, x_n)$ une famille de vecteurs de $F$. On a :

$(x_1, \dots, x_n)$ est une base de $F$ si et seulement si $(x_1, \dots, x_n)$ $\forall x \in F, \exists \lambda_1, \dots, \lambda_n \in K, x = \sum_{i=1}^n \lambda_i x_i$.

Si $\mathcal{F}$ est une famille libre et génératrice, alors $\mathcal{F}$ est une base. Ainsi sa dimension est $card(\mathcal{F})$.

### Cas particulier

Si $F$ est un sous-espace vectoriel de $\mathbb{R}^n$ de dimension $n$, alors, $(e_1, \dots, e_n)$, $e_i = \begin{pmatrix} 0 \\ \vdots \\ 0 \\ 1 \\ 0 \\ \vdots \\ 0 \end{pmatrix}$, est une base de $F$.

On peut donc écrire :

$$\begin{align}
x = \sum_{i=1}^{n} (x^T e_i) e_i && \text{ théorème de Riesz}
\end{align}$$

## Deuxième manière de caractériser un sous-espace vectoriel

On prend $(x_1, \dots, x_p)$ $p$ vecteurs de $\mathbb{R}^p$. On a :

$$\begin{align}
Lin(x_1,\dots x_p) = \{x \in \mathbb{R}^n, \exists \lambda_1, \dots, \lambda_p \in K, x = \sum_{i=1}^p \lambda_i x_i\}
\end{align}$$

*Note : avant appelé $Vect$*.

## Deuxième espace fondamental

Soit $\mathcal{M}_{m,n}(\mathbb{K})$ l'ensemble des matrices de taille $m \times n$ à coefficients dans $\mathbb{K}$.

Soit $A \in \mathcal{M}_{m,n}(\mathbb{K})$. $A$ est une transformation linéaire de $\mathbb{R}^n$ dans $\mathbb{R}^m$.

On note $A_{i, \cdot}$ la $i$-ème ligne de $A$ et $A_{\cdot, j}$ la $j$-ème colonne de $A$.

On a :

$$A = \begin{pmatrix} A_{1, \cdot} \\ \vdots \\ A_{m, \cdot} \end{pmatrix} = \begin{pmatrix} A_{\cdot, 1} & \dots & A_{\cdot, n} \end{pmatrix}$$

Si on a $y = AX$ :

- $\forall i \in [1,n]y = \sum_{i=1}^n A_{i, \cdot}^T X_i$ (écriture en ligne)
- $\forall j \in [1,m]y = \sum_{j=1}^n X_j A_{\cdot, j}$ (écriture en colonne)


## Matrice transposée

Soit $A \in \mathcal{M}_{m,n}(\mathbb{K})$. On a $A^T \in \mathcal{M}_{n,m}(\mathbb{K})$, l'unique matrice telle que : 

$$\forall x \in \mathbb{K}, \forall y \in \mathbb{K}^n, y^T A x = x^T A^T y$$

## Quelques définitions

- La *trace* d'une matrice $A \in \mathcal{M}_{n,n}(\mathbb{K})$ est la somme des éléments de la diagonale principale : $tr(A) = \sum_{i=1}^n A_{i,i}$
- Le *détérminant* d'une matrice $A \in \mathcal{M}_{n,n}(\mathbb{K})$ est : $det(A) = \sum_{\sigma \in \mathcal{S}_n} \epsilon(\sigma) \prod_{i=1}^n A_{i, \sigma(i)}$

![Déterminant](https://homeomath2.imingo.net/image2/determinant101.gif)

Géométriquement, cela correspond à la surface (pour $dim=2$) du parallélogramme formé par les vecteurs colonnes de la matrice.

## Produit matriciel

Soit $A \in \mathcal{M}_{m,n}(\mathbb{K})$ et $B \in \mathcal{M}_{n,p}(\mathbb{K})$. On a $AB \in \mathcal{M}_{m,p}(\mathbb{K})$. $\forall i \in [1,m], \forall j \in [1,p]$, on a :

$$AB_{i,j} = \sum_{k=1}^n A_{i,k} B_{k,j} = A_{i, \cdot}^T B_{\cdot, j}$$

Géométriquement, un produit matriciel correspond à une rotation, une homothétie, une symétrie, une projection, etc.

On va calculer la complexité du produit matriciel. On doit définir le coût de $x^Ty$, $x \in \mathbb{K}^n, y \in \mathbb{K}^n$. Le *$flop$* est le coût d'une addition ou d'une multiplication. On a :

$$s \leftarrow s + a \times b : 1 \ flop$$

Donc $x^Ty$ coûte $n$ $flop$.

```Pascal
Pour i=1 à n
    Pour j=1 à n
        s <- s + x[i] * y[j]
    FinPour
FinPour
```

$$C = AB \Rightarrow m \times p \times n \ flops$$

Si $n = m = p$, on a $n^3$ $flops$.

## Inverse d'une matrice

Soit $A \in \mathcal{M}_{n,n}(\mathbb{K})$. On a $A^{-1} \in \mathcal{M}_{n,n}(\mathbb{K})$ tel que $AA^{-1} = A^{-1}A = I_n$. 

Une matrice est inversible *uniquement* si son déterminant est non nul et si elle est carrée.

## Matrice particulière

- Une matrice est *symétrique* si $A = A^T$

$$\begin{align}
A = \begin{pmatrix} 1 & 2 & 3 \\ 2 & 4 & 5 \\ 3 & 5 & 6 \end{pmatrix} && A^T = \begin{pmatrix} 1 & 2 & 3 \\ 2 & 4 & 5 \\ 3 & 5 & 6 \end{pmatrix}
\end{align}$$

- Une matrice est *diagonale* si $A_{i,j} = 0, \forall i \neq j$

$$\begin{align}
A = \begin{pmatrix} 1 & 0 & 0 \\ 0 & 4 & 0 \\ 0 & 0 & 6 \end{pmatrix}
\end{align}$$

- Une matrice est *triangulaire* si $A_{i,j} = 0, \forall i > j$ (triangulaire inférieure) ou $A_{i,j} = 0, \forall i < j$ (triangulaire supérieure)

$$\begin{align}
A = \begin{pmatrix} 1 & 2 & 3 \\ 0 & 4 & 5 \\ 0 & 0 & 6 \end{pmatrix} && A = \begin{pmatrix} 1 & 0 & 0 \\ 2 & 4 & 0 \\ 3 & 5 & 6 \end{pmatrix}
\end{align}$$

- Une matrice est *orthogonale* si $A^T A = I_n$

$$\begin{align}
A = \begin{pmatrix} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{pmatrix}
\end{align}$$

## Deux visions : matricielle, vectorielle

| Vectorielle | Matricielle |
| ----------- | ----------- |
| $S = \{x \in \mathbb{R}^n, Ax = 0\}$ | $y = AX$ |
| $S^\perp = \{y \in \mathbb{R}^n, \forall x \in S, x^T y = 0\}$ | $y = \sum_{i=1}^n A_{i, \cdot}^T X_i$ |
| $Lin \ S = \{x \in \mathbb{R}^n, \exists \lambda_1, \dots, \lambda_p \in K, x = \sum_{i=1}^p \lambda_i x_i\}$ | $y = \sum_{i=1}^n X_i A_{\cdot, i}$ |

On a $Im \ A = {y = \sum_{i=1}^n x_j A_{\cdot, j}, \forall x \in \mathbb{K}^n}$.

Si $y=0$, $AX = 0$ s'écrit :

$$\begin{align}
\forall i \in [1,n], \sum_{j=1}^n A_i^T X_j = 0
\end{align}$$

C'est à dire $\ker \ A = \{x \in \mathbb{K}, AX = 0\}$.

###### Exemple

On pose :

$$\left\{\begin{align}
2x_1 -x_2 = 1\\
x_1 + x_2 = 5\\
\end{align}\right.$$

C'est à dire $A = \begin{pmatrix} 2 & -1 \\ 1 & 1 \end{pmatrix}$,  $X = \begin{pmatrix} x_1 \\ x_2 \end{pmatrix}$, $Y = \begin{pmatrix} 1 \\ 5 \end{pmatrix}$.

###### Exemple

On se place dans $\mathbb{R}^3$, $\mathcal{P} = \{x = \begin{pmatrix} x_1 \\ x_2 \\ x_3 \end{pmatrix}, x_3=0, x_1, x_2 \in \mathbb{R}\}$.

> Trouver $A,B$ tel que $\mathcal{P} = Im \ A$ et $\mathcal{P} = \ker \ B$.

On a $A = \begin{pmatrix} 1 & 0 \\ 0 & 1 \\ 0 & 0 \end{pmatrix}$ et $B = \begin{pmatrix} 0 & 0 & 1 \end{pmatrix}$.

En effet, si on prend $x = \begin{pmatrix} x_1 \\ x_2 \\ 0 \end{pmatrix}$, on a $Ax = \begin{pmatrix} x_1 \\ x_2 \\ 0 \end{pmatrix}$.

De plus, si on prend $x = \begin{pmatrix} 0 \\ 0 \\ x_3 \end{pmatrix}$, on a $Bx = \begin{pmatrix} 0 \\ 0 \\ 0 \end{pmatrix}$.

## Orthogonalité de l'image et du noyau

Soit $A \in \mathcal{M}_{m,n}(\mathbb{K})$. On a :

- $(Im \ A)^{\perp} = \ker \ A^T$ (dans $\mathbb{K}^m$)
- $(\ker \ A)^{\perp} = Im \ A^T$ (dans $\mathbb{K}^n$)

###### Exemple

Soit $A = \begin{pmatrix} 1 & 2 & 0 & 1 \\ 0 & 1 & 1 & 0 \\ 1 & 2 & 0 & 1 \end{pmatrix} \in \mathcal{M}_{3,4}(\mathbb{R})$. On a :

$$\begin{align}
Im \ A &= Lin \ \left(\begin{pmatrix}
    1 \\ 0 \\ 1
\end{pmatrix}, \begin{pmatrix}
    0 \\ 1 \\ 0
\end{pmatrix}\right)\\
\ker \ A &= Lin \ \begin{pmatrix}
    1 \\ 0 \\ -1
\end{pmatrix}\\
Im \ A^T &= \left(\begin{pmatrix}
    1 \\ 2 \\ 0 \\ 1
\end{pmatrix}, \begin{pmatrix}
    0 \\ 1 \\ 1 \\ 0
\end{pmatrix}\right) \\
\ker \ A^T &= Lin \ \left(\begin{pmatrix}
    1 \\ 0 \\ 0 \\ -1
\end{pmatrix}, \begin{pmatrix}
    1 \\ -1 \\ 1 \\ 1
\end{pmatrix}\right)
\end{align}$$

## Système linéaire $AX = Y$

Si $Y\not \in Im \ A$, alors le système n'a pas de solution (*incompatible*).

Si $X_0$ est solution du système tel que $AX_0 = Y$. On pose $X = X_0 + X_1$, $X_1 \in \ker \ A$. On a :

$$\begin{align}
AX &= A(X_0 + X_1)\\
&= AX_0 + AX_1\\
&= Y + 0\\
&= Y
\end{align}$$

L'unicité de la solution dépend de la dimension de $\ker \ A$.