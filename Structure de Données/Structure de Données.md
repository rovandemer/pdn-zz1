---
title: Structure de Données ZZ1
author: VAN DE MERGHEL Robin
date: 2023-2024
---

# Introduction

## Informatique - Programme

- Traitements / Algorithmique
- Données / Informations
  - Données d'entrée
  - Données intermédiaires
  - Données de sortie : résultats
  - Structuration des données en mémoire
  - Comment y accéder ? Accès par adresse (chaque case mémoire est identifiée par son adresse)
- Choix de *SDD* (Structure de Données) : c'est l'organisation des informations et performances de l'algorithme (par exemple le triangle de Pascaln où on peut optimiser les calculs)

## Construction d'un programme

Faire un programme c'est savoir formuler le *problème* afin de connaître toutes les contraintes.

> Par exemple : "Rechercher dans une liste triée"

C'est aussi *spécifier les données* d'entrée et de sortie. Cela rentre aussi avec les contraintes.

> Par exemple : "Rechercher dans une liste triée" : liste triée, élément à rechercher, résultat : booléen

Ensuite c'est construire un *algorithme* qui résout le problème, c'est à dire une suite d'instructions qui permettent de résoudre le problème.

> Par exemple : "Rechercher dans une liste triée" : [une recherche dichotomique](https://fr.wikipedia.org/wiki/Méthode_de_dichotomie)

Calculer la *complexité* de l'algorithme, c'est à dire le nombre d'opérations élémentaires effectuées par l'algorithme en fonction de la taille des données d'entrée. Il est important pour optimiser l'algorithme.

> Par exemple : "Rechercher dans une liste triée" : $O(log(n))$

Enfin, l'*implémentation* de l'algorithme en un langage de programmation. Il faut traduire l'algorithme en un langage de programmation.

## Règles du cours

*(aidez nous WSH 💀)*

- On écrit toujours soit une procédure soit une fonction (Cf Algo) comme suit :
  - Pour réaliser une tâche spécifique
  - Pas d'instructions en dehors de procédure ou fonction
  - La valeur de retour : type simple, une seule valeur, retour par valeur
  - Paramètres
    - Procédure : `E, S, ES`
    - Fonction : `E`, ne pas préciser
- On manipule uniquement des adresses (mémoire adressable) ou des valeurs directes
  - Toutes les variables représentent des adresses
  - Allocation de mémoire plus tard dans le cours

## Objectifs du cours

Les objectifs du cours sont les suivants :

- Connaître et savoir manipuler les principales structures de données données d'un
  - Différentes organisations des objets "élémentaires" (liste continue / chaînée, table, arbre, ...)
  - Comment manipuler ces organisation (ajout, suppression d'élements, tri, ...)
  - Avantages et inconvéniants des ces organisations par rapport au traitement

Ce qu'il faut savoir :

- Définir des fonctions et des procédures
- Utiliser les structures de contrôle (`si`, `alors`, `sinon`, `fsi;`, ...)
- Gérer la mémoire

*Note : Les algorithmes et les structures de données sont indépendants des langages de programmation*

## Plan du cours

- Chapitre I : généralités
  - Mémoire adressable
  - Language utilisé dans le cours
- Chapitre II : Les listes
  - Liste contigüe
  - Liste chaînée
  - Liste doublement chaînée
  - Piles et files

# Chapitre I : Généralités

## 1. Données élémentaires

Données élémentaires :

| Type de données | Exemples   | Description      |
|-----------------|------------|------------------|
| Entier          | 1, 2, 3    | Nombre entier    |
| Réel            | 1.2, 3.4   | Nombre décimal   |
| Caractère       | 'a', 'b'   | Caractère        |
| Booléen         | Vrai, Faux | Valeur de vérité |

## 2. Données composées

Données composées :

| Type de données | Exemples  | Description                          |
|-----------------|-----------|--------------------------------------|
| Tableau         | [1, 2, 3] | Liste d'éléments de même type        |
| Enregistrement  | (1, 2, 3) | Liste d'éléments de types différents |

## 3. Mémoire adressable

### 3.1. Définition

On a la mémoire adressable : `mot x` :

- *Notion abstraite* qui permet d'accéder à une valeur (cela est différent du mot mémoire physique qui a un taille variable)
- Deux notions associées :
  - Une adresse mémoire : `a` du mot
  - Le contenu du mot : `2` (caractère, entier)

> Les explications sont wtf, faut que je le refasse après, mais en gros on regarde sur le schémas

![Mémoire adressable](./img/SchemasWTF.jpg)

- Les ensembles
  - $U$ : *ensemble des valeurs* peuvent être contenues dans les mots, exemple : `mot` de 32 bits, cela revient à $U$ un ensemble de 32 chiffres binaires qui peuvent représenter n'importe quel type de données
  - $A$ : l'*ensemble des adresses* $A\subset U$
  - $M$ : l'*ensemble des mots* (cases mémoire pouvant contenir des valeurs)

- Fonctions associées au mot
  - Fonction d'*adressage* $m$ : permet de trouver le mot à partir d'une adresse.

On a :

$$\begin{array}{rcl}
c : M & \rightarrow & U \\
x & \mapsto & c(x) 
\end{array}$$

Dans l'exemple précédent (Cf [Mémoire adressable](#3-mémoire-adressable)), $c(x) = 2$.

- Successeurs
  - Successeur d'une valeur de $U$ :

$$\begin{array}{rcl}
su : U & \rightarrow & U \\
u & \mapsto & su(u) = u + 1\\
\end{array}$$

$$\begin{array}{rcl}
su : A & \subset U &\rightarrow A \\
a & \mapsto su(a) &= a + 1
\end{array}$$

- Successeur d'un mot :

$$\begin{array}{rcl}
s : M & \rightarrow & M \\
x & \mapsto & s(x) = x + 1\\
\end{array}$$

![Successeur](./img/Successeur.jpg)

- Relation entre $s$, $m$, et $su$ :
  - $s : M \rightarrow M$ successeur d'un mot, $x \mapsto x + 1$
  - $m : A \rightarrow M$ mot d'une adresse, $a \mapsto x$
  - $su : U \rightarrow U$ successeur d'une valeur, $u \mapsto u + 1$
  - $s = m\circ su \circ m^{-1}$ : $M \rightarrow A \rightarrow A \rightarrow M$ : $x \mapsto a \mapsto a + 1 \mapsto x + 1$

### 3.2. Composition des accès élémentaires

Le chaînage $m\circ c$ est "mot à mot suivant" par chaînage.

$$\begin{array}{rcl}
m\circ c : M & \rightarrow & A \rightarrow M \\
x & \mapsto & c(x) \mapsto m \circ c(x) \\
\end{array}$$

###### Exemple

- $c(x) = a$ : le contenu d'un mot est une adresse
- $m(c(x)) = m\circ c(x) = x'$ : le mot suivant le mot $x$ est $x'$
- $c(x') = 5$ : le contenu du mot suivant le mot $x$ est $5$

![Chaînage](img/Chainage.jpg)

- $c\circ m$ donne le contenu du mot d'une adresse

$$\begin{array}{rcl}
c\circ m : A & \rightarrow & M \rightarrow U \\
a & \mapsto & m(a) \mapsto c \circ m(a) = cm(a)\\
\end{array}$$

- $m(a) = x \ ; \ c(x) = 2$
- $c(m(a)) = c\circ m(a) = 2$


Si on veut mettre la valeur 3 dans $x$ :

```Pascal
m(a) := 3;
// Ou
x := 3;
```

Si on veut récupérer le contenu du $x$ : 

```Pascal
cm(a);
c(x);
```

- $m\circ cm$ : adressage indirect (donne le mot à l'adresse $a$)

$$\begin{array}{rcl}
cm : A & \rightarrow & A \subset U \\
a & \mapsto & cm(a) \equiv a'
\end{array}$$

et

$$\begin{array}{rcl}
m: A \rightarrow M\\
a \rightarrow m(a) \equiv x
\end{array}$$

On peut en déduire :

$$\begin{array}{rcl}
m\circ cm : A & \rightarrow & A \rightarrow M \\
a & \mapsto & cm(a) \mapsto m \circ cm(a) = m(a')\\
\end{array}$$


- Notion du *pointeur*
  - Un pointeur $(x)$ est une variable qui contient une adresse
  - L'accès à une valeur est passé par une autre variable (pointeur)

![Pointeur](./img/Pointeur.jpg)

###### Exercice 1

> Procédure d'échange du contenu de deux adresse selon l'exemple suivant :

![Exercice 1](./img/Exercice1.jpg)

```Pascal
procedure echange(E: a, ap)
    m(tmp) := cm(a);
    m(a) := cm(ap);
    m(ap) := cm(tmp);
fin;
```

On peut simplifié comme dit au dessus $m(a) := cm(ap)$ est équivalent à $a := ap$.

### 3.3. Accès d'une adresse à un $k$-uple de valeurs

- Accès à un $k$-uple de valeurs
  
$$\begin{array}{rcl}
cm_k : A'&\subset A &\rightarrow U^k \\
& a & \mapsto cm_k(a)
\end{array}$$

- Accès direct par contiguïté (en C : tableau)

$$\begin{array}{rcl}
a\rightarrow cm_k(a) = (cm(a),cm(a+1), \dots, cm(a+k-1))
\end{array}$$

- Accès indirect : accès au début de $k$-uple par un chaînage (C : pointeur vers tableau)

![K-uple](img/K-Uplet.jpg)

Accès indirect, quels sont les intérêts ?

- Permet un partage de valeurs (accès via plusieurs adresses)
- Une seule copie de valeurs : gain de place mémoire
- Changement de suite de valeurs simplifié si les k-uples existent déjà (la modification d'une seule adresse)


###### Exemple

On a une commande qui est égale à $\text{client}, \text{produit}$ :

![Exemple 1](img/Exemple1.jpg)

###### Exercice 3

![Exercice 3](img/Exercice3.jpg)

###### Exercice 4-1

> Écrire une procédure d'inversion des valeurs d'une zone mémoire d'adresse de début $a$ et de longueur contenu à l'adresse $n$.

```Pascal
procedure inverse(a, n)
	Pour i de 0 à n/2 faire
		m(tmp) := cm(a);
		m(a) := cm(a + n - 1);
		m(a + n - 1) := cm(tmp);
	fin;
fin;
```

> Correction du prof

```Pascal
procedure inversionDL(E: n; ES: a)
	m(milieu) := cm(n) div 2 - 1;
	Pour m(i) de 0 à cm(milieu) pas 1 faire
		echange(a + cm(i), a + cm(n) - cm(i) - 1);
	fin;
fin;
```

###### Exercice 4-2

> Écrire une procédure d'inversion d'une zone mémoire de l'adresse de début $a$ et celle de la fin $b$.

```Pascal
procedure inversionDFNb(ES: a,b)
	m(n) := b-a+1;
	m(milieu) := cm(2) div 2 - 1;
	Pour m(i) de 0 à cm(milieu) pas 1 faire
		echange(a + cm(i), b - cm(i));
	fin;
fin;
```

###### Exercice 4-3

> Écrire une procédure d'inversion d'une zone mémoire de l'adresse dedébut $a$ et de celle de la fin $b$

```Pascal
procedure inversionDFPts(ES: a,b)
	m(couraa) := a;
	m(courab) := b;
	Tant que cm(coura) < cm(courb) faire
		echange(cm(coura), cm(courb));
		m(coura) := cm(coura) + 1;
		m(courb) := cm(courb) - 1;
	fin;
fin;
```

###### Exercice 5

> Illustre l'opération $m(cm(a)+1) := cm(cm(s+1))$

![Exercice 5](img/Exercice5.jpg)

# Les listes

## Définitions

Une liste est un ensemble $V=U^k$ (élément codés sur $k$ mots). *Une liste sur $V$* est une suite finie d'éléments de $V$.

On pose $\alpha$ la première place de la liste (adresse du premier élément), et $\omega$ la dernière place de la liste (adresse du dernier élément).

On a l'ensemble $F$ l'ensemble des places contenant les éléments de $V$. Une place peut contenir un mot, plusieurs mots, voir une cellule dans le cas de liste chaînée.

Il y a des fonctions d'accès élémentaires :

- $\sigma$ : passer d'une place à la place suivante de $F$ : $\sigma : F-\{\omega\} \rightarrow F - \{\alpha\}$
- $\delta$ : $F\rightarrow V$ pour obtenir la valeur qui appartient à $V$ d'une place
- $\alpha$ : Récupère le début de la liste

### Exemple d'algos

On fait un parcours de liste :

```Pascal
procedure parcours(E: alpha)
	x := alpha;
	Tant que x définit faire
		// Traitement
		x := sigma(x); // Passe à la place suivante
	fin;
fin;
```

## Représentation des listes

- Opérations sur les listes
  - Recherche d'un élément
  - Modification de la valeur d'un élément
  - Suppression d'un élément
  - Adjonction d'un élément
- *Attention* ⚠️ : une valeur de $V$ est un $k$-uple de $U$
- Fonction d'accès à l'adresse d'une place
  - $a(\alpha)$ renvoie l'adresse de la première place de la liste
  - $a(\sigma(x))$ renvoie l'adresse de la place suivante de $x$

On a deux représentations possibles

- Représentation contiguë (accès direct / indirect)
- Représentation chaînée (accès indirect)

![Représentation listes](img/ListRep.jpg)

### Liste contiguë

#### Fin de liste

Pour l'accès d'une élément au suivant on a :

$$\begin{array}{rcl}
\text{Si }k=1, \text{ alors } \left\{\begin{array}{rcl}
a(\sigma(x)) &=& a(x) + 1 \\
\delta(x) &=& cm(a(x)) \equiv c(x)
\end{array}\right.
\end{array}$$

Et aussi :

$$\begin{array}{rcl}
\text{Si }k>1, \text{ alors } \left\{\begin{array}{rcl}
a(\sigma(x)) &=& a(x) + k \\
\delta(x) &=& cm_k(a(x))
\end{array}\right.
\end{array}$$

On pose $NIL$ dans $m(a(\omega))$ pour dire que la liste est finie.

Et le pointeur de fin de liste est $a(\omega)$.

Le nombre d'éléments de la liste est $cm(a(\omega)) - cm(a(\alpha)) + 1$.

### Liste vide

Quand la taille est vide, alors on a $a(\alpha) = a(\omega)$ et directement $NIL$.

Avec une liste à un seul élément, on a $a(\alpha) = a(\omega) + k$.